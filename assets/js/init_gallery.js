$(document).ready(function(){
function initBannerSliderAndThumbs(container) {
    /***************************************************************
     * Slider for blog pages
     * See http://kenwheeler.github.io/slick/ for more options
     ==============================================================*/
    var $sliders = $(container);
    if (!$sliders.length) return;
    var $sliderNavSlick, $sliderSlick, _$sliderNavSlickCache;
    $sliders.each(function () {
      var $sliderContainer = $(this);
      var $sliderSlick = $sliderContainer.find('.js-slick-slider');
      var $sliderNavContainer = $sliderContainer.siblings('.slider');
      $sliderNavSlick = $sliderNavContainer.find('.js-slick-slider');

      $sliderSlick
        .slick({
          /*rtl: true,*/
          dots: false,
          infinite: true,
          speed: 300,
          slidesToShow: 1,
          centerMode: false,
          arrows: false,
          accessibility: false,
          fade: true
        });

      _$sliderNavSlickCache = $sliderNavSlick.html();
      $sliderNavSlick
        .slick({
          /*rtl: true,*/
          slidesToShow: 5,
          slidesToScroll: 1,
          focusOnSelect: true,
          arrows: true,
          accessibility: false,
          centerMode: true,
          centerPadding: 0,
          prevArrow: $sliderNavContainer.find('.js-slick-prev'),
          nextArrow: $sliderNavContainer.find('.js-slick-next'),
          responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 3
              }
            }
          ]
        });

      var $sliderCategories = $('.js-slider-category');
      var firstRun = true;
      var slideRel;
      $sliderCategories.on('click', function () {
        $sliderCategories.removeClass('active');
        $(this).addClass('active');
        var slides;
        var param = $(this).data('filter');

        $sliderNavSlick.slick('slickRemove', true, true, true);
        if (param === 'all') {
          slides = _$sliderNavSlickCache;
        } else {
          slides = $(_$sliderNavSlickCache).filter('.slider__item--' + param);
        }
        $sliderNavSlick.slick('slickAdd', slides);


        slideRel = $sliderNavSlick.find('.slick-current').data('slide-rel');
        $sliderSlick.slick('slickGoTo', slideRel, false);
      });

      // On before slide change
      $sliderNavSlick
        .on('afterChange', function(event, slick, currentSlide, nextSlide){
          slideRel = $sliderNavSlick.find('.slick-current').data('slide-rel');
          $sliderSlick.slick('slickGoTo', slideRel, false);
        })
        .on('click', '.slider__item', function () {
          var item = $(this);
          slideRel = $(item).data('slide-rel');
          $sliderSlick.slick('slickGoTo', slideRel, false);
        });

    });
  }
  initBannerSliderAndThumbs('.js-slider-thumbs');




function getItems(cssSelector) {
    var $items = $(cssSelector),
      arrItems = [];

    $items.each(function (i, item) {
      var size = item.getAttribute('data-size').split('x'),
        src = item.getAttribute('href');
      arrItems.push({
        src: src,
        w: parseInt(size[0]),
        h: parseInt(size[1])
      });
    });

    return arrItems;
}

function buildPopup() {

    var htmlPswp =
      '<div class="pswp js-pspw1111" tabindex="-1" role="dialog" aria-hidden="true">\
        <div class="pswp__bg"></div>\
        <div class="pswp__scroll-wrap">\
          <div class="pswp__container">\
            <div class="pswp__item"></div>\
            <div class="pswp__item"></div>\
            <div class="pswp__item"></div>\
            <div class="pswp__item"></div>\
          </div>\
          <div class="pswp__ui pswp__ui--hidden">\
            <div class="pswp__top-bar">\
              <div class="pswp__counter"></div>\
              <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>\
              <button class="pswp__button pswp__button--share" title="Share"></button>\
              <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>\
              <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>\
              <div class="pswp__preloader">\
                <div class="pswp__preloader__icn">\
                  <div class="pswp__preloader__cut">\
                    <div class="pswp__preloader__donut"></div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">\
              <div class="pswp__share-tooltip"></div>\
            </div>\
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>\
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>\
            <div class="pswp__caption">\
              <div class="pswp__caption__center"></div>\
            </div>\
          </div>\
        </div>\
      </div>';

    return $(htmlPswp).appendTo('body')[0];
  }

// var popup = buildPopup();
// var items = getItems('.js-gallery-item');

// var gallery = new PhotoSwipe(popup, PhotoSwipeUI_Default, items);

// gallery.init();

    var cssSelector = '.js-gallery-item';
    var items = getItems(cssSelector);
    var galleryLinks = $(cssSelector);
    var popup = buildPopup();




    // listen events>
    galleryLinks.on('click', function (e) {
      var index = $(this).data('gallery-index') ? $(this).data('gallery-index') : galleryLinks.index(this);
      // Initializes and opens PhotoSwipe
      var gallery = new PhotoSwipe(popup, PhotoSwipeUI_Default, items);
      gallery.init();

      gallery.listen('initialZoomInEnd', function() {
        gallery.goTo(index);
        $('.pswp__item').show();
      });

      return false;
      
    });
  


function initGallery (container) {
    /**
     * Setup image popups with Photoswipe plugin
     * See documentation in http://photoswipe.com/documentation/options.html
     ==============================================================*/
    var $galleryItem = $(container);
    if (!$galleryItem.length) return;
    var gallery = new PhotoSwipe(container,
      {
        /*
         See here available options
         http://photoswipe.com/documentation/options.html
         */
      }
    );
    
  }
   

   });