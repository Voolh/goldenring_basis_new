function initWideSlider(container) {
    /***************************************************************
     * Initialize sliders on the frontpage
     * See http://kenwheeler.github.io/slick/ for more options
     ==============================================================*/
    var $wideBanner = $(container);
    if (!$wideBanner.length) return;
    var $wideBannerSlider = $wideBanner.find('.js-slick-slider');
    $wideBannerSlider
        .slick({
            rtl: false,
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            lazyLoad: 'progressive',
            prevArrow: $wideBanner.find('.js-banner-prev'),
            nextArrow: $wideBanner.find('.js-banner-next'),
            responsive: [
                {
                    breakpoint: 1300,
                    settings: {
                        centerMode: false,
                        variableWidth: false,
                        arrows: true
                    }
                }
            ]
        });
    $wideBannerSlider.on('setPosition', function (event, slick) {
        $wideBanner.addClass('slider--init');
    });
}

$(document).ready(function () {
	initWideSlider('#slider-wide');
});
