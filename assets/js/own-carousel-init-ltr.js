$('.owl-start').owlCarousel({
	rtl: false,
	loop: true,
	margin: 15,
	navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>'],
	responsiveClass: true,
	responsive: {
		0: {
			items: 1,
			nav: false
		},
		480: {
			items: 1,
			nav: false
		},
		768: {
			items: 3,
			nav: true
		},
		1000: {
			items: 4,
			nav: true,
			loop: false
		}
	}
});

$('.owl-similar').owlCarousel({
	rtl: false,
	loop: true,
	margin: 15,
	navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>'],
	responsiveClass: true,
	responsive: {
		0: {
			items: 1,
			nav: false
		},
		480: {
			items: 1,
			nav: false
		},
		768: {
			items: 2,
			nav: true
		},
		1000: {
			items: 3,
			nav: true,
			loop: false
		}
	}
});