$(document).ready(function () {
    
    $(document).ajaxSend(function () {
        $("#loading").show();
        /*$("#overlay-content").show();
         $("#loading-blocks").show();*/
    }).ajaxComplete(function () {
        $("#loading").hide();
        /*$("#overlay-content").hide();
         $("#loading-blocks").hide();*/
    });

    $('.btn').on('click', function () {
        var color = $(this).find('input').attr('id');
        $('body').attr('class', color);
    });

    $("#search_tab a").click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $("#hot_tabs_link a").click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $(".mobilemenu .chevrond").click(function () {
        $(this).siblings(".collapsible").toggle();
        $(this).toggleClass("hided");
        $(this).parent().next().toggleClass("in");
    });


/*    $('#jquery-accordion-menu > ul > li:has(ul)').addClass("has-sub");

    $('#jquery-accordion-menu > ul > li > a').click(function() {
        var checkElement = $(this).next();

        $('#jquery-accordion-menu li').removeClass('active');
        $(this).closest('li').addClass('active');


        if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            $(this).closest('li').removeClass('active');
            checkElement.slideUp('normal');
        }

        if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
            $('#jquery-accordion-menu ul ul:visible').slideUp('normal');
            checkElement.slideDown('normal');
        }

        if (checkElement.is('ul')) {
            return false;
        } else {
            return true;
        }
    });*/

    //$("#comparison_top").sticky({topSpacing:20, bottomSpacing:$('.footer').innerHeight()+$('.footer_bottom').innerHeight()+200});

    // if ($(document).width() > 768) {
    //     $("#sticker").sticky({
    //         topSpacing: 30,
    //         bottomSpacing: $('.footer').innerHeight() + $('.footer_bottom').innerHeight() + 200
    //     });
    // }
    // else if ($(document).width() > 375) {
    //
    //     $("#sticker").unstick();
    // }
    //
    // window.onresize = function () {
    //     if ($(document).width() > 768) {
    //         $("#sticker").sticky({
    //             topSpacing: 30,
    //             bottomSpacing: $('.footer').innerHeight() + $('.footer_bottom').innerHeight() + 200
    //         });
    //
    //     }
    //     else if ($(document).width() > 375) {
    //
    //         $("#sticker").unstick();
    //     }
    // }

});
