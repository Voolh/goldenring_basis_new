<?php if (issetModule('tariffPlans') && issetModule('paidservices')) : ?>
    <div class="tariff-info panel panel-default">
        <div class="panel-heading">
            <strong><?php echo $name; ?></strong>
        </div>

        <div class="user-tariff-plan-info-detail panel-body">
            <?php if (!$isDefaultTariffPlan && $tariffDateEnd): ?>
                <div class="sbox">
                    <?php $color = ($tariffDateEnd <= date('Y-m-d', strtotime("+5 day"))) ? '#FF0000' : '#008000'; ?>

                    <span class="name-param">
                        <?php echo tc('Date of completion') . ':'; ?>
                    </span>
                    <span class="value-param">
                        <span style="color: <?php echo $color; ?>;"><strong><?php echo $tariffDateEndFormat; ?></strong></span>
                    </span>
                </div>
            <?php endif; ?>

            <div class="sbox">
                <span class="name-param">
                    <?php echo tt('Show_address', 'tariffPlans') . ':'; ?>
                </span>
                <span class="value-param">
                    <?php echo ($showAddress) ? tc("Yes") : tc("No"); ?>
                </span>
            </div>

            <div class="sbox">
                <span class="name-param">
                    <?php echo tt('Show_phones', 'tariffPlans') . ':'; ?>
                </span>
                <span class="value-param">
                    <?php echo ($showPhones) ? tc("Yes") : tc("No"); ?>
                </span>
            </div>

            <div class="sbox">
                <span class="name-param">
                    <?php echo tt('Limit_objects', 'tariffPlans') . ':'; ?>
                </span>
                <span class="value-param">
                    <?php echo ($limitObjects) ? $limitObjects : tt('Unlimited', 'tariffPlans'); ?>
                </span>
            </div>

            <div class="sbox">
                <span class="name-param">
                    <?php echo tt('Limit_photos', 'tariffPlans') . ':'; ?>
                </span>
                <span class="value-param">
                    <?php echo ($limitPhotos) ? $limitPhotos : tt('Unlimited', 'tariffPlans'); ?>
                </span>
            </div>

            <?php if ($this->showChangeTariffLnk): ?>
                <div class="change-tariff-plan-lnk">
                    <?php echo CHtml::link(tc('Change tariff plan'), Yii::app()->createUrl('/tariffPlans/main/index')); ?>
                </div>
            <?php endif; ?>
        </div>

    </div>
<?php endif; ?>