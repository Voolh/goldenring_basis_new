<?php
$isPrintable = (isset($isPrintable)) ? $isPrintable : false;

if ($additionFields) {

    ?>
    <div class="table-responsive">
        <table class="table main-table-additional-tab">
            <tbody>
            	<tr>
            		<td>
                        <?php
                        HFormEditor::renderViewRows($additionFields, $data, $isPrintable);
        
                        ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php
    echo '<div class="clear"></div>';
}