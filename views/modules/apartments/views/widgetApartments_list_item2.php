<?php
if (empty($apartments)) {
    $apartments = HApartment::findAllWithCache($criteria);
}

$findIds = $countImagesArr = array();
foreach ($apartments as $item) {
    $findIds[] = $item->id;
}

if (count($findIds) > 0) {
    $countImagesArr = Images::getApartmentsCountImages($findIds);
}

$p = 0;

?>
<div class="wrapper_app_item">
    <?php foreach ($apartments as $item): ?>
        <?php
        $addClass = $lastClass = '';

        $isLast = ($p % $this->numBlocks) ? false : true;
        //$lastClass = ($isLast) ? 'right_null' : '';

        if (!empty($item->date_up_search) && !is_null($item->date_up_search)) {
            $addClass = 'up_in_search';
        }
        $adUrl = $this->showBooking ? 'javascript:;' : $item->getUrl();
        $url = $item->getUrl();
        $title = CHtml::encode($item->getStrByLang('title'));

        ?>


        <div class="item slow <?php
        echo ($p % 2 == 0) ? 'odd' : '';
        echo $lastClass;

        ?>" data-lat="<?php echo $item->lat; ?>" data-lng="<?php echo $item->lng; ?>"
             data-ap-id="<?php echo $item->id; ?>">

            <div class="in_row">
                <div class="col-md-4 col-sm-4  photo_block">
                    <div class="item-photo item-photo_large">

                        <?php
                        if (isset($item->images) && !empty($item->images)) {
                            if ($item->images) {

                                ?>
                                <a href="<?php echo $url; ?>" class="js-item-slider item-slider large-picture">
                                    <?php
                                    $this->widget('application.modules.images.components.ImagesWidgetAvito', array(
                                        'images' => $item->images,
                                        'objectId' => $item->id,
                                        'width' => 356
                                    ));

                                    ?>
                                </a>
                                <?php
                            }
                        } else {

                            ?>
                            <a href="<?php echo $url; ?>" class="js-item-slider item-slider large-picture">
                                <ul class="item-slider-list js-item-slider-list">
                                    <li class="item-slider-item js-item-slider-item">
                                        <?php
                                        $res = Images::getMainThumb(356, 238, $item->images);
                                        $imgAlt = (isset($res['alt']) && $res['alt']) ? $res['alt'] : CHtml::encode($item->getStrByLang('title'));

                                        ?>
                                        <div class="item-slider-image large-picture"
                                             style="background-image: url(<?php echo $res['thumbUrl']; ?>)"></div>
                                    </li>
                                </ul>
                            </a>
    <?php } ?>

                    </div>

                    <?php if ($item->is_special_offer) { ?>
                        <div class="hot_label"> <i class="fab fa-hotjar"></i><?php echo tc('Special offer!'); ?></div>
    <?php } ?>

                </div>

                <div class="col-md-8 col-sm-8 desc_teaser">
                    <div class="item_teaser_content">
                        <div class="title_item"><?php echo '<a href="' . $url . '" class="slow">#' . $item->id . ', ' . $title . '</a>'; ?></div>
    <?php if ($item['loc_city'] || MetroStations::getApartmentStationsTitle($item->id)): ?>
                            <ul class="adress_info list-unstyled">
                                <li>
                                    <i class="fas fa-map-marker"></i>
                                    <?php echo HApartment::getLocationString($item); ?>
                                    <?php
                                    if ($item->address_ru) {
                                        echo ', ' . $item->address_ru;
                                    }

                                    ?>
                                </li>

                                    <?php if ($item->num_of_rooms || $item->floor || $item->floor_total || $item->square) { ?>
                                    <ul class="list-inline">
                                                <?php if ($item->canShowInView('num_of_rooms')) { ?>
                                            <li> <strong><i class="fas fa-columns"></i><?php echo Yii::t('module_apartments', '{n} bedroom|{n} bedrooms|{n} bedrooms', array($item->num_of_rooms));

                                                    ?></strong></li>
                                        <?php } ?>
                                        <?php
                                        if (($item->floor) && ($item->floor_total)) {
                                            echo '<li><i class="fas fa-building"></i> ';
                                            echo ' <strong>' . tt('Floor', 'apartments') . '/' . tt('Total number of floors', 'apartments');
                                            echo ': </strong>' . $item->floor . '/' . $item->floor_total;
                                            echo '</li>';
                                        } elseif ($item->floor) {
                                            echo '<li><i class="fas fa-building"></i> <strong>' . tt('Floor', 'apartments') . ':</strong> ' . $item->floor . '</li>';
                                        } elseif ($item->floor_total) {
                                            echo '<li><i class="fas fa-building"></i> <strong>' . tt('Total number of floors', 'apartments') . ': </strong>' . $item->floor_total . '</li>';
                                        }

                                        ?>

                                        <?php
                                        if ($item->canShowInView('square')) {
                                            echo '<li><i class="fas fa-arrows-alt"></i> <strong>' . utf8_ucfirst(Yii::t('module_apartments', 'total square: {n}', $item->square)) . " " . tc('site_square') . '</strong></li>';
                                        }

                                        ?>
                                    </ul>
                                <?php } ?>
    <?php endif; ?>


                                <?php if ($item->canShowInView('price')) { ?>
                                <ul class="price list-inline">
                                    <?php
                                    if ($item->is_price_poa) {
                                        echo '<li>' . tt('is_price_poa', 'apartments') . '</li>';
                                    } else {
                                        echo '<li>' . $item->getPrettyPrice(false) . '</li>';
                                    }

                                    ?>
                                </ul>
    <?php } ?>

                        </ul>


                        <div class="descr">
                            <?php
                            $description = $item->getStrByLang('description');
                            echo truncateText($description, 10);

                            ?>

                        </div>
                        <ul class="list-inline info_list">
                            <li>
                                <i class="fas fa-calendar-alt"></i> <?php echo date('Y.m.d', strtotime($item->date_created)); ?>
                            </li>
                            <li><i class="fas fa-user"></i> <strong><?php echo $item->user->username;
                            ;

                            ?></strong></li>
                        <?php if (0) { ?>
                                <li><i class="fas fa-phone"></i> <strong><?php echo $item->user->phone; ?></strong></li>
    <?php } ?>
                        </ul>
    <?php //echo '<p><a href="' . $url . '" class="slow read_more">' . tc('Show more') . '</a></p>';  ?>
                    </div>

                </div>

                    <?php if ($this->showBooking) { ?>
                    <div class="col-md-12 more_block_room">
                        <?php
                        echo CHtml::link(tc('Show more') . ' <i class="fa fa-arrow-down" aria-hidden="true"></i>', 'javascript:;', array(
                            'class' => 'btn btn-default more_room',
                            'onclick' => 'var item=$(this).children();
                     $(this).children().toggleClass("fa-arrow-down fa-arrow-up");
                     $("#more-room-' . $item->id . '").toggle(400);',));

                        ?>
                    </div>

                    <div id="more-room-<?php echo $item->id ?>" style="display: none;" class="col-md-12">
                            <?php if ($item->canShowInView('description')) { ?>
                            <div class="desc">
                                <div class="h3"><?php echo tt('Description', 'apartments'); ?></div>
                                <?php
                                echo $description;

                                ?>
                            </div>
                        <?php } ?>
                        <?php
                        Yii::import('application.modules.referencevalues.models.ReferenceValues');

                        $dataRef = HApartment::getReferenceForCategory($item->id, 17);
                        if ($dataRef) {
                            $refTitle = $dataRef[0]['category_title'];
                            echo '<h4>' . $refTitle . '</h4>';
                            echo '<ul>';
                            foreach ($dataRef as $ref) {
                                echo '<li>' . $ref['value'] . '</li>';
                            }
                            echo '</ul>';
                        }

                        ?>

                        <?php
                        if (issetModule('seasonalprices') && isset($item->seasonalPrices) && $item->seasonalPrices && $item->type == Apartment::TYPE_RENT)
                            echo '<div class="h3">Цены</div>';
                        echo '<div class="ap-view-seasons-prices">' . Yii::app()->controller->renderPartial('//modules/seasonalprices/views/_table', array('apartment' => $item, 'showDeleteButton' => false), true) . '</div>';
                        if (issetModule('bookingcalendar') && $item->type == Apartment::TYPE_RENT) {
                            echo '<div class="h3">Календарь занятости</div>';
                            Yii::app()->controller->renderPartial('//modules/bookingcalendar/views/calendar', array('apartment' => $item));
                        }

                        ?>
                    </div>
    <?php } ?>

                <div class="clear"></div>
            </div>

        </div>
    <?php $p++; ?>

<?php endforeach; ?>
    <div class="clear"></div>
</div>