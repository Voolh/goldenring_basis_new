<?php
Yii::app()->clientScript->registerScript('init-view-tabs', "
		$('ul.object_tabs a').click(function(e){
            e.preventDefault();
            $(this).tab('show');
        });
	", CClientScript::POS_READY);

?>


<div class="b_item_aux">
    <div class="b_item_aux__tabs">
        <?php
        $firstTabsItems = array();

        $generalContent = $this->renderPartial('//modules/apartments/views/_tab_general', array(
            'data' => $model,
            ), true);

        if ($generalContent) {
            $firstTabsItems[tc('General')] = array(
                'content' => $generalContent,
                'id' => 'tabs1_1',
                'active' => false,
            );
        }

        if (!param('useBootstrap')) {
            Yii::app()->clientScript->scriptMap = array(
                'jquery-ui.css' => false,
            );
        }

        if (issetModule('bookingcalendar') && $model->allowShowBookingCalendar()) {
            Bookingcalendar::publishAssets();

            $firstTabsItems[tt('The periods of booking apartment', 'bookingcalendar')] = array(
                'content' => $this->renderPartial('//modules/bookingcalendar/views/calendar', array('apartment' => $model), true),
                'id' => 'tabs1_2',
                'active' => false,
            );
        }

        $additionFields = HFormEditor::getExtendedFields();
        $existValue = HFormEditor::existValueInRows($additionFields, $model);

        if ($existValue) {
            $firstTabsItems[tc('Additional info')] = array(
                'content' => $this->renderPartial('//modules/apartments/views/_tab_addition', array(
                    'data' => $model,
                    'additionFields' => $additionFields
                    ), true),
                'id' => 'tabs1_3',
                'active' => false,
            );
        }

        if (param('enableCommentsForApartments', 1)) {
            if (!isset($comment)) {
                $comment = null;
            }

            $firstTabsItems[Yii::t('module_comments', 'Comments') . ' (' . Comment::countForModel('Apartment', $model->id) . ')'] = array(
                'content' => $this->renderPartial('//modules/apartments/views/_tab_comments', array(
                    'model' => $model,
                    ), true),
                'id' => 'tabs1_4',
                'active' => false,
            );
        }

        if (isset($model->apDocuments) && count($model->apDocuments)) {
            $firstTabsItems[tc('Documents')] = array(
                'content' => $this->renderPartial('//modules/apartments/views/__table_documents_view', array(
                    'apartment' => $model,
                    ), true),
                'id' => 'tabs1_5',
                'active' => false,
            );
        }

        ?>

        <?php if (count($firstTabsItems) > 0): ?>
            <?php
            // выставляем открытым первый таб
            $total = count($firstTabsItems);
            if ($firstTabsItems > 1) {
                $counter = 0;
                foreach ($firstTabsItems as $key => $tab) {
                    $counter++;
                    if ($counter == 1) {
                        $firstTabsItems[$key]['active'] = true;
                    }
                }
            } else {
                $firstTabsItems[0]['active'] = true;
            }

            ?>

            <div class="" id="firsttabs">

                <div class="tab_header">
                    <ul class="nav nav-tabs object_tabs">
                        <?php foreach ($firstTabsItems as $title => $vals): ?>
                            <li <?php echo ($vals['active']) ? 'class="active"' : ''; ?>>
                                <a class="slow" href="#<?php echo $vals['id']; ?>"><?php echo $title; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>

                <div class="tab-content">
                    <?php foreach ($firstTabsItems as $title => $vals): ?>
                        <div class="tab-pane fade in <?php echo $vals['active'] ? 'active' : ''; ?>" id="<?php echo $vals['id']; ?>">
                            <div class="h3 h_line_in"><?php echo $title ?></div>
                            <?php echo $vals['content']; ?>
                        </div>
                    <?php endforeach; ?>
                </div>

            </div>
        <?php endif; ?>

    </div>
</div>
