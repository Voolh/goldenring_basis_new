
<?php
$secondTabsItems = array();

if ($model->type != Apartment::TYPE_BUY && $model->type != Apartment::TYPE_RENTING && $model->lat && $model->lng) {
    if (param('useGoogleMap', 1) || param('useYandexMap', 1) || param('useOSMMap', 1)) {
        $secondTabsItems[tc('Map')] = array(
            'content' => $this->renderPartial('//modules/apartments/views/_tab_map', array('data' => $model), true),
            'id' => 'tab2_1',
            'active' => false,
            'onClick' => 'reInitMap();',
        );
    }
}

if ($model->panorama) {
    $secondTabsItems[tc('Panorama')] = array(
        'content' => $this->renderPartial('//modules/apartments/views/_tab_panorama', array('data' => $model), true),
        'id' => 'tab2_2',
        'active' => false,
    );
}

if (isset($model->video) && $model->video) {
    $secondTabsItems[tc('Videos for listing')] = array(
        'content' => $this->renderPartial('//modules/apartments/views/_tab_video', array('data' => $model), true),
        'id' => 'tab2_3',
        'active' => false,
    );
}

?>

<?php if (count($secondTabsItems) > 0): ?>
    <?php
    // выставляем открытым первый таб
    $total = count($secondTabsItems);
    if ($secondTabsItems > 1) {
        $counter = 0;
        foreach ($secondTabsItems as $key => $tab) {
            $counter++;
            if ($counter == 1) {
                $secondTabsItems[$key]['active'] = true;
            }
        }
    } else {
        $secondTabsItems[0]['active'] = true;
    }

    ?>

    <div class="" id="second_tabs">
        <div class="tab_header">
            <ul class="nav nav-tabs object_tabs">
                <?php foreach ($secondTabsItems as $title => $vals): ?>
                    <li <?php echo ($vals['active']) ? 'class="active"' : ''; ?>>
                        <a class="slow" href="#<?php echo $vals['id']; ?>"><?php echo $title; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>

        <div class="tab-content">
            <?php foreach ($secondTabsItems as $title => $vals): ?>
                <div class="tab-pane fade in <?php echo $vals['active'] ? 'active' : ''; ?>" id="<?php echo $vals['id']; ?>">
                    <div class="h3 h_line_in"><?php echo $title ?></div>
                    <?php echo $vals['content']; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>


