<?
if (issetModule('seasonalprices') && isset($data->seasonalPrices) && $data->seasonalPrices && $data->type == Apartment::TYPE_RENT && Yii::app()->controller->countAdsWidget == 0)
echo '<div class="ap-view-seasons-prices">'.$this->renderPartial('//modules/seasonalprices/views/_table', array('apartment' => $data, 'showDeleteButton' => false), true).'</div>';