<?php
$getForMapSwitch = $_GET;
if (isset($getForMapSwitch['is_ajax'])) {
    unset($getForMapSwitch['is_ajax']);
}

$urlsSwitching = array(
    'block' => z_add_url_get(array('ls' => 'block') + $_GET),
    'table' => z_add_url_get(array('ls' => 'table') + $_GET),
    'map' => z_add_url_get(array('ls' => 'map') + $getForMapSwitch),
);

if (!param('useGoogleMap', 0) && !param('useYandexMap', 0) && !param('useOSMMap', 0) || Yii::app()->controller->useAdditionalView == Themes::ADDITIONAL_VIEW_FULL_WIDTH_MAP) {
    unset($urlsSwitching['map']);
}

if (empty($this->urlSwitching['block'])) {
    unset($urlsSwitching['block']);
}
if (empty($this->urlSwitching['table'])) {
    unset($urlsSwitching['table']);
}
if (empty($this->urlSwitching['map']) && isset($urlsSwitching['map'])) {
    unset($urlsSwitching['map']);
}

$modeListShow = $this->modeListShow ? $this->modeListShow : User::getModeListShow($urlsSwitching);

if (!Yii::app()->request->isAjaxRequest) {

    Yii::app()->clientScript->registerScript('search-params', "
		var updateText = '" . Yii::t('common', 'Loading ...') . "';
		var resultBlock = 'appartment_box';
		var bg_img = '" . Yii::app()->theme->baseUrl . "/images/pages/opacity.png';

		var useGoogleMap = " . param('useGoogleMap', 0) . ";
		var useYandexMap = " . param('useYandexMap', 0) . ";
		var useOSMap = " . param('useOSMMap', 0) . ";
			
		var modeListShow = " . CJavaScript::encode($modeListShow) . ";
		var urlsSwitching = " . CJavaScript::encode($urlsSwitching) . ";
		
		function setListShow(mode){
            modeListShow = mode;
            reloadApartmentList(urlsSwitching[mode]);
        };

        $(function () {
            if(modeListShow == 'map'){
                list.apply();
            }
        });
	", CClientScript::POS_BEGIN);

    // хз почему не работает, возможно из за owl галереи
    $script = <<< JS
    $('div#appartment_box').on('mouseover', 'div.appartment_item', function(event){
        if (event.type == 'mouseover') {
            console.log('over');
            $(this).find('div.apartment_item_edit').show();
        } else {
            console.log('out');
            $(this).find('div.apartment_item_edit').hide();
        }
    });
JS;

    //Yii::app()->clientScript->registerScript('search-edit', $script, CClientScript::POS_READY);
}

?>

<?php
if (empty($this->urlSwitching['block'])) {
    unset($urlsSwitching['block']);
}
if (empty($this->urlSwitching['table'])) {
    unset($urlsSwitching['table']);
}
if (empty($this->urlSwitching['map']) && isset($urlsSwitching['map'])) {
    unset($urlsSwitching['map']);
}

$countString = (isset($count) && $count && $showCount ? ' (' . $count . ')' : '');

$title = ($callFromWidget && $customWidgetTitle) ? $customWidgetTitle : ($this->widgetTitle ? $this->widgetTitle . $countString : '');

if ($title) {
    if (isset($isH1Widget) && $isH1Widget) {
        echo '<div class="title highlight-left-right"><div><h1>' . $title . '</h1></div></div>';
    } else {
        echo '<div class="h3 h_line_in">' . $title . '</div>';
    }
}
?>

<div class="catalog block" id="appartment_box">

    <?php if ($pages && 0): ?>
        <div class="pagination pagination-top">
            <?php
            $this->widget('CLinkPager', array(
                'pages' => $pages,
                'maxButtonCount' => 6,
                'header' => '',
                'selectedPageCssClass' => 'active',
//                'firstPageLabel'=>'<<',
//                'lastPageLabel'=>'>>',
                'nextPageLabel' => '<i class="fas fa-long-arrow-right"></i>',
                'prevPageLabel' => '<i class="fas fa-long-arrow-left"></i>',
//                'lastPageLabel'=>'Последняя',
//                'firstPageLabel'=>'Первая',
                'hiddenPageCssClass' => 'disabled',
                'htmlOptions' => array(
                    'class' => 'pagination',
                    'onClick' => 'reloadApartmentList(this.href); return false;'
                ),
                )
            );

            ?>
        </div>
    <?php endif; ?>

    <?php
    $showSwitcher = $this->showSwitcher && $urlsSwitching;
    $showSorter = $this->showSorter && $sorter && $count;

    ?>
    <?php if ($showSwitcher || $showSorter) { ?>

        <div class="form-group row">

            <?php if ($showSwitcher) { ?>
                <div class="col-md-3 col-xs-12">
                    <div class="btn-group">
                        <a href="<?php echo $urlsSwitching['block']; ?>"
                           class="btn btn-light <?php echo ($modeListShow == 'block') ? 'active' : ''; ?>"
                           onclick="setListShow('block'); return false;"><i class="fas fa-th-large"></i></a>
                        <a href="<?php echo $urlsSwitching['table']; ?>"
                           class="btn btn-light <?php echo ($modeListShow == 'table') ? 'active' : ''; ?>"
                           onclick="setListShow('table'); return false;"><i class="fas fa-table"></i></a>
                           <?php if (array_key_exists('map', $urlsSwitching)) : ?>
                            <a href="<?php echo $urlsSwitching['map']; ?>"
                               class="btn btn-light <?php echo ($modeListShow == 'map') ? 'active' : ''; ?>"><i
                                    class="fas fa-map-marker"></i></a>
                            <?php endif; ?>
                    </div>
                </div>
            <?php } ?>

            <?php if ($showSorter) { ?>
                <div class="col-md-4 col-xs-12 pull-right">
                    <?php $sorter->renderDropDownSorter(apartmentsHelper::getSorterOptions($sorter)); ?>
                </div>
            <?php } ?>

        </div>

    <?php } ?>


    <?php if ($count): ?>
        <?php
        if ($modeListShow == 'block') {
            $this->render('widgetApartments_list_item', array('criteria' => $criteria));
        } elseif ($modeListShow == 'map' && (param('useGoogleMap', 0) || param('useYandexMap', 0) || param('useOSMMap', 0))) {
            $this->render('widgetApartments_list_map', array('criteria' => $criteria));
        } else {

            ?>
            <style>
                .grid-view table.items {
                    border-collapse: collapse;
                    width: 100%;
                }

                @media screen and (max-width: 800px) {
                    #ap-view-table-list td:nth-of-type(1):before {
                        content: "<?php echo CHtml::encode(tc('Main photo')); ?>";
                    }

                    #ap-view-table-list td:nth-of-type(2):before {
                        content: "<?php echo CHtml::encode(tt('Type', 'apartments')); ?>";
                    }

                    #ap-view-table-list td:nth-of-type(3):before {
                        content: "<?php echo CHtml::encode(tt('Apartment title', 'apartments')); ?>";
                    }

                    #ap-view-table-list td:nth-of-type(4):before {
                        content: "<?php echo CHtml::encode(tt('Address', 'apartments')); ?>";
                    }

                    #ap-view-table-list td:nth-of-type(5):before {
                        content: "<?php echo CHtml::encode(tt('Object type', 'apartments')); ?>";
                    }

                    #ap-view-table-list td:nth-of-type(6):before {
                        content: "<?php echo CHtml::encode(tt('Square', 'apartments')); ?>";
                    }

                    #ap-view-table-list td:nth-of-type(7):before {
                        content: "<?php echo CHtml::encode(tt('Price', 'apartments')); ?>";
                    }

                    #ap-view-table-list td:nth-of-type(8):before {
                        content: "<?php echo CHtml::encode(tt('Floor', 'apartments')); ?>";
                    }
                }
            </style>

            <?php
            $dataProvider = new CActiveDataProvider('Apartment', array(
                'criteria' => $criteria,
                'pagination' => false,
            ));

            $canShowAddress = isset($dataProvider->data[0]) ? $dataProvider->data[0]->canShowInView("address") : false;

            $this->widget('CustomGridView', array(
                'id' => 'ap-view-table-list',
                'afterAjaxUpdate' => 'function(){attachStickyTableHeader();}',
                'dataProvider' => $dataProvider,
                'rowCssClassExpression' => '$data->getRowCssClass()',
                'enablePagination' => false,
                'selectionChanged' => 'js:function(id) {
						$currentGrid = $("#"+id);
						$rows = $currentGrid.find(".items").children("tbody").children();
						$selKey = $.fn.yiiGridView.getSelection(id);

						if ($selKey.length > 0) {
							$.each($currentGrid.find(".keys").children("span"), function(i,el){
								if ($(this).text() == $selKey) {
									$(this).attr("data-rel", "selected");
								}
								else {
									$(this).removeAttr("data-rel");
								}
							});
						}

						$.each($currentGrid.find(".keys").children("span"), function(i,el){
							var attr = $(this).attr("data-rel");
							if (typeof attr !== "undefined" && attr !== false) {
								$currentGrid.find(".items").children("tbody").children("tr").eq(i).addClass("selected");
							}
							else {
								$currentGrid.find(".items").children("tbody").children("tr").eq(i).removeClass("selected");
							}
						});

						return false;
					}',
                'template' => '{items}{pager}',
                'columns' => array(
                    array(
                        'header' => '',
                        'type' => 'raw',
                        'value' => 'CHtml::link(Apartment::returnMainThumbForGrid($data), $data->getUrl(), array("title" => $data->getStrByLang("title")))',
                        'htmlOptions' => array('class' => 'ap-view-table-photo'),
                    ),
                    array(
                        'header' => tt('Type', 'apartments'),
                        'value' => 'HApartment::getNameByType($data->type)',
                        'htmlOptions' => array('class' => 'ap-view-table-type'),
                    ),
                    array(
                        'header' => tt('Apartment title', 'apartments'),
                        'type' => 'raw',
                        'value' => 'CHtml::link($data->getTitle(), $data->url)',
                        'htmlOptions' => array('class' => 'ap-view-table-title'),
                    ),
                    array(
                        'header' => tt('Address', 'apartments'),
                        'value' => 'HApartment::getLocationString($data, ", ", true)',
                        'visible' => $canShowAddress,
                        'htmlOptions' => array('class' => 'ap-view-table-address'),
                    ),
                    array(
                        'header' => tt('Object type', 'apartments'),
                        'type' => 'raw',
                        'value' => '$data->getObjType4table()',
                        'htmlOptions' => array('class' => 'ap-view-table-obj-type'),
                    ),
                    array(
                        'header' => tt('Square', 'apartments'),
                        'type' => 'raw',
                        'value' => '$data->getSquareString()',
                        'htmlOptions' => array('class' => 'ap-view-table-square'),
                    ),
                    array(
                        'header' => tt('Price', 'apartments'),
                        'type' => 'raw',
                        'value' => '$data->getPrettyPrice()',
                        'htmlOptions' => array('class' => 'ap-view-table-price'),
                    ),
                    array(
                        'header' => tt('Floor', 'apartments'),
                        'type' => 'raw',
                        'value' => '$data->floor == 0 ? tc("floors").":&nbsp;".$data->floor_total : $data->floor."/".$data->floor_total ;',
                        'htmlOptions' => array('class' => 'ap-view-table-floor'),
                    ),
                )
                )
            );
        }

        ?>
        <div class="clear"></div>
    <?php endif; ?>
</div>

<?php if (!$count): ?>
    <div class="empty"><?php echo Yii::t('module_apartments', 'Apartments list is empty.'); ?></div>
    <div class="clear"></div>
<?php endif; ?>

<?php if ($pages): ?>
    <div class="pagination pagination-bottom">
        <?php
        $this->widget('BasisPager', array(
            'pages' => $pages,
            'maxButtonCount' => 6,
            'header' => '',
            'selectedPageCssClass' => 'active',
            'nextPageLabel' => '<i class="fas fa-angle-right"></i>',
            'prevPageLabel' => '<i class="fas fa-angle-left"></i>',
            'firstPageLabel' => '<i class="fas fa-angle-double-left"></i>',
            'lastPageLabel' => '<i class="fas fa-angle-double-right"></i>',
            'htmlOptions' => array(
                'class' => 'pagination',
            ),
            'htmlOption' => array('onClick' => 'reloadApartmentList(this.href); return false;'),
            )
        );

        ?>
    </div>

<?php endif; ?>
