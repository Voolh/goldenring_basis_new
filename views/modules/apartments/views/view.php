<?php $this->layout = '//layouts/obj_view'; ?>

<?php
$isRTL = Lang::isRTLLang(Yii::app()->language);

Yii::app()->clientScript->registerScript('generate-phone-apartment-' . $model->id, '
		function generatePhone(){
			$.get(\'' . Yii::app()->controller->createUrl('/apartments/main/generatephone', array('id' => $model->id)) . '\', function(data) {
				$("span#owner-phone").html(data);
				$(".phone-show-alert").show();
			});
		}
	', CClientScript::POS_END);


Yii::app()->clientScript->registerScript('reInitMap', '
		var useYandexMap = ' . param('useYandexMap', 1) . ';
		var useGoogleMap = ' . param('useGoogleMap', 1) . ';
		var useOSMap = ' . param('useOSMMap', 1) . ';

		function reInitMap(elem) {
			// place code to end of queue
			if(useGoogleMap){
				setTimeout(function(){
					var tmpGmapCenter = mapGMap.getCenter();

					google.maps.event.trigger($("#googleMap")[0], "resize");
					mapGMap.setCenter(tmpGmapCenter);

					if (($("#gmap-panorama").length > 0)) {
						initializeGmapPanorama();
					}
				}, 0);
			}

			if(useYandexMap){
				setTimeout(function(){
					ymaps.ready(function () {
						globalYMap.container.fitToViewport();
						globalYMap.setCenter(globalYMap.getCenter());
					});
				}, 0);
			}

			if(useOSMap){
				setTimeout(function(){
					L.Util.requestAnimFrame(mapOSMap.invalidateSize,mapOSMap,!1,mapOSMap._container);
				}, 0);
			}
		}

	', CClientScript::POS_END, array(), true);

$model->references = HApartment::getFullInformation($model->id, $model->type);
$img = null;

$title = $model->getTitle();

$this->pageTitle .= ' - ' . $title;
if (isset($model->city) && isset($model->city->name)) {
    $this->pageTitle .= ', ' . tc('City') . ' ' . $model->city->name;
}

if ($model->getStrByLang('description')) {
    $this->pageDescription = truncateText($model->getStrByLang('description'), 20);
}

$searchUrl = Yii::app()->user->getState('searchUrl');
if ($searchUrl) {
    $this->breadcrumbs = array(
        Yii::t('common', 'Apartment search') => $searchUrl,
        truncateText($title, 10),
    );
} else {
    $this->breadcrumbs = array(
        Yii::t('common', 'Apartment search') => array('/quicksearch/main/mainsearch'),
        truncateText($title, 10),
    );
}

?>

<?php
$baseThemeUrl = Yii::app()->theme->baseUrl;

?>

<div class="full_property">

    <div class="row">

        <div class="col-md-9 col-sm-12">

            <h1 class="h_line title_property">#<?= $model->id ?>, <?= $title ?></h1>

            <ul class="list-inline">
                <?php if ($model->is_special_offer) { ?>
                    <li>
                        <div class="hot_label">
                            <i class="fab fa-hotjar"></i><?php echo tc('Special offer!') ?>
                            <?php
                            if (!empty($model->is_free_to) && !is_null($model->is_free_to)) {
                                echo Yii::t('common', 'Is avaliable');
                                echo ' ' . Yii::t('module_apartments', 'to');
                                echo ' ' . Booking::getDate($model->is_free_to, 1);
                            }

                            ?>
                        </div>
                    </li>

                <?php } ?>
                <li>


                    <?php if (issetModule('comparisonList')) { ?>

                        <?php
                        $inComparisonList = false;
                        if (in_array($model->id, Yii::app()->controller->apInComparison)) {
                            $inComparisonList = true;
                        }

                        ?>
                        <div class="compare_item2"
                             data-id="compare_check_control_<?php echo $model->id; ?>">

                            <div class="pretty p-default">
                                <?php
                                $checkedControl = '';

                                if ($inComparisonList) {
                                    $checkedControl = ' checked = checked ';
                                }

                                ?>
                                <input type="checkbox" name="compare<?php echo $model->id; ?>"
                                       class="compare-check compare-float-left"
                                       data-id="compare_check<?php echo $model->id; ?>" <?php echo $checkedControl; ?>>

                                <div class="state p-warning">
                                    <label>
                                        <a href="<?php echo ($inComparisonList) ? Yii::app()->createUrl('comparisonList/main/index') : 'javascript:void(0);'; ?>"
                                           data-rel-compare="<?php echo ($inComparisonList) ? 'true' : 'false'; ?>"
                                           data-id="compare_label<?php echo $model->id; ?>"
                                           class="compare-label">
                                               <?php
                                               echo ($inComparisonList) ? tt('In the comparison list', 'comparisonList') : tt('Add to a comparison list ', 'comparisonList');

                                               ?>
                                        </a>
                                    </label>
                                </div>
                            </div>

                        </div>

                    <?php } ?>
                </li>

                <?php
                if ($model->deleted) {
                    echo '<li><div class="name deleted">' . tt('Listing is deleted', 'apartments') . '</div></li>';
                }

                $editUrl = $model->getEditUrl();
                if ($editUrl) {
                    echo '<li>' . CHtml::link('<i class="fa fa-edit" title="' . tt('Update apartment', 'apartments') . '"></i>', $editUrl) . '</li>';
                }
                $imgPrint = '<i class="fa fa-print" title="' . tc('Print version') . '"></i>';
                echo '<li>' . CHtml::link($imgPrint, $model->getUrl() . '?printable=1', array('target' => '_blank')) . '</li>';

                ?>
            </ul>

            <?php
            if (isset($model->images) && !empty($model->images)) {
                if ($model->images) {
                    $this->widget('application.modules.images.components.ImagesWidget', array(
                        'images' => $model->images,
                        'objectId' => $model->id,
                    ));
                }
            } else {
                $res = Images::getMainThumb(893, 492, $model->images);
                $imgAlt = (isset($res['alt']) && $res['alt']) ? $res['alt'] : CHtml::encode($model->getStrByLang('title'));

                $img = CHtml::image($res['thumbUrl'], $imgAlt);
                if ($res['link']) {
                    echo CHtml::link($img, $res['link'], array(
                        'data-gal' => 'prettyPhoto[img-gallery]',
                        'title' => $imgAlt,
                    ));
                } else {
                    echo $img;
                }
            }

            include '_view_first_tabs.php';

            ?>

        </div>

        <div class="col-md-3 col-sm-12">
            <?php include '_view_column.php' ?>
        </div>

    </div>

    <?php if ($model->objType->with_obj) { ?>
        <div class="child_obj_list">
            <?php
            $criteria = new CDbCriteria();
            $criteria->compare('t.parent_id', $model->id);

            $useIndex = 'FORCE INDEX (type_priceType_halfActive)';
            if (param('useUserads')) {
                $useIndex = 'FORCE INDEX (type_priceType_fullActive)';
            }

            // hack
            $criteria->join = $useIndex;

            $this->widget('application.modules.apartments.components.ApartmentsWidget', array(
                'criteria' => $criteria,
                'widgetTitle' => tc('child_section_' . $model->objType->id),
                'numBlocks' => 3,
                'usePagination' => 0,
                'showSorter' => 0,
                'showIfNone' => 0,
                'showChild' => true,
                'showSwitcher' => 0,
                'urlSwitching' => array('block' => 1, 'table' => 1),
            ));

            ?>
        </div>
        <div class="clearfix"></div>
        <br/>
    <?php } ?>

    <?php include '_view_second_tabs.php' ?>

</div>


<?php
if (issetModule('similarads') && param('useSliderSimilarAds') == 1) {
    Yii::import('application.modules.similarads.components.SimilarAdsWidget');
    $ads = new SimilarAdsWidget;
    $ads->viewSimilarAds($model);
}

?>

<?php
if (param('useSchemaOrgMarkup')) {
    $jsonLDMain = array();
    $jsonLDMain['@context'] = 'http://schema.org';
    $jsonLDMain['@type'] = 'Offer';
    $jsonLDMain['name'] = CHtml::encode($model->getTitle());
    $jsonLDMain['description'] = strip_tags($model->getStrByLang("description"));
    if (!$model->is_price_poa) {
        $jsonLDMain['price'] = $model->getPriceFrom();
        $jsonLDMain['priceCurrency'] = (issetModule('currency')) ? Currency::getCurrentCurrencyModel()->char_code : param('siteCurrency', '$');
    }

    if (isset($model->images) && !empty($model->images)) {
        $res = Images::getMainThumb(640, 400, $model->images);
        $imgAlt = (isset($res['alt']) && $res['alt']) ? $res['alt'] : CHtml::encode($model->getStrByLang('title'));
        $img = CHtml::image($res['thumbUrl'], $imgAlt);

        $jsonLDMain['image'] = array(
            '@type' => 'ImageObject',
            'url' => $img,
            'height' => 400,
            'width' => 640
        );
    }
    echo '<script type="application/ld+json">' . CJavaScript::jsonEncode($jsonLDMain) . '</script>';

    if ($model->lat && $model->lng) {
        $jsonLDCoordinates = array();
        $jsonLDCoordinates['@context'] = 'http://schema.org';
        $jsonLDCoordinates['@type'] = 'GeoCoordinates';
        $jsonLDCoordinates['latitude'] = $model->lat;
        $jsonLDCoordinates['longitude'] = $model->lng;

        echo '<script type="application/ld+json">' . CJavaScript::jsonEncode($jsonLDCoordinates) . '</script>';
    }


    if ($model->rating) {
        $jsonLDRating = array();
        $jsonLDRating['@context'] = 'http://schema.org';
        $jsonLDRating['@type'] = 'AggregateRating';
        $jsonLDRating['ratingValue'] = (int) $model->rating;
        $jsonLDRating['bestRating'] = 10;
        $jsonLDRating['worstRating'] = 1;
        $commentCount = Comment::countForModel('Apartment', $model->id);
        $jsonLDRating['reviewCount'] = ($commentCount && $commentCount > 1) ? $commentCount : 1;
        $jsonLDRating['itemReviewed'] = array(
            '@type' => 'Offer',
            'name' => CHtml::encode($model->getTitle()),
            'url' => $model->getUrl()
        );

        echo '<script type="application/ld+json">' . CJavaScript::jsonEncode($jsonLDRating) . '</script>';
    }

    if ($model->canShowInView('address')) {
        $jsonLDAddress = array();
        $isShowAddress = true;

        # Search engine crawlers is "GUEST"
        if (Yii::app()->user->isGuest && issetModule('tariffPlans') && issetModule('paidservices')) {
            $defaultTariffInfo = TariffPlans::getFullTariffInfoById(TariffPlans::DEFAULT_TARIFF_PLAN_ID);
            if (!$defaultTariffInfo['showAddress']) {
                $isShowAddress = false;
            }
        }

        if ($isShowAddress) {
            if (issetModule('location') && isset($model->locCountry) && isset($model->locRegion) && isset($model->locCity)) {
                $jsonLDAddress['@context'] = 'http://schema.org';
                $jsonLDAddress['@type'] = 'PostalAddress';
                $jsonLDAddress['addressCountry'] = $model->locCountry->getStrByLang('name');
                $jsonLDAddress['addressRegion'] = $model->locRegion->getStrByLang('name');
                $jsonLDAddress['addressLocality'] = $model->locCity->getStrByLang('name');
                $jsonLDAddress['streetAddress'] = CHtml::encode($model->getStrByLang('address'));
            } elseif (isset($model->city)) {
                $jsonLDAddress['@context'] = 'http://schema.org';
                $jsonLDAddress['@type'] = 'PostalAddress';
                $jsonLDAddress['addressLocality'] = CHtml::encode($model->city->name);
                $jsonLDAddress['streetAddress'] = CHtml::encode($model->getStrByLang('address'));
            }

            echo '<script type="application/ld+json">' . CJavaScript::jsonEncode($jsonLDAddress) . '</script>';
        }
    }
}