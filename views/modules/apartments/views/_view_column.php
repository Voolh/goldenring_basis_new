<?php
/** @var $model Apartment */

?>

<div class="properties_list box widget">
    <div class="h_line h3"><?php echo tt('General information', 'theme_basis') ?></div>

    <ul class="list-unstyled">
        <li><i class="fas fa-map-marker"></i> <strong><?php echo HApartment::getLocationString($model) ?></strong></li>
        <li>
            <ul class="list-inline">
                <?php if ($model->canShowInView('square')) { ?>
                    <li><i class="fas fa-arrows-alt"></i> <strong><?php echo utf8_ucfirst(Yii::t('module_apartments', 'total square: {n}', $model->square)) . " " . tc('site_square');

                    ?></strong></li>
                <?php } ?>
                <li><i class="fas fa-columns"></i>
                    <strong><?php echo utf8_ucfirst($model->objType->name) ?></strong><?php if ($model->canShowInView('num_of_rooms')):?>, <strong><?php echo Yii::t('module_apartments', '{n} bedroom|{n} bedrooms|{n} bedrooms', array($model->num_of_rooms));?></strong><?php endif;?>
                </li>
            </ul>
        </li>
    </ul>

    <?php if ($model->canShowInView('price')) { ?>
        <ul class="list-unstyled">
            <li>
                <?php
                if ($model->is_price_poa) {
                    echo tt('is_price_poa', 'apartments');
                } else {
                    echo '<i class="fas fa-building"></i> <strong>' . HApartment::getNameByType($model->type) . '</strong> — <span class="price">' . $model->getPrettyPrice(false) . '</span>';
                }

                ?>
            </li>
        </ul>
    <?php } ?>

</div>

<?php if (param('useShowUserInfo')) { ?>
    <?php $owner = $model->user; ?>
    <div class="box widget agent_info">
        <div class="h3 h_line"><?php echo tc('Listing provided by'); ?></div>
        <div class="text-center">
            <?php if ($model->parse_from):?>
                <img alt="<?php echo $model->parse_owner_info_name;?>" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ava-default.jpg" class="message_ava">
            <?php else:?>
                <?php echo $owner->renderAva(true, '', true, false); ?>
            <?php endif;?>
            
            <ul class="list-unstyled text-center">
                <?php if ($model->parse_from):?>
                    <li class="h4"><strong><?php echo $model->parse_owner_info_name;?></strong></li>
                <?php else:?>
                    <li class="h4"><strong><?php echo $owner->getNameForType(); ?></strong></li>
                <?php endif;?>
                    
                <?php
                if ($model->canShowInView('phone')) {
                    if (issetModule('tariffPlans') && issetModule('paidservices') && ($model->owner_id != Yii::app()->user->id)) {
                        if (Yii::app()->user->isGuest) {
                            $defaultTariffInfo = TariffPlans::getFullTariffInfoById(TariffPlans::DEFAULT_TARIFF_PLAN_ID);

                            if (!$defaultTariffInfo['showPhones']) {
                                echo '<li class="li1">' . Yii::t('module_tariffPlans', 'Please <a href="{n}">login</a> to view', Yii::app()->controller->createUrl('/site/login')) . '</li>';
                            } else {
                                echo '<li class="li1"><span id="owner-phone">' . CHtml::link(tc('Show phone'), 'javascript: void(0);', array('onclick' => 'generatePhone();')) . '</span>' . '</li>';
                            }
                        } else {
                            if (TariffPlans::checkAllowShowPhone())
                                echo '<li class="li1"><span id="owner-phone">' . CHtml::link(tc('Show phone'), 'javascript: void(0);', array('onclick' => 'generatePhone();')) . '</span>' . '</li>';
                            else
                                echo '<li class="li1">' . Yii::t('module_tariffPlans', 'Please <a href="{n}">change the tariff plan</a> to view', Yii::app()->controller->createUrl('/tariffPlans/main/index')) . '</li>';
                        }
                    } else {
                        echo '<li class="li1"><span id="owner-phone">' . CHtml::link(tc('Show phone'), 'javascript: void(0);', array('onclick' => 'generatePhone();')) . '</span>' . '</li>';
                    }
                }

                ?>

                <?php
                if ($model->canShowInView('phone')) {
                    $hostname = IdnaConvert::checkDecode(str_replace(array('https://', 'http://', 'www.'), '', Yii::app()->getRequest()->getHostInfo()));
                    echo '<div class="flash-notice phone-show-alert" style="display: none;">' . Yii::t('common', 'Please tell the seller that you have found this listing here {n}', '<strong>' . $hostname . '</strong>') . '</div>';
                }
                ?>

                <?php if (!$model->parse_from):?>
                    <?php echo '<li class="li3">' . $owner->getLinkToAllListings() . '</li>'; ?>
                <?php endif; ?>
            </ul>
        </div>
        <ul class="list-unstyled user_link">
            <?php
            if (!$model->parse_from) {
                if (issetModule('messages') && $model->owner_id != Yii::app()->user->id && !Yii::app()->user->isGuest) {
                    echo '<li>' . CHtml::link(tt('Send message', 'messages'), Yii::app()->createUrl('/messages/main/read', array('id' => $owner->id, 'apId' => $model->id)), array('class' => 'btn btn-primary')) . '</li>';
                } elseif (param('use_module_request_property') && $model->owner_id != Yii::app()->user->id) {
                    echo '<li>' . CHtml::link(tt('request_for_property'), $model->getUrlSendEmail(), array('class' => 'fancy mgp-open-ajax btn btn-primary')) . '</li>';
                }
            }
            ?>

            <?php
            if (($model->owner_id != Yii::app()->user->getId()) && $model->allowShowBookingCalendar()) {
                echo '<li>' . CHtml::link(tt('Booking'), array('/booking/main/bookingform', 'id' => $model->id), array('class' => 'bron fancy btn btn-default')) . '</li>';
            }

            ?>
            <?php if (issetModule('apartmentsComplain') && ($model->owner_id != Yii::app()->user->getId())) { ?>
                <?php echo '<li>' . CHtml::link(tt('do_complain', 'apartmentsComplain'), $this->createUrl('/apartmentsComplain/main/complain', array('id' => $model->id)), array('class' => 'fancy mgp-open-ajax')) . '</li>'; ?>
            <?php } ?>
        </ul>
    </div>
<?php } ?>

<div class="box widget qrcode_block">

    <?php if (param('qrcode_in_listing_view', 1)): ?>
        <?php
        $url = $model->getUrl();
        echo '<div class="qr-code text-center">';
        $this->widget('application.extensions.qrcode.QRCodeGenerator', array(
            'data' => $url,
            'filename' => 'qr_' . md5($url) . '-' . $model->id . Yii::app()->language . '.png',
            'matrixPointSize' => 3,
            'fileUrl' => Yii::app()->getBaseUrl(true) . '/uploads',
            //'color' => array(33, 72, 131),
            'color' => array(0, 0, 0),
        ));
        echo '</div>';

        ?>
    <?php endif; ?>

    <?php if (isset($statistics) && is_array($statistics)) : ?>
        <br/>
        <ul class="list-unstyled user_link">
            <li><?php echo tt('views_all') . ': ' . $statistics['all'] ?></li>
            <li><?php echo tt('views_today') . ': ' . $statistics['today']; ?></li>
            <li><?php echo tc('Date created') . ': <span class="nobr">' . $model->getDateTimeInFormat('date_created') . '</span>'; ?></li>
        </ul>
    <?php endif; ?>
</div>

<?php
$wantTypes = HApartment::getI18nTypesArray();
$typeName = (isset($wantTypes[$model->type]) && isset($wantTypes[$model->type]['current'])) ? mb_strtolower($wantTypes[$model->type]['current'], 'UTF-8') : '';

?>

<?php if (issetModule('paidservices') && param('useUserads') && $typeName) { ?>
    <div class="alert alert-warning promotion-paidservices-in-apartment">
        <div class="h4"><strong><?php echo tt('Is it your listing?', 'apartments'); ?></strong></div>
        <p>
            <?php echo tt('Would you like to', 'apartments'); ?>&nbsp;<?php echo $typeName; ?>&nbsp;<?php echo tt('quicker?', 'apartments'); ?>
        </p>
        <p>
            <?php
            echo CHtml::link(tt('apply paid services', 'apartments'), Yii::app()->createUrl('/userads/main/update', array('id' => $model->id, 'show' => 'paidservices')), array());

            ?>
        </p>
    </div>
<?php } ?>
