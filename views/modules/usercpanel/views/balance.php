<?php
$this->pageTitle .= ' - ' . tc('My balance');
$this->breadcrumbs = array(
    tc('Control panel') => Yii::app()->createUrl('/usercpanel'),
    tc('My balance'),
);

$user = HUser::getModel();

echo BasisUserPanelHelper::title(tc('My balance'));

?>

<div class="clear"></div><br />

<?php
echo '<strong>' . tc('On the balance') . ': ' . $user->balance . ' ' . Currency::getDefaultCurrencyName() . '</strong>';
echo '<br /><br />';

echo CHtml::link(tt('Replenish the balance'), Yii::app()->createUrl('/paidservices/main/index', array('paid_id' => PaidServices::ID_ADD_FUNDS)), array('class' => 'fancy btn btn-primary'));
