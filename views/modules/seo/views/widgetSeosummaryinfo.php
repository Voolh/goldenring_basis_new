<?php if (!empty($citiesListResult)): ?>
    <div class="summary-site-ads-information">

        <div class="row">
            <?php foreach ($citiesListResult as $cityId => $cityValue): ?>

                <?php
                if (issetModule('location')) {
                    $city = City::model()->findByPk($cityId);
                } else {
                    $city = ApartmentCity::model()->findByPk($cityId);
                }

                $imgSrc = $city->image ? $city->image->getThumbHref(150, 115) : Yii::app()->baseUrl . '/common/images/150x100_no_photo_city.jpg';
                $imgAlt = ($city->image && $city->image->seo) ? $city->image->seo->alt : '';

                ?>

                <div class="col-md-6 col-sm-6">
                    <div class="item-info">
                        <div class="row">
                            <?php if ($imgSrc) { ?>
                                <div class="col-md-5 col-sm-5">
                                    <img src="<?php echo $imgSrc ?>" alt="<?php echo $imgAlt ?>" class="img_city">
                                </div>
                            <?php } ?>
                            <div class="<?php echo ($imgSrc) ? "col-md-7 col-sm-7" : "col-md-12 col-sm-12"; ?>">
                                <div class="box-heading">
                                    <?php
                                    echo CHtml::link(
                                        '<strong>' . $cityValue[Yii::app()->language]['name'] . '</strong>', Yii::app()->controller->createUrl('/infopages/main/view', array(
                                            'cityUrlName' => translit($cityValue[Yii::app()->language]['name'])
                                            )
                                        )
                                    );

                                    ?>
                                </div>
                                <?php if (!empty($objTypesListResult)): ?>
                                    <ul class="summary-info-obj-types list-inline">
                                        <?php foreach ($objTypesListResult as $objTypeId => $objValue): ?>

                                            <li>
                                                <?php
                                                $linkName = $objValue[Yii::app()->language]['name'];
                                                $addCount = '';
                                                $class = 'inactive-obj-type-url';
                                                if (!empty($countApartmentsByCategories)) {
                                                    if (isset($countApartmentsByCategories[$cityId]) && isset($countApartmentsByCategories[$cityId][$objTypeId])) {
                                                        $class = 'active-obj-type-url';
                                                        $addCount = '<span class="obj-type-count">(' . $countApartmentsByCategories[$cityId][$objTypeId] . ')</span>';
                                                    }
                                                }

                                                $linkParams = array(
                                                    'cityUrlName' => $cityValue[Yii::app()->language]['url'],
                                                    'objTypeUrlName' => $objValue[Yii::app()->language]['url'],
                                                );

                                                if (!empty($paramsType)) {
                                                    $linkParams['apType'] = $paramsType;
                                                }

                                                echo CHtml::link(
                                                    $linkName, Yii::app()->controller->createUrl('/seo/main/viewsummaryinfo', $linkParams), array('class' => $class)
                                                ) . $addCount;

                                                ?>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>


                </div>

                <?php //$i++;  ?>
            <?php endforeach; ?>
        </div>

    </div>

<?php endif; ?> 




