<?php
$this->pageTitle .= ' - ' . ReviewsModule::t('Reviews');
$this->breadcrumbs = array(
    ReviewsModule::t('Reviews'),
);

?>

<div class="title highlight-left-right">
    <div>
        <h1><?php echo ReviewsModule::t('Reviews'); ?></h1>
    </div>
</div>
<div class="clear"></div><br />


<?php if (count($reviews) > 3) { ?>
    <div class="add-review-link">
        <?php echo CHtml::link(tt('Add_feedback', 'reviews'), Yii::app()->createUrl('/reviews/main/add'), array('class' => 'btn btn-primary fancy mgp-open-ajax')); ?>
    </div>
<?php } ?>

<?php if ($reviews) : ?>
    <div id="reviews-wrap">

        <?php foreach ($reviews as $review) : ?>
            <div class="alert alert-info">	
                <div class="review1 row" id="li-review-<?php echo $review->id; ?>">
                    <div class="col-md-2">
                        <div class="review-author vcard"><strong><?php echo CHtml::encode($review->name); ?></strong></div>
                        <div class="review-meta reviewmetadata">
                            <?php //deb($review->date_created); ?>
                            <span class="review-date"><?php echo date('d/m/Y', strtotime($review->date_created)); ?></span>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div id="review-<?php echo $review->id; ?>" class="review-body clearfix">
        <!-- <span class="icon-avatar-review"></span> -->


                            <div class="review-inner">
                                <p><?php echo CHtml::encode($review->body); ?></p>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        <?php endforeach; ?>

    </div>

<?php endif; ?>

<?php if (!$reviews) : ?>
    <div><?php echo tt('Review list is empty'); ?></div>
<?php endif; ?>

<div class="add-review-link">
    <?php echo CHtml::link(tt('Add_feedback', 'reviews'), Yii::app()->createUrl('/reviews/main/add'), array('class' => 'btn btn-primary fancy mgp-open-ajax')); ?>
</div>

<?php if ($pages && $pages->pageCount > 1): ?>
    <div class="pagination pagination-bottom">
        <?php
        $this->widget('BasisPager', array(
            'pages' => $pages,
            'maxButtonCount' => 6,
            'header' => '',
            'selectedPageCssClass' => 'active',
            'nextPageLabel' => '<i class="fas fa-angle-right"></i>',
            'prevPageLabel' => '<i class="fas fa-angle-left"></i>',
            'firstPageLabel' => '<i class="fas fa-angle-double-left"></i>',
            'lastPageLabel' => '<i class="fas fa-angle-double-right"></i>',
            'htmlOptions' => array(
                'class' => 'pagination',
            ),
        ));
        ?>
    </div>
    <div class="clear"></div>
<?php endif; ?>