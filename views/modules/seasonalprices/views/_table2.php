<div class="grid-view table-responsive">
    <?php
    echo '<table class="table table-bordered">';

    echo '<thead>';
    echo '<tr>';
    foreach ($apartment->seasonalPrices as $season) {
        echo '<th class="text-center">';
        echo $season->{'name_' . Yii::app()->language} . '<br>';
        echo $season->dateStart . '- ' . $season->dateEnd;
        //echo $season->min_rental_period_with_type;
        echo '</th>';
    }
    echo '</tr>';
    echo '</thead>';

    echo '<tbody>';
    echo '<tr>';
    $i = 1;
    foreach ($apartment->seasonalPrices as $season) {
        $class = $i % 2 ? 'odd' : 'even';
        echo '<td class="text-center warning ' . $class . '">';
        echo $season->priceWithType;
        echo '</td>';
        $i++;
    }
    echo '</tr>';
    echo '</tbody>';

    echo '</table>';

    ?>
</div>
