<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages': ['corechart']});
</script>

<div class="flashes" style="display: none;"></div>

<?php
$room = new Apartment('search');
$room->unsetAttributes();  // clear any default values
if (isset($_GET['Apartment'])) {
    $room->attributes = $_GET['Apartment'];
}
$room->onlyAuthOwner()->notDeleted()->forParent($model->id);

Yii::app()->clientScript->registerScript('ajaxSetStatus', "
		function ajaxSetStatus(elem, id, isUpdate){
			var isUpdate = isUpdate || 1;

			$.ajax({
				url: $(elem).attr('href'),
				success: function(result){
					$('.flashes').hide();

					if (isUpdate == 2) {
						$('.flashes').show();
						$('.flashes').html(result);
						$('#'+id).yiiGridView.update(id);
					}
					else {
						$('#'+id).yiiGridView.update(id);
					}
				}
			});
		}
	", CClientScript::POS_HEAD);


$columns = array(
    array(
        'name' => 'id',
        'headerHtmlOptions' => array(
            'class' => 'apartments_id_column',
        ),
        'sortable' => false,
        'filter' => false,
    ),
    array(
        'name' => 'owner_active',
        'type' => 'raw',
        'value' => 'UserAds::returnStatusOwnerActiveHtml($data, "userads-grid", 1)',
        'headerHtmlOptions' => array(
            'class' => 'status_column',
        ),
        'filter' => false,
        'sortable' => false,
    ),
    array(
        'header' => tc('Name'),
        'name' => 'title_' . Yii::app()->language,
        'type' => 'raw',
        'value' => 'HApartment::getNameForGrid($data)',
        'sortable' => false,
        'filter' => false,
    ),
//    array(
//        'name' => 'obj_type_id',
//        'type' => 'raw',
//        'value' => '(isset($data->objType) && $data->objType) ? $data->objType->name : ""',
//        /*'htmlOptions' => array(
//            'style' => 'width: 100px;',
//        ),*/
//        'filter' => false,
//        'sortable' => false,
//    ),
    array(
        'name' => 'price',
        'type' => 'raw',
        'value' => '$data->getPrettyPrice(false)',
//		'htmlOptions' => array(
//			'style' => 'width: 100px;',
//		),
        'filter' => false,
        'sortable' => false,
    ),
);

$columns[] = array(
    'class' => 'CButtonColumn',
    'template' => (Yii::app()->user->type == User::TYPE_AGENCY) ?
    '<div class="t-but"><div class="t-but"><span class="t-but-u">{update}</span></div><div class="t-but"><span class="t-but-d">{delete}</span></div><div class="t-but"><span class="t-but-c">{clone}</span></div><div class="t-but"><span class="t-but-c-o">{change_owner}</span></div>' : '<div class="t-but"><div class="t-but"><span class="t-but-u">{update}</span></div><div class="t-but"><span class="t-but-d">{delete}</span></div><div class="t-but"><span class="t-but-c">{clone}</span></div></div>',
    'deleteConfirmation' => tc('Are you sure you want to delete this item?'),
    'viewButtonUrl' => '$data->getUrl()',
    'afterDelete' => 'function(link,success,data){ /*$(".flashes").show(); $(".flashes").html(data);*/ $(".flashes").hide(); }',
    'buttons' => array(
        'update' => array(
            'url' => 'Yii::app()->createUrl("/userads/main/update", array("id" => $data->id))',
        ),
        'delete' => array(
            'url' => 'Yii::app()->createUrl("/userads/main/delete", array("id" => $data->id))',
        ),
        'clone' => array(
            'label' => tc('Clone'),
            'imageUrl' => Yii::app()->theme->baseUrl . '/images/default/copy.png',
            'url' => 'Yii::app()->createUrl("/userads/main/clone", array("id" => $data->id))',
            'visible' => 'param("enableUserAdsCopy",0)',
            'click' => "js: function() { ajaxRequest($(this).attr('href'), 'userads-grid'); return false;}",
        ),
        'change_owner' => array(
            'label' => '',
            'imageUrl' => Yii::app()->theme->baseUrl . '/images/default/change-owner.png',
            'url' => 'Yii::app()->createUrl("userads/main/choosenewowner", array("id" => $data->id))',
            'options' => array('class' => 'icon-user', 'title' => tt('Set the owner of the listing', 'apartments')),
        ),
    ),
    'htmlOptions' => array(
        'class' => 'width70 center',
    ),
);

$this->widget('CustomGridView', array(
    'id' => 'ap-view-table-list',
    'afterAjaxUpdate' => 'function(){attachStickyTableHeader();}',
    'dataProvider' => $room->search(),
    //'filter'=>false,
    'columns' => $columns,
    'template' => "{items}\n{pager}",
    /* 	'htmlOptions' => array(
      'class' => 'picker__table',
      ), */
));

echo CHtml::link(tc('Добавить номер'), Yii::app()->createUrl('/userads/main/create', array('for' => $model->id)), array('class' => 'button colored small-text right-align'));
