<?php
// Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/unitegallery/js/unitegallery.min.js', CClientScript::POS_END);
// Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/js/unitegallery/css/unite-gallery.css');
// //Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/unitegallery/themes/tiles/ug-theme-tiles.js', CClientScript::POS_END);
// Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/unitegallery/themes/tilesgrid/ug-theme-tilesgrid.js', CClientScript::POS_END);
// //Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/js/unitegallery/themes/default/ug-theme-default.css');
// Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/slick/slick.min.js', CClientScript::POS_BEGIN);
// Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/js/slick/slick.css');
// Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/js/slick/slick-theme.css');

Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/owl2/owl.carousel.min.js', CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/js/owl2/assets/owl.carousel.min.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/js/owl2/assets/owl.theme.default.css');


$count = 1;

if ($this->images) {

    ?>
    <div  class="gallery<?php echo $this->objectId; ?> owl-carousel owl-theme">
        <?php
        foreach ($this->images as $image) {

// $imgTag = CHtml::image(Images::getThumbUrl($image, 112, 87), Images::getAlt($image));
// echo CHtml::link($imgTag, Images::getFullSizeUrl($image), array(
//     'title' => Images::getAlt($image),
//     'data-image'=> Images::getFullSizeUrl($image),
//     'alt' => Images::getAlt($image),
// ));
// $imgTag = CHtml::image(Images::getFullSizeUrl($image), Images::getAlt($image));
//                     echo $imgTag;
// $imgTag = CHtml::image(Images::getFullSizeUrl($image), Images::getAlt($image));
//                     echo CHtml::link($imgTag, Images::getFullSizeUrl($image), array(
//                         'title' => Images::getAlt($image),
//                         'data-image'=> Images::getFullSizeUrl($image),
//                         'alt' => Images::getAlt($image),
//                     ));

            ?>

            <a href="<?php echo Images::getFullSizeUrl($image); ?>"  data-lightbox="set<?php echo $this->objectId; ?>">
                <img src="<?php echo Images::getThumbUrl($image, 420, 230); ?>" alt="<?php echo Images::getAlt($image); ?>" data-image="<?php echo Images::getFullSizeUrl($image); ?>" >
            </a> 

            <?php // $count++; if($count>5) break;  ?>
        <?php } ?>

    </div>
    <script type="text/javascript">
        var api;
        jQuery(document).ready(function () {

            $('.gallery<?php echo $this->objectId; ?>').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                dots: false,
                navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
                responsive: {
                    0: {
                        items: 1,
                        nav: true
                    },
                    600: {
                        items: 1,
                        nav: false
                    },
                    1000: {
                        items: 1,
                        nav: true,
                        loop: false
                    }
                }
            })

        });
    </script>
<?php } ?>

