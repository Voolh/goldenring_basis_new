<?php
$count = 1;
if ($this->images) {
    if ($this->useFotorama) {

        ?>

        <div id="imgHolder" style="opacity: 1;">
            <div class="fotorama"
                 data-width="100%"
                 data-maxheight="600"
                 data-nav="thumbs"
                 data-thumbwidth="112px"
                 data-thumbheight="87px"
                 data-thumbmargin="15"
                 data-allowfullscreen="true" data-arrows="always">

                <?php
                foreach ($this->images as $image) {
                    $imgTag = CHtml::image(Images::getThumbUrl($image, 112, 87), Images::getAlt($image));
                    echo CHtml::link($imgTag, Images::getFullSizeUrl($image), array(
                        'title' => Images::getAlt($image),
                    ));
                }

                ?>
            </div>
        </div>

    <?php } else { ?>
        <div id="imgHolder" style="opacity: 1;"></div>
        <div class="jcarousel-wrapper">
            <div class="mini_gallery jcarousel" data-jcarousel="true">
                <ul style="left: 0px; top: 0px;" id="jcarousel">
                    <?php foreach ($this->images as $image) : ?>
                        <li>
                            <?php
                            $imgTag = CHtml::image(Images::getThumbUrl($image, 69, 66), Images::getAlt($image), array(
                                    'onclick' => 'setImgGalleryIndex("' . $count . '");',
                            ));
                            echo CHtml::link($imgTag, Images::getFullSizeUrl($image), array(
                                'data-gal' => 'prettyPhoto[img-gallery]',
                                'title' => Images::getAlt($image),
                                'alt' => Images::getAlt($image),
                            ));
                            $count++;

                            ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <div class="jcarousel-prev jcarousel-control-prev"></div>
            <div class="jcarousel-next jcarousel-control-next"></div>
        </div>
        <?php
    }
}
return;

/*
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/assets/js/slick/slick.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/js/slick/slick.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/js/slick/slick-theme.css');
//
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/assets/js/init_gallery.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/assets/js/photo-swipe/photoswipe.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/assets/js/photo-swipe/photoswipe-ui-default.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/js/photo-swipe/photoswipe.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/js/photo-swipe/default-skin/default-skin.css');

Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/css/my_slick.css');

$count = 1;

if ($this->images) { ?>
    <div class="property__slider">
        <div id="properties-thumbs" class="slider slider--small js-slider-thumbs">
            <div class="slider__block js-slick-slider">
                <?php
                $i = 0;
                foreach ($this->images as $image) {

                    $srcFullImage = Images::getFullSizeUrl($image);
                    $src = Images::getThumbUrl($image,893, 492);

                    $imgFullInfo = @getimagesize($srcFullImage);
                    $imgThumbInfo = @getimagesize($src);

                    if(isset($imgThumbInfo[0]) && isset($imgFullInfo[0]) && $imgFullInfo[0] < $imgThumbInfo[0]) {
                        $srcFullImage = $src;
                        $imgFullInfo = $imgThumbInfo;
                    }
                    ?>

                    <div class="slider__item slider__item--<?php echo $i; ?>" class="slider__img js-gallery-item text-center">
                        <a href="<?php echo $srcFullImage; ?>"
                           data-slick-index="<?php echo $i; ?>" data-size="<?php echo isset($imgFullInfo) ? $imgFullInfo[0].'x'.$imgFullInfo[1] : ''; ?>" data-gallery-index='<?php echo $i; ?>' class="slider__img js-gallery-item">
                            <img
                                    data-lazy="<?php echo $srcFullImage; ?>"
                                    src="<?php  echo $srcFullImage; ?>"
                                <?php echo (Images::getAlt($image)) ? 'alt="'.Images::getAlt($image).'"' : '' ?>
                            />
                        </a>
                    </div>

                    <?php $i++;
                } ?>
            </div>
        </div>

        <div class="slider slider--thumbs">
            <div class="slider__wrap">
                <div class="slider__block js-slick-slider">
                    <?php
                    $i = 0;
                    foreach ($this->images as $image) { ?>
                        <div data-slide-rel='<?php echo $i; ?>' class="slider__item slider__item--<?php echo $i; ?>">
                            <div class="slider__img">
                                <img data-lazy="<?php echo Images::getThumbUrl($image, 165, 107); ?>"
                                     src="<?php echo Images::getThumbUrl($image, 165, 107); ?>" <?php echo (Images::getAlt($image)) ? 'alt="' . Images::getAlt($image) . '"' : '' ?> >
                            </div>
                        </div>

                        <?php $i++;
                    } ?>
                </div>
                <button type="button" class="slider__control slider__control--prev js-slick-prev">
                    <i class="fas fa-angle-left"></i></button>
                <button type="button" class="slider__control slider__control--next js-slick-next">
                    <i class="fas fa-angle-right"></i>
                </button>
            </div>
        </div>

    </div>

<?php } ?>
*/
   