<?php
$formID = $this->modelName . '-form';
$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->controller->createUrl('/booking/main/bookingform', array('id' => $apartment->id)),
    'id' => $formID,
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-disable-button-after-submit'),
    ));

?>
<div class="booking-heading h3 text-center">Форма бронирования</div>



<div id="b_errors"><?php echo $form->errorSummary($model); ?></div>

<?php
echo CHtml::hiddenField('booking_ad', 1);

$this->renderPartial('//modules/booking/views/_form', array(
    'model' => $model,
    'form' => $form,
    'isGuest' => Yii::app()->user->isGuest,
    'isSimpleForm' => false,
    'apartment' => $apartment,
    'user' => $user,
    'forceStartDay' => $forceStartDay,
));

?>
<div class="form-group text-left">
    <?php
    echo CHtml::button(Yii::t('common', 'Бронировать'), array(
        'class' => 'btn btn-primary submit-button btn-lg',
        'onclick' => 'bookingAjaxSubmit(); return false;'
    ));

    ?>
</div>


<p class="note"><?php echo Yii::t('common', 'Fields with <span class="required">*</span> are required.'); ?></p>
<?php $this->endWidget(); ?>

<script>
    var formID = <?php echo CJavaScript::encode($formID) ?>;

    function bookingAjaxSubmit() {
        $('#b_errors').html('');
        $.ajax({
            url: $('#' + formID).attr('action'),
            type: 'post',
            data: $('#' + formID).serialize(),
            dataType: 'json',
            success: function (data) {
                if (data.status == 'ok') {
                    message(data.msg);
                    location.href = location.href;
                    $('#' + formID)[0].reset();
                    $('#booking_captcha_button').click();
                } else {
                    $('#b_errors').html(data.errors);
                    $('#booking_captcha_button').click();
                    //alert(data.errors);
                    $('html, body').animate({
                        scrollTop: $("#b_errors").offset().top
                    }, 2000);
                }
            }
        });

        return false;
    }
</script>
