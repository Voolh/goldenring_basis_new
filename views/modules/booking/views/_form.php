<?php
//			if(Yii::app()->user->isGuest && param('useUserRegistration')){
//				echo Yii::t('module_booking', 'Already have site account? Please <a title="Login" href="{n}">login</a>',
//					Yii::app()->controller->createUrl('/site/login')).'<br /><br />';
//			}
//			else{
if (!Yii::app()->user->isGuest) {
    if (!$model->username) {
        $model->username = $user->username;
    }
    if (!$model->phone) {
        $model->phone = $user->phone;
    }
    if (!$model->useremail) {
        $model->useremail = $user->email;
    }
}
//      	}
$hidden = '';
/* if(Yii::app()->user->id){
  $hidden = 'style="display: none;"';
  } */

?>

<div class="form-group row" <?php echo $hidden; ?>>
    <div class="full-multicolumn-first col-md-6 col-sm-6">
        <?php echo $form->labelEx($model, 'username'); ?>
        <?php echo $form->textField($model, 'username', array('class' => 'form-control')); ?>
        <?php //echo $form->error($model,'username');  ?>
    </div>
    <div class="full-multicolumn-second col-md-6 col-sm-6">
        <?php echo $form->labelEx($model, 'phone'); ?>
        <?php echo $form->telField($model, 'phone', array('class' => 'form-control')); ?>
        <?php //echo $form->error($model,'phone');  ?>
    </div>
</div>

<div class="form-group row" <?php echo $hidden; ?>>
    <div class="full-multicolumn-first col-md-6 col-sm-6">
        <?php echo $form->labelEx($model, 'useremail'); ?>
        <?php echo $form->textField($model, 'useremail', array('class' => 'form-control')); ?>
        <?php //echo $form->error($model,'useremail');  ?>
    </div>


    <div class="full-multicolumn-second col-md-6 col-sm-6">
        <?php if (!$isSimpleForm): ?>
            <?php echo $form->labelEx($model, 'num_guest'); ?>
            <?php echo $form->dropDownList($model, 'num_guest', Booking::getNumGuestList(), array('class' => 'form-control'));

            ?>
            <?php //echo $form->error($model,'num_guest');  ?>
        <?php endif; ?>
    </div>

</div>


<?php
if ($isSimpleForm) {
    echo '<div id="rent_form">';
}

?>
<?php
$useBookingCalendar = false;
if (issetModule('bookingcalendar')) {
    $useBookingCalendar = true;
}

if ($useBookingCalendar && isset($apartment) && $apartment) {
    Bookingcalendar::publishAssets();

    $reservedDays = Bookingcalendar::getReservedDays($apartment->id);

    Yii::app()->clientScript->registerScript('reservedDays', '
			var reservedDays = ' . $reservedDays . ';
			var extremeDays = ' . CJavaScript::encode(Bookingcalendar::$extremeDays) . ';
		', CClientScript::POS_END);
}

if (isset($roomList) && $roomList) {
    echo '<div class="form-group ">';
    echo $form->labelEx($model, 'apartment_id');
    echo $form->dropDownList($model, 'apartment_id', $roomList, array('class' => 'form-control'));
    echo '</div>';
}

?>

<div class="form-group row">
    <div class="full-multicolumn-first col-md-6 col-sm-6 date_z">
        <?php echo $form->labelEx($model, 'date_start'); ?>

        <?php
        if (!$model->date_start) {
            if (issetModule('bookingcalendar') && isset($apartment) && $apartment) {
                if (isset($forceStartDay) && $forceStartDay) {
                    $time = $forceStartDay;
                } else {
                    $time = Bookingcalendar::getFirstFreeDay($apartment->id);
                }
            } else {
                $time = time();
            }

            if (Yii::app()->language != 'ru') {
                $model->date_start = date('m/d/Y', $time);
            } else {
                $model->date_start = date('d.m.Y', $time);
                //$model->date_start = Yii::app()->dateFormatter->formatDateTime($time, 'medium', null);
            }
        }
        if (!$isSimpleForm && $useBookingCalendar) {
            $this->widget('application.modules.bookingcalendar.extensions.FFJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'date_start',
                'range' => 'eval_period',
                'language' => Yii::app()->controller->datePickerLang,
                'options' => array(
                    //'showAnim'=>'fold',
                    'dateFormat' => Booking::getJsDateFormat(),
                    'minDate' => 'new Date()',
                ),
                'htmlOptions' => array(
                    'readonly' => 'true',
                    'class' => 'form-control',
                //'id' => 'date_start_' . rand(1, 9999)
                ),
                'showDayForId' => (isset($apartment) && $apartment) ? $apartment->id : null,
            ));
            echo '<i class="fa fa-calendar" aria-hidden="true"></i>';
        } else {
            $this->widget('application.extensions.FJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'date_start',
                'range' => 'eval_period',
                'language' => Yii::app()->controller->datePickerLang,
                'options' => array(
                    'showAnim' => 'fold',
                    'dateFormat' => Booking::getJsDateFormat(),
                    'minDate' => 'new Date()',
                ),
                'htmlOptions' => array(
                    'readonly' => 'true',
                    'class' => 'form-control'
                ),
            ));
            echo '<i class="fa fa-calendar" aria-hidden="true"></i>';
        }

        ?>
        <?php //echo $form->error($model,'date_start'); ?>
    </div>
    <div class="full-multicolumn-second col-md-6 col-sm-6">
		<?php echo $form->labelEx($model, 'time_in'); ?>
		<?php echo $form->dropDownList($model, 'time_in', HBooking::getTimesIn(), array('class' => 'form-control')); ?>
		<?php //echo $form->error($model,'time_in');   ?>
    </div>
</div>
<div class="form-group row">
    <div class="full-multicolumn-first col-md-6 col-sm-6 date_v">
        <?php echo $form->labelEx($model, 'date_end'); ?>
        <?php
        /* if(!$model->date_end){
          $model->date_end = Yii::app()->dateFormatter->formatDateTime(time()+60*60*24, 'medium', null);
          } */
        if (!$isSimpleForm && $useBookingCalendar) {
            $this->widget('application.modules.bookingcalendar.extensions.FFJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'date_end',
                'range' => 'eval_period',
                'language' => Yii::app()->controller->datePickerLang,
                'options' => array(
                    //'showAnim'=>'fold',
                    'dateFormat' => Booking::getJsDateFormat(),
                    'minDate' => 'new Date()',
                ),
                'htmlOptions' => array(
                    'readonly' => 'true',
                    'class' => 'form-control',
                //'id' => 'date_end_' . rand(1, 9999)
                ),
                'showDayForId' => (isset($apartment) && $apartment) ? $apartment->id : null,
            ));
            echo '<i class="fa fa-calendar" aria-hidden="true"></i>';
        } else {
            $this->widget('application.extensions.FJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'date_end',
                'range' => 'eval_period',
                'language' => Yii::app()->controller->datePickerLang,
                'options' => array(
                    'showAnim' => 'fold',
                    'dateFormat' => Booking::getJsDateFormat(),
                    'minDate' => 'new Date()',
                ),
                'htmlOptions' => array(
                    'readonly' => 'true',
                    'class' => 'form-control'
                ),
            ));
            echo '<i class="fa fa-calendar" aria-hidden="true"></i>';
        }

        ?>
        <?php //echo $form->error($model,'date_end');  ?>
    </div>
    <div class="full-multicolumn-second col-md-6 col-sm-6">
		<?php echo $form->labelEx($model, 'time_out'); ?>
		<?php echo $form->dropDownList($model, 'time_out', HBooking::getTimesOut(), array('class' => 'form-control')); ?>
		<?php //echo $form->error($model,'time_out');  ?>
    </div>
</div>

<?php
if ($isSimpleForm) {
    echo '</div>';
}

?>

<div class="clear"></div>
<div class="form-group">
    <?php echo $form->labelEx($model, 'comment'); ?>
    <?php echo $form->textArea($model, 'comment', array('class' => 'form-control', 'form-groups' => '3')); ?>
    <?php //echo $form->error($model,'comment'); ?>
</div>


<?php if (Yii::app()->user->isGuest) : ?>
	<div class="form-group">
		<?php echo $form->labelEx($model, 'verifyCode');?>
		<?php $display = (param('useReCaptcha', 0)) ? 'none;' : 'block;'?>
		<?php echo $form->textField($model, 'verifyCode', array('autocomplete' => 'off', 'style' => "display: {$display}"));?><br/>
		<?php
		$cAction = '/booking/main/captcha';
		$this->widget('CustomCaptchaFactory',
			array(
				'captchaAction' => $cAction,
				'buttonOptions' => array('class' => 'get-new-ver-code'),
				'clickableImage' => true,
				'imageOptions'=>array('id'=>'booking_captcha'),
				'model' => $model,
				'attribute' => 'verifyCode',
			)
		);?>
		<?php //echo $form->error($model, 'verifyCode');?>
		<br/>
	</div>
<?php endif; ?>