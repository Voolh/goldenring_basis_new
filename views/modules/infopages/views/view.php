<?php
$this->pageTitle .= $model->getStrByLang('title');
$page = (isset($_GET) && isset($_GET['page'])) ? ($_GET['page']) : '';

if (0 && $model->in_bread && ($bread = $model->getBread())) {
    $this->breadcrumbs = $bread;
} elseif (isset($model->menuPageOne) && isset($model->menuPageOne->parent) && $model->menuPageOne->parent && $model->menuPageOne->parent->type == Menu::LINK_NEW_INFO) {
    $this->breadcrumbs = array(
        truncateText($model->menuPageOne->parent->getStrByLang('title'), 10) => $model->menuPageOne->parent->getUrl(),
        truncateText($model->getStrByLang('title'), 10),
    );
} else {
    $this->breadcrumbs = array(
        truncateText($model->getStrByLang('title'), 10),
    );
}

?>


<div class="title highlight-left-right">
	<div><h1><?php echo $model->getStrByLang('title');?></h1></div>
</div>
<div class="clear"></div><br />

<?php
if ($model->widget && $model->widget_position == InfoPages::POSITION_TOP) {
    $this->renderPartial('_view_widget', array('model' => $model, 'widgetTitles' => $model->widget_titles));
    echo '<div class="clear"></div><br />';
}

?>

<?php if (!$page): ?>
    <div class="content_box">
        <?php
        echo HSite::parseText($model->body);
        //echo $model->body; 

        ?>
        <div class="clear"></div>
    </div>
<?php endif; ?>

<?php
if ($model->widget && $model->widget_position == InfoPages::POSITION_BOTTOM){
    $this->renderPartial('_view_widget', array('model' => $model, 'widgetTitles' => $model->widget_titles));
}

if (param('enableCommentsForPages', 1)) {
    $this->widget('application.modules.comments.components.commentListWidget', array(
        'model' => $model,
        'url' => $model->getUrl(),
        'showRating' => false,
    ));
}