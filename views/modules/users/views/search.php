<?php
$userTypes = User::getTypeList('withAll');
$typeName = isset($userTypes[$type]) ? $userTypes[$type] : '?';

$this->pageTitle .= ' - ' . tc('Users') . ': ' . $typeName;
$this->breadcrumbs = array(
    tc('Users'),
);

?>

<div class="title highlight-left-right">
    <div>
        <h1><?php echo tc('Users'); ?></h1>
    </div>
</div>
<div class="clear"></div><br />

<?php
$links = array();
$links[] = array('label' => tc('All'), 'url' => Yii::app()->createUrl('/users/main/search', array('type' => 'all')), 'active' => $type == 'all');
$links[] = array('label' => tc('Private persons'), 'url' => Yii::app()->createUrl('/users/main/search', array('type' => User::TYPE_PRIVATE_PERSON)), 'active' => $type == User::TYPE_PRIVATE_PERSON);
$links[] = array('label' => tc('Agents'), 'url' => Yii::app()->createUrl('/users/main/search', array('type' => User::TYPE_AGENT)), 'active' => $type == User::TYPE_AGENT);
$links[] = array('label' => tc('Agency'), 'url' => Yii::app()->createUrl('/users/main/search', array('type' => User::TYPE_AGENCY)), 'active' => $type == User::TYPE_AGENCY);

?>

<div id="userfilter">
    <?php
    $this->widget('zii.widgets.CMenu', array(
        'items' => $links,
        'htmlOptions' => array(
            'class' => 'nav nav-pills'
        )
    ));

    ?>
</div>

<div class="users-list-view media-list">
    <?php if ($dataProvider && isset($dataProvider->data) && $dataProvider->data): ?>
        <?php
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_search_user_item', // представление для одной записи
            'ajaxUpdate' => false, // отключаем ajax поведение
            'emptyText' => false,
            'summaryText' => "{start}&mdash;{end} " . tc('of') . " {count}",
            'template' => '{summary} {sorter} {items} <div class="clear"></div><br /> {pager}',
            'sortableAttributes' => array('username', 'date_created'),
            'pager' =>
            array(
                'class' => 'BasisPager',
                'maxButtonCount' => 6,
                'header' => '',
                'selectedPageCssClass' => 'active',
                'nextPageLabel' => '<i class="fas fa-angle-right"></i>',
                'prevPageLabel' => '<i class="fas fa-angle-left"></i>',
                'firstPageLabel' => '<i class="fas fa-angle-double-left"></i>',
                'lastPageLabel' => '<i class="fas fa-angle-double-right"></i>',
                'htmlOptions' => array(
                    'class' => 'pagination',
                ),
            ),
            'pagerCssClass' => 'pagination pagination-bottom',
            )
        );

        Yii::app()->clientScript->registerScript('generate-phone', '
				function getPhoneNum(elem, id){
					$(elem).closest("span").html(\'<img src="' . Yii::app()->controller->createUrl('/users/main/generatephone') . '?id=\' + id + \'" />\');
				}
			', CClientScript::POS_END);

        ?>
    <?php else: ?>
        <br/>
        <br/>
        <div class="empty">
            <strong><?php echo tc('No user'); ?></strong>
        </div>
    <?php endif; ?>
</div>