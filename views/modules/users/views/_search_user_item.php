<?php
/** @var User $data */
$count = $index + 1;
$isLast = ($count % 3) ? false : true;
$addClass = ($isLast) ? 'last' : '';

$url = $data->getUrl();

?>




<div class="media">

    <div class="media-left">
        <a href="<?php echo $url ?>">
            <?php $data->renderAva(true, 'media-object img-circle', true); ?>
        </a>
    </div>

    <div class="media-body">

        <h4 class="media-heading"><?php echo $data->type == User::TYPE_AGENCY ? $data->agency_name : $data->username ?> <small><i><?php echo $data->getTypeName() ?></i></small></h4>

        <p>
            <?php
            $additionalInfo = 'additional_info_' . Yii::app()->language;
            echo CHtml::encode($data->{$additionalInfo});

            ?>
        </p>

        <p class="meta">
            <?php
            if ($data->phone) {
                if (issetModule('tariffPlans') && issetModule('paidservices') && ($data->id != Yii::app()->user->id)) {
                    if (Yii::app()->user->isGuest) {
                        $defaultTariffInfo = TariffPlans::getFullTariffInfoById(TariffPlans::DEFAULT_TARIFF_PLAN_ID);

                        if (!$defaultTariffInfo['showPhones']) {
                            //echo '<p><span>'. Yii::t('module_tariffPlans', 'Please <a href="{n}">login</a> to view', Yii::app()->controller->createUrl('/site/login')).'</span></p>';
                        } else {
                            echo '<p><span id="owner-phone">' . CHtml::link(tc('Show phone'), 'javascript: void(0);', array('onclick' => 'getPhoneNum(this, ' . $data->id . ');')) . '</span>' . '</p>';
                        }
                    } else {
                        if (TariffPlans::checkAllowShowPhone())
                            echo '<p><span id="owner-phone">' . CHtml::link(tc('Show phone'), 'javascript: void(0);', array('onclick' => 'getPhoneNum(this, ' . $data->id . ');')) . '</span>' . '</p>';
                        else {
                            //echo '<p><span>'.Yii::t('module_tariffPlans', 'Please <a href="{n}">change the tariff plan</a> to view', Yii::app()->controller->createUrl('/tariffPlans/main/index')).'</span></p>';
                        }
                    }
                } else {
                    echo '<p><span id="owner-phone">' . CHtml::link(tc('Show phone'), 'javascript: void(0);', array('onclick' => 'getPhoneNum(this, ' . $data->id . ');')) . '</span>' . '</p>';
                }
            }

            ?>
            <?php echo '<p><span>' . $data->getLinkToAllListings() . '</span></p>'; ?>
        </p>

        <?php if (issetModule('messages') && $data->id != Yii::app()->user->id && !Yii::app()->user->isGuest): ?>
            <p class="meta">
                <span>
                    <?php echo CHtml::link(tt('Send message', 'messages'), Yii::app()->createUrl('/messages/main/read', array('id' => $data->id)), array('class' => 'btn btn-primary')); ?>
                </span>
            </p>
        <?php endif; ?>

    </div>

</div>
