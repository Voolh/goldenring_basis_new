<?php if ($apartment->type == Apartment::TYPE_RENT) : ?>
    <?php
    Yii::app()->clientScript->registerScript('resize-booking', '
			setTimeout(function(){
				$(".ui-datepicker").width("");
			}, 0);
		', CClientScript::POS_READY);

    ?>

    <?php if (isset($showBookingForm)) { ?>
        <div class="booking-form">
            <?php
            $user = HUser::getModel();
            $booking = new Booking;
            $booking->scenario = 'bookingform';
            Yii::app()->controller->renderPartial('//modules/booking/views/booking_ad', array(
                'apartment' => $apartment,
                'model' => $booking,
                'isFancy' => false,
                'user' => $user,
                'forceStartDay' => '',
            ));

            ?>
        </div>
    <?php } ?>

<?php endif; ?>