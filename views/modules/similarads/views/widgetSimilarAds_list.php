
<?php
if (is_array($ads) && count($ads) > 0) {
    echo '<div class="similar_block">';

    echo '<div class="h3 h_line">' . tt('Similar ads', 'similarads') . '</div>';

    echo '<div id="owl-similar-ads" class="owl-carousel owl-similar owl-theme">';
    foreach ($ads as $key => $item) {
        $adUrl = $item->getUrl();
        if (!$item) {
            continue;
        }
        $url = $item->getUrl();
        $title = CHtml::encode($item->getStrByLang('title'));
        $first_word = strpos(trim($title), ' ');

        ?>

        <div class="item slow <?php if ($key % 2 == 0) echo 'even'; ?>">

            <div class="photo_block">
                <div class="item-photo item-photo_large">
                    <?php
                    if (isset($item->images) && !empty($item->images)) {
                        if ($item->images) {

                            ?>
                            <a href="<?php echo $url; ?>"
                               class="js-item-slider item-slider large-picture">
                                   <?php
                                   $this->widget('application.modules.images.components.ImagesWidgetAvito', array(
                                       'images' => $item->images,
                                       'objectId' => $item->id,
                                       'width' => 330,
                                       'height' => 228
                                   ));

                                   ?>
                            </a>

                            <?php
                        }
                    } else {

                        ?>

                        <a href="<?php echo $url; ?>"
                           class="js-item-slider item-slider large-picture">
                            <ul class="item-slider-list js-item-slider-list">
                                <li class="item-slider-item js-item-slider-item">
                                    <?php
                                    $res = Images::getMainThumb(320, 238, $item->images);
                                    $imgAlt = (isset($res['alt']) && $res['alt']) ? $res['alt'] : CHtml::encode($item->getStrByLang('title'));

                                    ?>
                                    <div class="item-slider-image large-picture"
                                         style="background-image: url(<?php echo $res['thumbUrl']; ?>)"></div>
                                </li>
                            </ul>
                        </a>

                    <?php } ?>
                </div>

                <?php if ($item->is_special_offer) { ?>
                    <div class="hot_label"><i class="fab fa-hotjar"></i><?php echo tc('Special offer!') ?></div>
                <?php } ?>
            </div>

            <div class="item_teaser_content">

                <div class="title_item"><?php echo '<a href="' . $url . '" class="slow">#' . $item->id . ', ' . truncateText($title, 7) . '</a>'; ?></div>

                <ul class="adress_info list-unstyled">
                    <li>
                        <i class="fas fa-map-marker"></i>
                        <?php echo HApartment::getLocationString($item); ?>
                    </li>

                    <?php if ($item->num_of_rooms || $item->floor || $item->floor_total || $item->square) { ?>
                        <ul class="list-inline">
                            <?php if ($item->canShowInView('num_of_rooms')) { ?>
                                <li> <strong><i class="fas fa-columns"></i><?php echo Yii::t('module_apartments', '{n} bedroom|{n} bedrooms|{n} bedrooms', array($item->num_of_rooms));

                                ?></strong></li>
                            <?php } ?>
                            <?php
                            if (($item->floor) && ($item->floor_total)) {
                                echo '<li><i class="fas fa-building"></i> ';
                                echo ' <strong>' . tt('Floor', 'apartments') . '/' . tt('Total number of floors', 'apartments');
                                echo ': </strong>' . $item->floor . '/' . $item->floor_total;
                                echo '</li>';
                            } elseif ($item->floor) {
                                echo '<li><i class="fas fa-building"></i> <strong>' . tt('Floor', 'apartments') . ':</strong> ' . $item->floor . '</li>';
                            } elseif ($item->floor_total) {
                                echo '<li><i class="fas fa-building"></i> <strong>' . tt('Total number of floors', 'apartments') . ': </strong>' . $item->floor_total . '</li>';
                            }

                            ?>

                            <?php
                            if ($item->canShowInView('square')) {
                                echo '<li><i class="fas fa-arrows-alt"></i> <strong>' . utf8_ucfirst(Yii::t('module_apartments', 'total square: {n}', $item->square)) . " " . tc('site_square') . '</strong></li>';
                            }

                            ?>
                        </ul>
                    <?php } ?>

                    <?php if ($item->canShowInView('price')) { ?>
                        <ul class="price list-unstyled">
                            <?php
                            if ($item->is_price_poa) {
                                echo '<li>' . tt('is_price_poa', 'apartments') . '</li>';
                            } else {
                                echo '<li>' . $item->getPrettyPrice(false) . '</li>';
                            }

                            ?>
                        </ul>
                    <?php } ?>

                </ul>

                <?php if (0) { ?>
                    <div class="descr">
                        <?php
                        if ($item->canShowInView('description')) {
                            $description = $item->getStrByLang('description');
                        }
                        if (utf8_strlen($description) > 110)
                            $description = utf8_substr($description, 0, 110) . '...';
                        //echo $description;
                        echo truncateText($description, 22);

                        ?>

                    </div>
                <?php } ?>

                <?php //echo '<p><a href="' . $url . '" class="slow read_more">Подробнее</a></p>';

                ?>
            </div>


            <div class="compare_block">
                <div class="col-md-7 col-sm-7 col-xs-7 compare text-center">
                    <?php if (issetModule('comparisonList')) { ?>
                        <?php
                        $inComparisonList = false;
                        if (in_array($item->id, Yii::app()->controller->apInComparison)) {
                            $inComparisonList = true;
                        }

                        ?>
                        <div class="row compare-check-control"
                             data-id="compare_check_control_<?php echo $item->id; ?>">
                            <div class="pretty p-default">
                                <?php
                                $checkedControl = '';

                                if ($inComparisonList) {
                                    $checkedControl = ' checked = checked ';
                                }

                                ?>
                                <input type="checkbox" name="compare<?php echo $item->id; ?>"
                                       class="compare-check compare-float-left"
                                       data-id="compare_check<?php echo $item->id; ?>" <?php echo $checkedControl; ?>>
                                <div class="state p-warning">
                                    <label>
                                        <a href="<?php echo ($inComparisonList) ? Yii::app()->createUrl('comparisonList/main/index') : 'javascript:void(0);'; ?>"
                                           data-rel-compare="<?php echo ($inComparisonList) ? 'true' : 'false'; ?>"
                                           data-id="compare_label<?php echo $item->id; ?>"
                                           class="compare-label">
                                               <?php
                                               echo ($inComparisonList) ? tt('In the comparison list', 'comparisonList') : tt('Add to a comparison list ', 'comparisonList');

                                               ?>
                                        </a>
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-5 photo_count text-center">
                    <?php
                    $countPhoto = $item->count_img;
                    if ($countPhoto > 0) {

                        ?>
                        <span><i class="fas fa-images"></i> <?php echo $countPhoto . ' ' . Yii::t('common', 'photo|photos|photos', $countPhoto) ?></span>
                    <?php } else { ?>
                        <span><i class="fas fa-images"></i> <?php echo Yii::t('common', 'Without photo') ?></span>
                    <?php } ?>
                </div>
            </div>

        </div>

        <?php
    }
    echo '</div>';
    echo '</div>';
}

?>
