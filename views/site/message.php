<div class="clear"></div>

<h1><?php echo CHtml::encode($messageTitle); ?></h1>

<p><?php echo CHtml::encode($messageText); ?></p>
