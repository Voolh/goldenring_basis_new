<?php
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('common', 'Recover password');
$this->breadcrumbs = array(
    Yii::t('common', 'Recover password')
);

?>

<div class="title highlight-left-right">
    <div>
        <h1><?php echo Yii::t('common', 'Recover password'); ?></h1>
    </div>
</div>
<div class="clear"></div><br />

<div class="form well well-sm">
    <?php
    $form = $this->beginWidget('CustomActiveForm', array(
        'id' => 'recover-form',
        'enableClientValidation' => false,
        'htmlOptions' => array('class' => 'form-disable-button-after-submit'),
        /* 'clientOptions'=>array(
          'validateOnSubmit'=>true,
          ), */
    ));

    ?>

    <p class="note"><?php echo Yii::t('common', 'Fields with <span class="required">*</span> are required.'); ?></p>

    <div class="form-group">
        <?php echo tc('recover_pass_form_help'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email', array('class' => 'width250 form-control')); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'verifyCode'); ?>
        <?php $display = (param('useReCaptcha', 0)) ? 'none;' : 'block;' ?>
        <?php echo $form->textField($model, 'verifyCode', array('autocomplete' => 'off', 'style' => "display: {$display}", 'class' => 'width250 form-control')); ?><br/>
        <?php
        $this->widget('CustomCaptchaFactory', array(
            'captchaAction' => '/site/captcha',
            'buttonOptions' => array('class' => 'get-new-ver-code'),
            'clickableImage' => true,
            'imageOptions' => array('id' => 'recover_captcha'),
            'model' => $model,
            'attribute' => 'verifyCode',
            )
        );

        ?>
        <?php echo $form->error($model, 'verifyCode'); ?>
        <br/>
    </div>

    <div class="form-group buttons">
        <?php echo CHtml::submitButton(Yii::t('common', 'Recover'), array('class' => 'btn btn-primary button-blue submit-button')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->
