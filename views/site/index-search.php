<?php
$this->searchShowLabel = false;

$typesArray = HApartment::getTypesArray();
$searchUrl = (isset($searchUrl)) ? $searchUrl : Yii::app()->controller->createUrl('/quicksearch/main/mainsearch');
?>

<form id="search-form" class="forma" action="<?php echo $searchUrl;?>" method="get">
    <div class="tab_header gr-tab_header">
        <div class="container">
            <ul class="nav nav-tabs" id="search_tab">
                <?php foreach ($typesArray as $typeId => $typeName) { ?>
                    <?php
                    $activeClass = (isset($this->apType) && $typeId == $this->apType) ? 'active' : '';

                    ?>
                    <li class="<?php echo $activeClass ?>" id="li-search-type-<?php echo $typeId ?>"><a href="javascript:;" onclick="setSearchType(<?php echo $typeId ?>);" class="slow"><?php echo $typeName ?></a></li>
                <?php } ?>
            </ul>
            <div class="add_block"><a href="<?php echo Yii::app()->createUrl('/guestad/add') ?>" class="btn btn-primary slow" title="<?php tc('Add ad') ?>"><i class="fas fa-plus"></i></a></div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="search_content">
        <div class="container">
            <div class="index-header-form" id="search_form">
                <?php $this->renderPartial('//site/_search_form', array('isInner' => 0)); ?>
            </div>

            <div class="clear"></div>

            <div class="text-center">

                <ul class="list-inline" id="block_btn">
                    <li><a id="btnleft" href="javascript: void(0);" onclick="doSearchAction();" class="btn btn-primary text-uppercase search_btn slow"><?php echo tc('Search'); ?></a></li>
                    <li><a id="more-options-link" href="javascript:;" class="btn btn-default text-uppercase more_search slow"><i class="fas fa-1x fa-angle-double-down"></i> <?php echo tc('More options'); ?></a></li>
                </ul>
            </div>

            <div class="clear"></div>
        </div>
    </div>
</form>

<?php
$min = param('minLengthSearch', 4);
$searchDesc = tc("Search by description or address");

$script = <<< JS

function doSearchAction() {
    if($("#search_term_text").length){
        var term = $(".search-term input#search_term_text").val();
        if (term.length < $min || term == "{$searchDesc}") {
            $(".search-term input#search_term_text").attr("disabled", "disabled");
        }
    }

    $("#search-form").submit();
}

JS;


Yii::app()->clientScript->registerScript('doSearchActionIndex', $script, CClientScript::POS_END);

$content = $this->renderPartial('//site/search/_search_js', array(
    'isInner' => 0
    ), true, false
);

Yii::app()->clientScript->registerScript('search-params-index-search', $content, CClientScript::POS_END);

?>



