<?php
$showHideFilter = (isset($showHideFilter)) ? $showHideFilter : true;
$compact = ($showHideFilter == false) ? false : param("useCompactInnerSearchForm", false);

$searchUrl = isset($this->aData['searchUrl']) ? $this->aData['searchUrl'] : Yii::app()->controller->createUrl('/quicksearch/main/mainsearch');
if (!isset($this->aData['searchOnMap'])) {

    ?>

    <form id="search-form" action="<?php echo $searchUrl; ?>"
          method="get">
        <div class="h3 h_line"><?php echo tc('quick search') ?></div>

        <div class="inner_form search_inner">
            <?php
            if (isset($this->userListingId) && $this->userListingId) {
                echo CHtml::hiddenField('userListingId', $this->userListingId);
            }

            ?>
            <div id="search_form">
                <?php
                $this->renderPartial('//site/_search_form', array(
                    'isInner' => 1,
                    'compact' => $compact,
                ));

                ?>

            </div>

            <div class="col-md-12" id="block_btn_inner">
                <ul class="list-unstyled">
                    <li><a href="javascript: void(0);" onclick="doSearchAction();" id="btnleft"
                           class="btn btn-primary text-uppercase search_btn slow"><?php echo Yii::t('common', 'Search');

                ?></a></li>
                    <li class="li-more"><a href="javascript:;" id="more-options-link"
                                           class="btn btn-default more_search slow"><i
                                class="fas fa-1x fa-angle-double-down"></i> <?php echo tc('More options'); ?></a>
                    </li>
                </ul>
            </div>

            <div class="clear"></div>

        </div>

    </form>

    <?php
    $content = $this->renderPartial('//site/search/_search_js', array(
        'isInner' => 1
        ), true, false
    );
    Yii::app()->clientScript->registerScript('search-params-inner-search', $content, CClientScript::POS_END);
}

Yii::app()->clientScript->registerScript('doSearchActionInner', '
		function doSearchAction() {
			if($("#search_term_text").length){
				var term = $(".search-term input#search_term_text").val();
				if (term.length < ' . param('minLengthSearch', 4) . ' || term == "' . tc("Search by description or address") . '") {
					$(".search-term input#search_term_text").attr("disabled", "disabled");
				}
			}

			$("#search-form").submit();
		}
', CClientScript::POS_END);

?>