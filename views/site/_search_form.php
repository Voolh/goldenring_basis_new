<?php
$compact = isset($compact) ? $compact : 0;
$isInner = isset($isInner) ? $isInner : 0;
$objType = isset($this->objType) ? $this->objType : 0;
$searchFields = SearchFormModel::getFields($isInner, $objType);

$this->searchShowLabel = true;

$showHidden = $issetApTypeInSearch = false;
$i = 1;
$countField = 0;
foreach ($searchFields as $search) {
    if ($search->field == 'apType' || $search->field == 'ap_type') {
        $issetApTypeInSearch = true;
    }

    if ($isInner) {
        $formGroup = 'form-group';
        $divClass = 'f_inner';
    } else {
        $formGroup = 'form-group';
        $divClass = 'col-md-3 col-sm-6';
    }

    if (!$showHidden && $countField >= 8 && $countField % 4 == 0) {
        $display = (!$isInner || ($isInner && $compact)) ? 'style="display: none;"' : '';
        echo '<div class="additional_hidden_fields" '.$display.'>';
        $showHidden = true;
    }

    if ($search->status <= SearchFormModel::STATUS_NOT_REMOVE) {
        $this->renderPartial('//site/search/_search_field_' . $search->field, array(
            'divClass' => $divClass,
            'textClass' => 'formalabel',
            'formgroup' => $formGroup,
            'controlClass' => '',
            'fieldClass' => 'form-control searchField',
            'isInner' => $isInner,
        ));

        if ($search->field == 'location') {
            $countField += 3;
            if (issetModule('metroStations')) {
                $countField += 1;
            }
        } else {
            $countField++;
        }

        SearchForm::increaseJsCounter();
    } else {
        if ($search->formdesigner) {
            $references = '';
            if ($search->formdesigner->type == FormDesigner::TYPE_REFERENCE || $search->formdesigner->type == FormDesigner::TYPE_MULTY) {
                $references = FormDesigner::getListByCategoryID($search->formdesigner->reference_id, Apartment::convertType($this->apType));
            };

            if (!($search->formdesigner->type == FormDesigner::TYPE_REFERENCE || $search->formdesigner->type == FormDesigner::TYPE_MULTY) || $references) {
                $this->renderPartial('//site/search/_search_new_field', array(
                    'divClass' => $divClass,
                    'textClass' => 'formalabel',
                    'controlClass' => '',
                    'fieldClass' => 'form-control',
                    'formgroup' => $formGroup,
                    'search' => $search,
                    'isInner' => $isInner,
                    'references' => $references,
                ));
                $countField++;

                SearchForm::increaseJsCounter();
            } else
                continue;
        }
    }

    $i++;
}

if ($showHidden) {
    echo '</div>';
}

if (!$issetApTypeInSearch) {
    $apType = 0;
    $typesArray = HApartment::getTypesArray();
    if ($typesArray) {
        $apType = key($typesArray);
    }

    echo CHtml::hiddenField('apType', $apType, array('id' => 'apType', 'name' => 'apType'));
}
