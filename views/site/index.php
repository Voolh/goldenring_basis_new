<?php $baseThemeUrl = Yii::app()->theme->baseUrl; ?>

<?php
$issetPageWidget = (isset($page) && $page && isset($page->page)) && $page->page->widget;
if ($issetPageWidget && $page->page->widget_position == InfoPages::POSITION_TOP) {
    $this->renderPartial('_index_view_widget', array(
        'widget' => $page->page->widget,
        'page' => $page->page,
        'widgetTitles' => $page->page->widget_titles
    ));
    echo '<div class="clear"></div><br />';
}

?>

<?php if (isset($page) && $page && isset($page->page)) { ?>
    <?php
    if ($page->page->title) {
        echo '<h1 class="h_line">' . CHtml::encode($page->page->title) . '</h1>';
    }

    ?>

    <?php
    if ($page->page->body) {
        echo HSite::parseText($page->page->body);
    }

    ?>
<?php } ?>

<?php
if ($issetPageWidget && $page->page->widget_position == InfoPages::POSITION_BOTTOM) {
    $this->renderPartial('_index_view_widget', array(
        'widget' => $page->page->widget,
        'page' => $page->page,
        'widgetTitles' => $page->page->widget_titles
    ));
    echo '<div class="clear"></div><br />';
}

?>
