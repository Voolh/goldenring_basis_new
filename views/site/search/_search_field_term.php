<div class="<?php echo $divClass; ?>">
    <div class="<?php echo $formgroup; ?>">
        <?php if ($this->searchShowLabel) { ?>
            <div class="<?php echo $textClass; ?>">
                <?php echo tc("Search by description or address") ?>
            </div>
        <?php } ?>
        <div class="input-group mb-3 search-term <?php echo $isInner ? ' search-term-inner' : '' ?>">
            <?php
            $term = '';
            if (isset($this->term)) {
                $term = $this->term;
            }
            echo CHtml::textField('term', $term, array(
                'class' => 'textbox ' . $fieldClass,
                'id' => 'search_term_text',
                'maxlength' => 50,
                'placeholder' => tc("Search by description or address"),
            ));

            ?>
            <div class="input-group-btn">
                <button class="search-icon btn btn-outline-secondary" onclick="prepareSearch(); return false;"><i class="fas fa-search"></i></button>
            </div>
        </div>

        <input type="hidden" value="0" id="do-term-search" name="do-term-search">
    </div>
</div>
