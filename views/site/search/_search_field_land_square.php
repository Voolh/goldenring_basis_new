<div class="<?php echo $divClass; ?>">
    <div class="<?php echo $formgroup; ?>">
        <?php if ($this->searchShowLabel) { ?>
            <div class="<?php echo $textClass; ?>"><?php echo tc('Apartment square to'); ?>, <span
                    class="measurement ms2"><?php echo tc("site_land_square"); ?></span></div>
            <?php } ?>
        <div class="rel">
            <input onblur="changeSearch();" type="number" name="land_square"
                   placeholder="<?php echo tc('Apartment square to') ?>"
                   class="<?php echo $fieldClass; ?>"
                   value="<?php echo isset($this->landSquare) && $this->landSquare ? CHtml::encode($this->landSquare) : ""; ?>"/>
        </div>
    </div>
</div>
