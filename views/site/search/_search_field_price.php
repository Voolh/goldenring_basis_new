<?php
if (!issetModule('currency')) {
    $currency = param('siteCurrency', '$');
} else {
    $currency = Currency::getCurrentCurrencyName();
}

if (issetModule('selecttoslider') && param('usePriceSlider') == 1) {

    ?>

    <div class="<?php echo $divClass; ?>">

        <?php if ($this->searchShowLabel) { ?>
            <div class="<?php echo $textClass; ?>"><?php echo tc('Price range'); ?>
                , <?= $currency ?></div>
        <?php } ?>

        <div class="search">
            <div class="<?php echo $formgroup; ?>">
                <?php
                if (isset($this->objType) && $this->objType) {
                    $priceAll = HApartment::getPriceMinMax($this->objType);
                } else {
                    $priceAll = HApartment::getPriceMinMax(1, true);
                }

                $priceAll['price_min'] = isset($priceAll['price_min']) ? $priceAll['price_min'] : 0;
                $priceAll['price_max'] = isset($priceAll['price_max']) ? $priceAll['price_max'] : 1000;

                if (issetModule('currency')) {
                    $priceAll['price_min'] = floor(Currency::convertFromDefault($priceAll['price_min']));
                    $priceAll['price_max'] = ceil(Currency::convertFromDefault($priceAll['price_max']));
                }

                $diffPrice = $priceAll['price_max'] - $priceAll['price_min'];
                $step = SearchForm::getSliderStep($diffPrice);

                $priceMinSel = (isset($this->priceSlider) && isset($this->priceSlider["min"]) && $this->priceSlider["min"] >= $priceAll["price_min"] && $this->priceSlider["min"] <= $priceAll["price_max"]) ? $this->priceSlider["min"] : $priceAll["price_min"];
                $priceMaxSel = (isset($this->priceSlider) && isset($this->priceSlider["max"]) && $this->priceSlider["max"] <= $priceAll["price_max"] && $this->priceSlider["max"] >= $priceAll["price_min"]) ? $this->priceSlider["max"] : $priceAll["price_max"];

                //$priceMinSel = Apartment::priceFormat($priceMin);
                //$priceMaxSel = Apartment::priceFormat($priceMax);

                SearchForm::renderSliderRange(array(
                    'field' => 'price',
                    'min' => $priceAll['price_min'],
                    'max' => $priceAll['price_max'],
                    'min_sel' => $priceMinSel,
                    'max_sel' => $priceMaxSel,
                    'step' => $step,
                    'measure_unit' => $currency,
                    'class' => 'price-search-select',
                ));

                ?>
            </div>
        </div>
    </div>

    <?php
} else {
    $priceMinSel = (isset($this->priceSlider) && isset($this->priceSlider["min"]) && (int) $this->priceSlider["min"]) ? $this->priceSlider["min"] : '';
    $priceMaxSel = (isset($this->priceSlider) && isset($this->priceSlider["max"]) && (int) $this->priceSlider["max"]) ? $this->priceSlider["max"] : '';

    ?>

    <div class="<?php echo $divClass; ?>">
        <div class="pricebox row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="<?php echo $formgroup; ?>">
                    <?php if ($this->searchShowLabel) { ?>
                        <div class="<?php echo $textClass; ?>"><?php echo tc('Price from') ?>
                            , <?= $currency ?></div>
                    <?php } ?>
                    <input onblur="changeSearch();" type="number" id="priceMin" name="price_min"
                           class="<?php echo $fieldClass; ?>" placeholder="<?php echo tc('Price from') ?>"
                           value="<?php echo CHtml::encode($priceMinSel); ?>"/>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="<?php echo $formgroup; ?>">
                    <?php if ($this->searchShowLabel) { ?>
                        <div class="<?php echo $textClass; ?>"><?php echo tc('Price to') ?>
                            , <?= $currency ?></div>
                    <?php } ?>
                    <input onblur="changeSearch();" type="number" id="priceMax" name="price_max"
                           class="<?php echo $fieldClass; ?>" placeholder="<?php echo tc('Price to') ?>"
                           value="<?php echo CHtml::encode($priceMaxSel); ?>"/>
                </div>
            </div>
        </div>
    </div>
    <?php
    Yii::app()->clientScript->registerScript('priceFocusSubmit', '
			focusSubmit($("input#priceMin"));
			focusSubmit($("input#priceMax"));
		', CClientScript::POS_READY);
}

?>
