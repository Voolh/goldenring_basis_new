<div class="<?php echo $divClass; ?>">
    <div class="<?php echo $formgroup; ?>">
        <?php if (issetModule('selecttoslider') && param('useFloorSlider') == 1) { ?>
            <div class="<?php echo $textClass; ?>"><?php echo Yii::t('common', 'Floor range'); ?></div>
            <div class="<?php echo $formgroup; ?>">
                <?php
                $floorItems = array_merge(
                    range(0, param('moduleApartments_maxFloor', 30))
                );
                $floorMin = isset($this->floorCountMin) ? CHtml::encode($this->floorCountMin) : 0;
                $floorMax = isset($this->floorCountMax) ? CHtml::encode($this->floorCountMax) : max($floorItems);

                SearchForm::renderSliderRange(array(
                    'field' => 'floor',
                    'min' => 0,
                    'max' => param('moduleApartments_maxFloor', 30),
                    'min_sel' => $floorMin,
                    'max_sel' => $floorMax,
                    'step' => 1,
                    'class' => 'floor-search-select',
                ));

                ?>
            </div>
        <?php } else { ?>

            <?php
            $floorMin = isset($this->floorCountMin) ? CHtml::encode($this->floorCountMin) : '';
            $floorMax = isset($this->floorCountMax) ? CHtml::encode($this->floorCountMax) : '';

            ?>

            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="<?php echo $formgroup; ?>">
                        <?php if ($this->searchShowLabel) { ?>
                            <div class="<?php echo $textClass; ?>"><?php echo tc('Floor from') ?></div>
                        <?php } ?>
                        <input onblur="changeSearch();" type="number" id="floorMin" name="floor_min"
                               class="<?php echo $fieldClass; ?>" placeholder="<?php echo tc('Floor from') ?>"
                               value="<?php echo CHtml::encode($floorMin); ?>"/>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="<?php echo $formgroup; ?>">
                        <?php if ($this->searchShowLabel) { ?>
                            <div class="<?php echo $textClass; ?>"><?php echo tc('Floor to') ?></div>
                        <?php } ?>
                        <input onblur="changeSearch();" type="number" id="floorMax" name="floor_max"
                               class="<?php echo $fieldClass; ?>" placeholder="<?php echo tc('Floor to') ?>"
                               value="<?php echo CHtml::encode($floorMax); ?>"/>
                    </div>
                </div>

            </div>


            <?php
            Yii::app()->clientScript->registerScript('squareFocusSubmit', '
                    focusSubmit($("input#floorMin"));
                    focusSubmit($("input#floorMax"));
                ', CClientScript::POS_READY);
        }

        ?>
    </div>
</div>
