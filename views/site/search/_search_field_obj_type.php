<div class="<?php echo $divClass; ?>">
    <div class="block-type <?php echo $formgroup; ?>">
        <?php if ($this->searchShowLabel) { ?>
            <div class="<?php echo $textClass; ?>"><?php echo Yii::t('common', 'Property type'); ?></div>
        <?php } ?>

        <?php
        echo CHtml::dropDownList(
            'objType', isset($this->objType) ? $this->objType : 0, CMap::mergeArray(array(0 => Yii::t('common', 'Property type')), Apartment::getObjTypesArray(false)), array('class' => $fieldClass)
        );
        Yii::app()->clientScript->registerScript('objType', '
		focusSubmit($("select#objType"));
	', CClientScript::POS_READY);

        ?>
    </div>
</div>