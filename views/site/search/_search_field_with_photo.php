<div class="<?php echo $divClass; ?>">
    <div class="<?php echo $controlClass; ?>">
        <div class="<?php echo $formgroup; ?>" style="padding: 33px 0 0;">
            <div class="pretty p-default">
                <?php
                echo CHtml::checkBox('wp', (isset($this->wp) && $this->wp) ? CHtml::encode($this->wp) : '', array(
                    'class' => 'search-input-new searchField',
                    'id' => 'search_with_photo'
                ));

                ?>
                <div class="state p-success">
                    <label for="search_with_photo">
                        <?php echo tc('Only with photo'); ?>
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
