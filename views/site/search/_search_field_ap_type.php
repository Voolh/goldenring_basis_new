<div class="<?php echo $divClass; ?>">
    <div class="<?php echo $formgroup; ?>">
        <?php if ($this->searchShowLabel) { ?>
            <div class="<?php echo $textClass; ?>"><?php echo tt('Search in section', 'common'); ?></div>
        <?php } ?>
        <?php
        echo CHtml::dropDownList(
            'apType', isset($this->apType) ? CHtml::encode($this->apType) : '', HApartment::getTypesForSearch(true), array('class' => $fieldClass)
        );

        Yii::app()->clientScript->registerScript('ap-type-init', '
				focusSubmit($("select#apType"));
			', CClientScript::POS_READY);

        ?>
    </div>
</div>
