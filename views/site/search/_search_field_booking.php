<div class="<?php echo $divClass; ?>">
    <div class="<?php echo $formgroup; ?>">

        <?php
        $datePickerData = array(
            'b_start' => array(
                'showAnim' => 'fold',
                'dateFormat' => Booking::getJsDateFormat(),
                'minDate' => 'new Date()',
            //'maxDate'=>'+12M',
            ),
            'b_end' => array(
                'showAnim' => 'fold',
                'dateFormat' => Booking::getJsDateFormat(),
                'minDate' => 'new Date()',
            ),
        );

        $bStart = isset($this->bStart) ? $this->bStart : null;
        $bEnd = isset($this->bEnd) ? $this->bEnd : null;

        ?>
        <div class="row">

            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="<?php echo $formgroup; ?>">
                    <?php if ($this->searchShowLabel) { ?>
                        <div class="<?php echo $textClass; ?>">
                            <?php echo tc('Booking from') . ':&nbsp;'; ?>
                        </div>
                    <?php } ?>

                    <?php
                    $this->widget('application.extensions.FJuiDatePicker', array(
                        'name' => 'b_start',
                        'value' => $bStart,
                        'range' => 'eval_period',
                        'language' => Yii::app()->controller->datePickerLang,
                        'options' => $datePickerData['b_start'],
                        'htmlOptions' => array(
                            'class' => 'searchField form-control',
                            'readonly' => 'true',
                        ),
                    ));

                    ?>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="<?php echo $formgroup; ?>">
                    <?php if ($this->searchShowLabel) { ?>
                        <div class="<?php echo $textClass; ?>">
                            <?php echo tc('to') . ':'; ?>
                        </div>
                    <?php } ?>
                    <?php
                    $this->widget('application.extensions.FJuiDatePicker', array(
                        'name' => 'b_end',
                        'value' => $bEnd,
                        'range' => 'eval_period',
                        'language' => Yii::app()->controller->datePickerLang,
                        'options' => $datePickerData['b_end'],
                        'htmlOptions' => array(
                            'class' => 'searchField form-control',
                            'readonly' => 'true',
                        ),
                    ));

                    ?>
                </div>
            </div>

        </div>

    </div>
</div>

<script>
    useDatePicker = <?php echo CJavaScript::encode($datePickerData); ?>
</script>

