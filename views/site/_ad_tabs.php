<div class="listing_tabs_box">

    <?php
    Yii::import('application.modules.referencevalues.models.ReferenceValues');

    // ключ id типа недвижимости
    //  $tabs = array(
    //     0 => array(
    //       tc('Все работы'),
    //       'site_type' => 7
    //       ),
    //     9 => tc('Недвижимость'),
    //     10 => tc('Интернет магазины'),
    //     2 => tc('Фотографии'),
    //     4 => tc('Виртуальные 3D туры'),
    // );
    // $tabs = array(
    //     0 => array('name'=>tc('Все работы'),'objType'=>0),
    //     1 => array('name'=>tc('Сайты'),'objType'=>1),
    //     // 2 => array('name'=>tc('Недвижимость'),'objType'=>1, 'site_type' => 48),
    //     // 3 => array('name'=>tc('Интернет магазины'),'objType'=>1, 'site_type' => 49),
    //     2 => array('name'=>tc('Фотографии'),'objType'=>2),
    //     3 => array('name'=>tc('Виртуальные 3D туры'),'objType'=>4),
    // );


    $tabsData = HApartment::getTabsData($tabs, $limit);

    //deb($tabsData);
    if (!$tabsData)
        return;

    ?>
    <div class="tab_header">
        <ul class="nav nav-tabs" id="myTab">
            <?php
            //deb($tabs);
            foreach ($tabs as $id => $name) {
                if ($id == 0) {
                    echo '<li class="active"><a href="#Tab' . $id . '" class="slow"><span>' . $name['name'] . '</span></a></li>';
                } else {
                    echo '<li><a href="#Tab' . $id . '" class="slow"><span>' . $name['name'] . '</span></a></li>';
                }
            }

            ?>
        </ul>
    </div>
    <div class="tab-content">
        <?php foreach ($tabsData as $key => $models) : ?>
            <div  class="tab-pane row fade <?php if ($key == 0) echo 'active in'; ?>" id="Tab<?php echo $key ?>">

                <?php
                foreach ($models as $item) :




                    $adUrl = $item->getUrl();
                    if (!$item) {
                        continue;
                    }
                    $url = $item->getUrl();
                    $title = CHtml::encode($item->getStrByLang('title'));
                    $first_word = strpos(trim($title), ' ');
                    //$title_format = substr($title,0, $first_word).'<strong>'.substr($title,$first_word).'</strong>';

                    ?>



                    <div class="col-md-4 col-sm-6"> 
                        <div class="item slow <?php if ($key % 2 == 0) echo 'even'; ?>">


                            <!--       <?php
                            $res = Images::getMainThumb(310, 207, $item->images);
                            $imgAlt = (isset($res['alt']) && $res['alt']) ? $res['alt'] : $title;
                            $img = CHtml::image($res['thumbUrl'], $imgAlt, array(
                                    'title' => $title,
                                    'class' => 'apartment_type_img',
                                    'alt' => $imgAlt,
                            ));

                            ?>
                            <?php echo $img; ?> -->
                            <!--     <?php
                            $res = Images::getMainThumb(310, 207, $item->images);
                            $imgAlt = (isset($res['alt']) && $res['alt']) ? $res['alt'] : CHtml::encode($item->getStrByLang('title'));
                            $img = CHtml::image($res['thumbUrl'], $imgAlt, array(
                                    'title' => $item->getStrByLang('title'),
                                    'class' => 'apartment_type_img',
                                    'alt' => $imgAlt,
                                    'id' => 'mainImage_' . $item->id,
                                    'data-full' => $res['thumbUrl'],
                            ));
                            echo CHtml::link($img, $adUrl, array('title' => $item->getStrByLang('title')));

                            ?> -->

                            <div class="item-photo item-photo_large">
                                <?php if (($item->demand_check && $item->new_price) || $item->is_special_offer) { ?>
                                    <ul class="like list-unstyled">  
                                        <?php if ($item->demand_check && $item->new_price): ?>
                                            <li class="l1">цена снижена</li>
                                        <?php endif; ?>
                                        <?php if ($item->is_special_offer): ?>
                                            <li class="l2"><?php echo tt('is_special_offer'); ?></li>
                                        <?php endif; ?>

                                    </ul>
                                <?php } ?>
                                <?php
                                if (isset($item->images) && !empty($item->images)) {
                                    if ($item->images) {

                                        ?>
                                        <a href="<?php echo $url; ?>"  class="js-item-slider item-slider large-picture">
                                            <?php
                                            $this->widget('application.modules.images.components.ImagesWidgetAvito', array(
                                                'images' => $item->images,
                                                'objectId' => $item->id,
                                                'width' => 330,
                                                'height' => 228
                                            ));

                                            ?>
                                        </a>

                                        <?php
                                    }
                                } else {

                                    ?>


                                    <a href="<?php echo $url; ?>" class="js-item-slider item-slider large-picture">
                                        <ul class="item-slider-list js-item-slider-list">
                                            <li class="item-slider-item js-item-slider-item">
                                                <?php
                                                $res = Images::getMainThumb(320, 238, $item->images);
                                                //deb($res);
                                                $imgAlt = (isset($res['alt']) && $res['alt']) ? $res['alt'] : CHtml::encode($item->getStrByLang('title'));

                                                ?>
                                                <div class="item-slider-image large-picture" style="background-image: url(<?php echo $res['thumbUrl']; ?>)"></div>
                                                <?php
                                                // $img = CHtml::image($res['thumbUrl'], $imgAlt);
                                                // if ($res['link']) {
                                                //  echo CHtml::link($img, $res['link'], array(
                                                //    'data-gal' => 'prettyPhoto[img-gallery]',
                                                //    'title' => $imgAlt,
                                                //    ));
                                                //  } else {echo $img;} 

                                                ?>
                                            </li>
                                        </ul>
                                    </a>

                                <?php } ?>
                            </div>


                            <div class="item_teaser_content">
                                <div class="title_item"><?php echo '<a href="' . $url . '" class="slow">#' . $item->id . ', ' . $title . '</a>'; ?></div>
                                <?php if ($item['loc_city'] || MetroStations::getApartmentStationsTitle($item->id)): ?>
                                    <ul class="adress_info list-unstyled">
                                        <li>
                                            <i class="fas fa-map-marker"></i>
                                            <?php echo $item->locCity->name; ?>
                                            <?php
                                            if ($item->address_ru) {
                                                echo ', ' . $item->address_ru;
                                            }

                                            ?>
                                        </li>

                                            <?php if ($item->num_of_rooms || $item->floor || $item->floor_total || $item->square || $item->naznachenie_) { ?>
                                            <ul class="list-inline">
                                                <?php
                                                if ($item->num_of_rooms) {
                                                    echo '<li><strong><i class="fas fa-columns"></i>Комнат —</strong> ' . $item->num_of_rooms . '</li>';
                                                }

                                                ?>
                                                <?php
                                                if (($item->floor) && ($item->floor_total)) {
                                                    echo '<li><i class="fas fa-building"></i><strong>Этаж/этажей: </strong>' . $item->floor . '/' . $item->floor_total . '</li>';
                                                } elseif ($item->floor) {
                                                    echo '<li><i class="fas fa-building"></i><strong>Этаж:</strong> ' . $item->floor . '</li>';
                                                } elseif ($item->floor_total) {
                                                    echo '<li><i class="fas fa-building"></i><strong>Этажей: </strong>' . $item->floor_total . '</li>';
                                                }
                                                // if($item->square)
                                                //   echo '<li><i class="far fa-square"></i><strong>Площадь: </strong> '.$item->square.' м<sup>2</sup></li>';
                                                if ($item->square || $item->ploshad_zhil || $item->ploshad_kuhn) {
                                                    echo '<li><i class="far fa-square"></i><strong>Площадь: </strong>';
                                                    if ($item->square && $item->ploshad_zhil && $item->ploshad_kuhn) {
                                                        echo $item->square . '/' . $item->ploshad_zhil . '/' . $item->ploshad_kuhn . ' м<sup>2</sup>';
                                                    } elseif ($item->square && ($item->ploshad_zhil || $item->ploshad_kuhn)) {
                                                        echo $item->square . '/' . (($item->ploshad_zhil) ? $item->ploshad_zhil : '') . (isset($item->ploshad_kuhn) ? $item->ploshad_kuhn : '') . ' м<sup>2</sup>';
                                                    } elseif (!$item->square && ($item->ploshad_zhil && $item->ploshad_kuhn)) {
                                                        echo ($item->ploshad_zhil) ? $item->ploshad_zhil . '/' : '';
                                                        echo ($item->ploshad_kuhn) ? $item->ploshad_kuhn : '';
                                                        echo ' м<sup>2</sup>';
                                                    } else {
                                                        echo ($item->square) ? $item->square . ' м<sup>2</sup>' : '';
                                                        echo ($item->ploshad_zhil) ? $item->ploshad_zhil . ' м<sup>2</sup>' : '';
                                                        echo ($item->ploshad_kuhn) ? $item->ploshad_kuhn . ' м<sup>2</sup>' : '';
                                                    }
                                                }

                                                ?>
                                                <?php
                                                if ($item->naznachenie_) {
                                                    echo '<li><i class="fab fa-app-store"></i> <strong>Назначение участка: </strong> ' . ReferenceValues::getTitleById($item->naznachenie_) . '</li>';
                                                }

                                                ?>
                                            </ul>
                                        <?php } ?>
        <?php endif; ?>


                                        <?php if ($item->canShowInView('price')) { ?>
                                        <ul class="price list-inline">
                                            <?php
                                            if ($item->is_price_poa) {
                                                echo '<li>' . tt('is_price_poa', 'apartments') . '</li>';
                                            } else {
                                                if ($item->demand_check && $item->new_price) {
                                                    echo '<li><span class="new_price">' . number_format($item->new_price, 0, ',', ' ') . ' руб.</span></li><li><s>' . $item->getPrettyPrice(false) . '</s></li>';
                                                } else {
                                                    echo '<li>' . $item->getPrettyPrice(false) . '</li>';
                                                }
                                            }

                                            ?>
                                        </ul>
        <?php } ?>


                                </ul>





                                <div class="descr">
                                    <?php
                                    if ($item->canShowInView('description')) {
                                        $description = $item->getStrByLang('description');
                                    }
                                    if (utf8_strlen($description) > 110)
                                        $description = utf8_substr($description, 0, 110) . '...';
                                    //echo $description;
                                    echo truncateText($description, 22);

                                    ?>

                                </div>
        <?php //echo '<p><a href="' . $url . '" class="slow read_more">Подробнее</a></p>';   ?>
                            </div>                            









                        </div>
                    </div>   


    <?php endforeach; ?>

            </div>
<?php endforeach; ?>
        <div class="clear"> </div>
    </div>


</div>

<script>


// $('#myTabs a').click(function (e) {
//   e.preventDefault()
//   $(this).tab('show')
// })

    var tabChange = function () {
        var tabs = $('#myTab.nav-tabs > li');
        var active = tabs.filter('.active');

        var next = active.next('li').length ? active.next('li').find('a') : tabs.filter(':first-child').find('a');
        // Use the Bootsrap tab show method
        next.tab('show');
    };
    // Tab Cycle function
    //var tabCycle = setInterval(tabChange, 5000);

    // Tab click event handler
    $('#myTab a').on('click', function (e) {
        e.preventDefault();
        // Stop the cycle
        // clearInterval(tabCycle);
        // Show the clicked tabs associated tab-pane
        $(this).tab('show');
        // Start the cycle again in a predefined amount of time
        // setTimeout(function () {
        //     //tabCycle = setInterval(tabChange, 5000);
        // }, 15000);
    });


</script>
