<?php if ($images) { ?>

    <div id="slider-wide" class="box slider slider--wide">
        <div class="slider__block js-slick-slider">

            <?php foreach ($images as $img) { ?>

                <?php //$img['url']   ?>
                <div class="slider__item">
                    <div class="slider__preview">
                        <div class="slider__img slider__img--lg">
			                <?php if($img['url']) echo "<a href='".$img['url']."'>"; ?>
                            <img src="<?php echo $img['src']; ?>" alt=""/>
			                <?php if($img['url']) echo "</a>"; ?>
                        </div>
                    </div>
                    <?php if (isset($img['title']) && $img['title']) { ?>
                        <div class="slider__caption">
                            <div class="h3"><?= $img['title'] ?></div>
                            <?php if (isset($img['text']) && $img['text']) { ?>
                                <div class="slider__text"><?= $img['text'] ?></div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>

            <?php } ?>

        </div>

        <div class="slider__controls">
            <button class="slider__control slider__control--prev js-banner-prev slow"></button>
            <button class="slider__control slider__control--next js-banner-next slow"></button>
        </div>
    </div>

<?php } ?>
