<div id="ore-ads-block">
    <div>
        <ul>
			<li class="ads-block-buy">
				<?php
				$linkTitle = Yii::t('module_install', 'Buy', array(), 'messagesInFile', Yii::app()->language);
				$linkHref = (Yii::app()->language == 'ru') ? 'http://open-real-estate.info/ru/download-open-real-estate' : 'http://open-real-estate.info/en/download-open-real-estate';

				echo CHtml::link(
					'<span class="download"></span>' . $linkTitle,
					$linkHref,
					array(
						'class' => 'btn btn-success',
						'target' => '_blank',
					)
				);
				?>
			</li>
			<?php if (isFree()):?>
				<li class="ads-block-demo-pro">
					<?php
					echo CHtml::link(
						Yii::t('module_install', 'PRO version demo', array(), 'messagesInFile', Yii::app()->language),
						'http://re-pro.monoray.net/',
						array(
							'class' => 'btn btn-success',
							'target' => '_blank',
						)
					);
					?>
				</li>

				<li class="ads-block-addons">
					<?php
					echo CHtml::link(
						Yii::t('module_install', 'Add-ons', array(), 'messagesInFile', Yii::app()->language),
						(Yii::app()->language == 'ru') ? 'http://open-real-estate.info/ru/open-real-estate-modules' : 'http://open-real-estate.info/en/open-real-estate-modules',
						array(
							'class' => 'btn btn-default',
							'target' => '_blank',
						)
					);
					?>
				</li>
			<?php endif;?>
            <li class="ads-block-about">
                <?php
                echo CHtml::link(
                    Yii::t('module_install', 'About product', array(), 'messagesInFile', Yii::app()->language),
                    (Yii::app()->language == 'ru') ? 'http://open-real-estate.info/ru/about-open-real-estate' : 'http://open-real-estate.info/en/about-open-real-estate',
                    array(
                        'class' => 'btn btn-default',
						'target' => '_blank',
                    )
                );
                ?>
            </li>
           <li class="ads-block-contact">
                <?php
                echo CHtml::link(
                    Yii::t('module_install', 'Contact us', array(), 'messagesInFile', Yii::app()->language),
                    (Yii::app()->language == 'ru') ? 'http://open-real-estate.info/ru/contact-us' : 'http://open-real-estate.info/en/contact-us',
                    array(
                        'class' => 'btn btn-default',
						'target' => '_blank',
                    )
                );
                ?>
            </li>

			<?php if(Yii::app()->user->isGuest){ ?>
			<li class="item-login">
				<?php
				echo CHtml::link(
					Yii::t('module_install', 'Log in', array(), 'messagesInFile', Yii::app()->language),
					Yii::app()->createUrl('/login'),
					array(
						'class' => 'btn btn-primary',
					)
				);
				?>
			</li>
            <li class="item-login-admin-panel">
                <?php
                echo CHtml::link(
                    Yii::t('module_install', 'Admin panel', array(), 'messagesInFile', Yii::app()->language),
                    Yii::app()->createUrl('/login', array('inadminpanel' => 1)),
                    array(
                        'class' => 'btn btn-primary',
                    )
                );
                ?>
            </li>
			<?php } ?>

			<?php if (!isFree() && !basicVersion()):?>
				<li class="ads-block-change-template">
					<?php
					$themeList = Themes::getTemplatesList();
                    
					echo CHtml::dropDownList('template', Themes::getParam('title'), $themeList, array(
						'onchange' => 'js: changeTemplate(this.value);',
						'empty' => Yii::t('module_install', 'Theme', array(), 'messagesInFile', Yii::app()->language),
                        'class' => 'form-control'
					));
					?>
				</li>
			<?php endif;?>
        </ul>
    </div>
</div>

<script type="text/javascript">
    function changeTemplate(template){
        location.href = URL_add_parameter(location.href, 'template', template);
    }
	
    function URL_add_parameter(url, param, value){
        var hash       = {};
        var parser     = document.createElement('a');

        parser.href    = url;

        var parameters = parser.search.split(/\?|&/);

        for(var i=0; i < parameters.length; i++) {
            if(!parameters[i])
                continue;

            var ary      = parameters[i].split('=');
            hash[ary[0]] = ary[1];
        }

        hash[param] = value;

        var list = [];
        Object.keys(hash).forEach(function (key) {
            list.push(key + '=' + hash[key]);
        });

        parser.search = '?' + list.join('&');
        return parser.href;
    }
</script>