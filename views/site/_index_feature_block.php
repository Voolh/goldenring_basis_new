<?php
$baseThemeUrl = Yii::app()->theme->baseUrl;

$cs = Yii::app()->clientScript;
$cs->registerScriptFile($baseThemeUrl . '/assets/js/parallax.min.js', CClientScript::POS_END);
$cs->registerScriptFile($baseThemeUrl . '/assets/js/fm.revealator.jquery.min.js', CClientScript::POS_END);
$cs->registerCssFile($baseThemeUrl . '/assets/css/fm.revealator.jquery.min.css');

?>

<div class="parallax-window feature_block" data-parallax="scroll"
     data-image-src="<?php echo $baseThemeUrl ?>/assets/images/parallax/feature_bg.jpg">
    <div class="container">
        <div class="h3 h_line">OPEN REAL ESTATE CMS</div>
        <div class="slogan_text"><p><?= tt('You receive the full-fledged website to a flow of new clients', 'theme_basis') ?></p></div>
        <div class="wrapper_feature">
            <div class="col-md-4 col-sm-4 col-xs-12 revealator-rotateleft  revealator-once">
                <div class="h4"><strong><?= tt('Free of charge', 'theme_basis') ?></strong><br><?= tt('download the script', 'theme_basis') ?></div>
                <div class="icon"><img src="<?php echo $baseThemeUrl ?>/assets/images/icon1.png" alt=""></div>
                <p><strong><?= tt('The free version', 'theme_basis') ?></strong> <?= tt('with an open source', 'theme_basis') ?></p>
                <p><?= tt('Just download', 'theme_basis') ?></p>
                <p><?= tt('Open Real Estate archive and install on a hosting', 'theme_basis') ?></p>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 darker revealator-rotateleft revealator-delay1 revealator-once">
                <div class="h4"><strong><?= tt('Fill', 'theme_basis') ?></strong><br><?= tt('the website', 'theme_basis') ?></div>
                <div class="icon"><img src="<?php echo $baseThemeUrl ?>/assets/images/icon2.png" alt=""></div>
                <p><strong><?= tt('The website is ready!', 'theme_basis') ?></strong></p>
                <p><?= tt('You just have to fill the base of listings', 'theme_basis') ?></p>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 revealator-rotateleft revealator-delay4 revealator-once">
                <div class="h4"><strong><?= tt('Receive', 'theme_basis') ?></strong><br><?= tt('new clients', 'theme_basis') ?></div>
                <div class="icon"><img src="<?php echo $baseThemeUrl ?>/assets/images/icon3.png" alt=""></div>
                <p><?= tt('With our help, ', 'theme_basis') ?> <strong><?= tt('more than 1000 real estate', 'theme_basis') ?></strong></p>
                <p><?= tt('agencies and private realtors already derive an income from the website', 'theme_basis') ?></p>
            </div>
            <div class="clear"></div>
        </div>
        <p class="text-center">
            <?php
            if (Yii::app()->language == 'ru') {
                $urlTo = 'https://open-real-estate.info/ru/download-open-real-estate';
            } else {
                $urlTo = 'https://open-real-estate.info/en/download-open-real-estate';
            }

            ?>
            <a href="<?= $urlTo ?>" class="btn btn-default slow text-uppercase"><i class="fas fa-download"></i> <?= tt('To try free of charge', 'theme_basis') ?></a>
        </p>

    </div>
</div>