<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/usercpanel.css');

?>

<?php $this->beginContent('//layouts/main'); ?>

<?php $this->aData['bodyClass'] = 'inner_page' ?>

<div class="content main_content  container">
    <?php if (issetModule('advertising')) : ?>
        <?php $this->renderPartial('//modules/advertising/views/advert-top', array()); ?>
    <?php endif; ?>
    
    <div class="content_center box">

        <div class="row in_row">
            <div class="col-md-3 left_sidebar">

                <nav class="navbar navbar-default usercpanel-navbar sidebar" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                                <span class="sr-only"><?php echo tt('Toggle navigation', 'theme_basis') ?></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                            <?php
                            $this->widget('zii.widgets.CMenu', array(
                                'items' => HUser::getMenu(),
                                'htmlOptions' => array(
                                    'id' => 'navlist',
                                    'class' => 'nav navbar-nav usercpanel-nav'
                                ),
                            ));

                            ?>
                        </div>
                    </div>
                </nav>

                <div class="clearfix"></div>
                <br/>

                <?php
                if (issetModule('tariffPlans') && issetModule('paidservices')) {
                    $this->widget('application.modules.tariffPlans.components.userTariffInfoWidget', array('userId' => Yii::app()->user->id, 'showChangeTariffLnk' => true));
                }

                ?>

            </div>

            <div class="col-md-9 main-content-wrapper <?php echo ($this->htmlPageId == 'viewlisting') ? 'item-list' : 'content_entries'; ?>">

                <div class="row">
                    <div class="usercpanel-right floatleft">
                        <?php
                        foreach (Yii::app()->user->getFlashes() as $key => $message) {
                            if ($key == 'error' || $key == 'success' || $key == 'notice') {
                                echo "<div class='flash-{$key}'>{$message}</div>";
                            }
                        }

                        ?>
                        <?php echo $content; ?>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php $this->endContent(); ?>