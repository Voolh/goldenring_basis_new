<!DOCTYPE html>
<?php
$isRTL = Lang::isRTLLang(Yii::app()->language);

$cs = Yii::app()->clientScript;
$cs->coreScriptPosition = CClientScript::POS_BEGIN;

$cs->defaultScriptFilePosition = CClientScript::POS_BEGIN;
$cs->defaultScriptPosition = CClientScript::POS_END;

$baseThemeUrl = Yii::app()->theme->baseUrl;
$baseThemePath = Yii::app()->theme->basePath;

BasisAssets::register();

?>
<html lang="<?php echo Yii::app()->language; ?>">
    <head>
        <title><?php echo CHtml::encode($this->seoTitle ? $this->seoTitle : $this->pageTitle); ?></title>
        <meta name="description" content="<?php echo CHtml::encode($this->seoDescription ? $this->seoDescription : $this->pageDescription); ?>" />
        <meta name="keywords" content="<?php echo CHtml::encode($this->seoKeywords ? $this->seoKeywords : $this->pageKeywords); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/favicon.ico" type="image/x-icon">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic" rel="stylesheet">
    </head>

    <body class="<?= isset($this->aData['bodyClass']) ? $this->aData['bodyClass'] : '' ?> <?php echo ($isRTL) ? 'rtl' : ''; ?>">
        
        <?php if (demo()) :?>
            <style>
                div.header { padding-top: 48px; }
                @media screen and (max-width: 800px){
                    div.header { padding-top: 0px; }
                }
            </style>
            <?php $this->renderPartial('//site/ads-block', array()); ?>
            <div class="clear"></div>
        <?php endif; ?>

        <?php require '_header.php' ?>

        <?php echo $content ?>

        <?php if (issetModule('advertising')) : ?>
            <div class="content container">
                <?php $this->renderPartial('//modules/advertising/views/advert-bottom', array()); ?>
            </div>
        <?php endif; ?>

        <?php require '_footer.php' ?>
    </body>
</html>