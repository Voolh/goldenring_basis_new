<?php $this->beginContent('//layouts/main'); ?>

<?php 
$this->aData['bodyClass'] = 'inner_page';
$showHideFilter = (isset($showHideFilter)) ? $showHideFilter : true;
$compact = ($showHideFilter == false) ? false : param("useCompactInnerSearchForm", false);
$searchUrl = isset($this->aData['searchUrl']) ? $this->aData['searchUrl'] : Yii::app()->controller->createUrl('/quicksearch/main/mainsearch');
$useTopFullWidthInnerSearchForm = false;
?>

<?php if ($useTopFullWidthInnerSearchForm): ?>
    <div class="search_inner_top custom_search_inner_top">
        <?php Yii::app()->controller->renderPartial('//site/index-search', array('searchUrl' => $searchUrl)); ?>
    </div>
    <div class="clear"></div>
<?php endif; ?>

<div class="content main_content <?php echo ($useTopFullWidthInnerSearchForm) ? 'custom_search_main_content' : ''?> container">
    <div class="content_center box">

        <div class="row in_row">

            <div class="col-md-9 property_block">

                <div class="row">

                    <?php if (isset($this->breadcrumbs) && $this->breadcrumbs): ?>
                        <?php
                        $this->widget('zii.widgets.CBreadcrumbs', array(
                            'tagName' => 'ul',
                            'homeLink' => '<li class="breadcrumb-item">' . CHtml::link(tc('Home'), Yii::app()->homeUrl, array('class' => 'path')). '</li>',
                            'links' => $this->breadcrumbs,
                            'htmlOptions' => array('class' => 'breadcrumb'),
                            'separator' => false,
                            'activeLinkTemplate' => '<li class="breadcrumb-item"><a  href="{url}">{label}</a></li>',
                            'inactiveLinkTemplate' => '<li class="breadcrumb-item"><a href="javascript: void(0);" >{label}</a></li>',
                        ));

                        ?>
                    <?php endif; ?>

                    <?php
                    foreach (Yii::app()->user->getFlashes() as $key => $message) {
                        if ($key == 'error' || $key == 'success' || $key == 'notice') {
                            echo "<div class='flash-{$key}'>{$message}</div>";
                        }
                    }

                    ?>

                    <?php if (issetModule('advertising')) : ?>
                        <?php $this->renderPartial('//modules/advertising/views/advert-top', array()); ?>
                    <?php endif; ?>

                    <div class="main-content-wrapper">
                        <?php echo $content; ?>
                    </div>

                </div>

            </div>

            <div class="col-md-3 sidebar_left">
                <div id="sticker">

                    <?php if (!$useTopFullWidthInnerSearchForm):?>
                        <div class="box-content search-inner">
                            <?php Yii::app()->controller->renderPartial('//site/inner-search'); ?>
                        </div>
                    <?php endif;?>

                    <?php
                    $lastNews = Entries::getLastForCategory(1, 4);
                    if (isset($lastNews) && $lastNews) {

                        ?>

                        <div class="news_block box widget <?php echo ($useTopFullWidthInnerSearchForm) ? 'news_block_search_inner_top' : ''?>">
                            <div class="h3 h_line"><span><?php echo tc('News') ?></span></div>

                            <div class="news_content">
                                <?php foreach ($lastNews as $entries) { ?>
                                    <?php
                                    $src = Yii::app()->theme->baseUrl . '/assets/images/news_no_photo_272x181.jpg';
                                    if ($entries->image) {
                                        $src = Yii::app()->getBaseUrl() . '/uploads/entries/' . $entries->image->getThumb(272, 181, true);
                                    }
                                    $title = $entries->getStrByLang('title');
                                    $url = $entries->getUrl();
                                    $announce = ($entries->getAnnounce()) ? $entries->getAnnounce() : '&nbsp;';
                                    $tagLinks = $entries->getTagLinks();

                                    ?>
                                    <div class="item_news">
                                        <div class="img_news">
                                            <a href="<?php echo $url ?>"><img src="<?php echo $src ?>" alt="<?php echo $title ?>"></a>
                                            <?php if ($tagLinks) { ?>
                                                <ul class="list-inline tags">
                                                    <?php foreach ($tagLinks as $link) { ?>
                                                        <li><?php echo $link ?></li>
                                                    <?php } ?>
                                                </ul>
                                            <?php } ?>
                                        </div>
                                        <div class="h5">
                                            <a href="<?php echo $url ?>"><?php echo $title ?></a><span class="date_news"><?= $entries->getDateTimeInFormat('date_created') ?></span>
                                        </div>

                                        <div class="text_news"><p><?php echo truncateText($announce, 20); ?></p></div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                    <?php }; ?>

                    <?php if (1) { ?>

                        <div class="menu_sidebar">
                            <div class="box-content">
                                <div id="jquery-accordion-menu" class="jquery-accordion-menu blue">
                                    <?php
                                    $this->widget('BasisMenu', array(
                                        'id' => 'acc-menu1',
                                        'items' => Menu::getMenuItems(false, 4),
                                        'htmlOptions' => array('class' => 'acc_menu'),
                                        'encodeLabel' => false,
                                        'activateParents' => true,
                                    ));

                                    ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>

                    <?php } ?>

                </div>

            </div>

        </div>
    </div>
</div>
<?php $this->endContent(); ?>