<div class="header">
    <div class="header_top">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-3 top_menu">

                    <div class="mobile-button hidden-lg hidden-md" data-toggle="offcanvas" data-target=".navmenu.mobcats" data-canvas="body">
                        <i class="fa fa-bars"></i>&nbsp; <span><?php echo tc('Menu') ?></span>
                    </div>
                    <nav class="navbar hidden-sm hidden-xs">
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <?php
                            $headerTopMenuItems = Menu::getMenuItems(false, 3);
                            if (!empty($headerTopMenuItems)) {
                                $this->widget('BasisMenu', array(
                                    'id' => 'main_menu_nav_1',
                                    'items' => $headerTopMenuItems,
                                    'htmlOptions' => array('class' => 'nav navbar-nav'),
                                    'encodeLabel' => false,
                                    'activateParents' => true,
                                ));
                            } else {
                                echo '<div style="min-height: 68px; height: 68px;">&nbsp;</div>';
                            }
                            ?>
                        </div>
                    </nav>
                </div>
                <div class="col-md-7 col-sm-7 right_top_block">
                    <div class="row">

                        <?php
                        if (issetModule('comparisonList')) {
                            $countComparison = ComparisonList::getCountIn();

                            ?>
                            <div <?php echo!$countComparison ? 'style="display: none"' : '' ?> id="comparison_top">
                                <a href="<?php echo Yii::app()->createUrl('comparisonList/main/index') ?>" class="btn btn-primary">
                                    <i class="fa fa-list"></i>
                                    <?php echo Yii::t('common', '{n} in the comparison list', array('{n}' => $countComparison)) ?>
                                </a>
                            </div>
                        <?php } ?>

                        <div class="col-md-3 col-sm-3 currency_block white_select">
                            <?php $this->widget('BasisCurrencySelector'); ?>
                        </div>

                        <div class="col-md-3 col-sm-3 btn-group language_block white_select">
                            <?php $this->widget('BasisLangSelector'); ?>
                        </div>

                        <?php // переключатель цветовой темы  ?>
                        <?php if (0) { ?>
                            <div class="col-md-3 col-sm-3 change_color_block">
                                <div class="btn-group" data-toggle="buttons" id="change_color">
                                    <label class="btn btn-info">
                                        <input type="radio"  id="violet" autocomplete="off">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </label>
                                    <label class="btn btn-primary active">
                                        <input type="radio"  id="blue" autocomplete="off">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </label>
                                    <label class="btn btn-warning">
                                        <input type="radio"  id="orange" autocomplete="off">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </label>
                                    <label class="btn btn-success">
                                        <input type="radio"  id="green" autocomplete="off" chacked="">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </label>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="col-md-3 col-sm-3 personal_cabinet">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-user" aria-hidden="true"></i> <?= Yii::t('common', 'Control panel') ?> <span class="caret"></span>
                                </button>
                                <?php
                                $this->widget('BasisMenu', array(
                                    'id' => 'main_menu_nav_2',
                                    'items' => BasisMenuHelper::getControlMenu(),
                                    'htmlOptions' => array('class' => 'dropdown-menu'),
                                    'encodeLabel' => false,
                                    'activateParents' => true,
                                ));

                                ?>

                                <?php if (0) { ?>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="#">Войти</a></li>
                                        <li><a class="dropdown-item" href="#">Регистрация</a></li>
                                        <li><a class="dropdown-item" href="#">Забыли пароль?</a></li>
                                    </ul>
                                <?php } ?>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="header_bottom">
    <div class="container">
      <div class="gr-contacts col-xs-12 col-lg-4">
        <div class="gr-contacts__address"><?php echo param('adminAddress'); ?></div>
        <div class="gr-contacts__phone"><?php echo param('adminPhone'); ?></div>
      </div>
        <div class="logo text-center col-xs-12 col-lg-4">
            <?php if (0) { ?>
                <a title="<?php echo Yii::t('common', 'Go to main page'); ?>" href="<?php echo Yii::app()->controller->createAbsoluteUrl('/'); ?>">
                    <?php if (!param('golden') || (param('golden') && Yii::app()->params->golden['logoImgBg'])) { ?>
                      <div class="logo-img">
                        <img width="77" height="70" alt="<?php echo CHtml::encode(Yii::app()->name); ?>" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/pages/logo-open-ore.png" />
                      </div>
                    <?php } ?>
                    <div class="logo-text"><?php echo CHtml::encode(Yii::app()->name); ?></div>
                </a>
            <?php } ?>

            <a href="<?= Yii::app()->createAbsoluteUrl('/') ?>">
                <div class="logo-img"><img src="<?php echo $baseThemeUrl ?>/assets/images/logo.png" alt="<?php echo CHtml::encode(Yii::app()->name); ?>"></div>
                <?php if (!param('golden') || (param('golden') && Yii::app()->params->golden['logoText'])) { ?>
                  <div class="logo-text"><?php echo CHtml::encode(Yii::app()->name); ?></div>
                <?php } ?>
              <?php echo $contactData ?>
            </a>
        </div>
        <div class="row col-xs-12 col-lg-4">

            <div class="col-md-9 col-sm-9 hidden-xs main_menu">
                <nav class="navbar">

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                        <?php
                        $this->widget('BasisMenu', array(
                            'id' => 'main_menu_nav_3',
                            'items' => Menu::getMenuItems(false, 4),
                            'htmlOptions' => array('class' => 'nav navbar-nav'),
                            'encodeLabel' => false,
                            'activateParents' => true,
                        ));

                        ?>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="m_menu">
    <nav class="mobilemenu navmenu mobcats navmenu-default navmenu-fixed-left offcanvas canvas-slid">
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <?php
            $this->widget('BasisMenuDropDown', array(
                'id' => 'sf-menu-id7',
                'items' => Menu::getMenuItems(false, 3),
                'htmlOptions' => array('class' => 'nav navbar-nav'), //nav-pills
                'encodeLabel' => false,
                'activateParents' => true,
                'mobile' => true
            ));
            echo '<hr>';
            $this->widget('BasisMenuDropDown', array(
                'id' => 'sf-menu-id8',
                'items' => Menu::getMenuItems(false, 4),
                'htmlOptions' => array('class' => 'nav navbar-nav'),
                'encodeLabel' => false,
                'activateParents' => true,
                'mobile' => true
            ));

            ?>
        </div>
    </nav>
</div>