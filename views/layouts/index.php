<?php $this->beginContent('//layouts/main'); ?>

<div class="search_index">
    <?php Yii::app()->controller->renderPartial('//site/index-search'); ?>
</div>

<div class="content main_content container">

    <?php if (issetModule('advertising')) : ?>
        <?php $this->renderPartial('//modules/advertising/views/advert-top', array()); ?>
    <?php endif; ?>

    <?php if (Themes::getParamJson('i_enable_slider_and_pd')) { ?>
        <div class="row">
            <?php if (issetModule('seo')):?>
                <div class="col-md-7 col-sm-12 col-xs-12">
                    <?php Yii::app()->controller->renderPartial('//site/_slider_homepage', array('images' => Yii::app()->controller->imagesForIndexPageSlider));

                    ?>
                </div>

                <div class="col-md-5 col-sm-12 col-xs-12">
                    <?php $this->widget('BasisPopularDestWidget') ?>
                </div>
            <?php else: ?>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <?php Yii::app()->controller->renderPartial('//site/_slider_homepage', array('images' => Yii::app()->controller->imagesForIndexPageSlider));

                    ?>
                </div>
            <?php endif;?>
        </div>
    <?php } ?>

    <div class="content_center main-content-wrapper box">
        <?php
        foreach(Yii::app()->user->getFlashes() as $key => $message) {
            if ($key=='error' || $key == 'success' || $key == 'notice'){
                echo "<div class='flash-{$key}'>{$message}</div>";
            }
        }
        ?>
        
        <?php echo $content ?>
    </div>

    <?php
    if (Themes::getParamJson('i_enable_best_ads')) {
        $this->widget('BasisHotAdsWidget');
    }

    ?>
</div>

<?php
if (Themes::getParamJson('i_enable_feature')) {
    Yii::app()->controller->renderPartial('//site/_index_feature_block');
}

?>

<?php if (Themes::getParamJson('i_enable_last_news') || Themes::getParamJson('i_enable_contact')) { ?>
    <div class="content main_content container">
        <?php if (Themes::getParamJson('i_enable_last_news')) { ?>
            <?php $this->widget('BasisNewsWidget', array('entries' => Entries::getLastNews())) ?>
        <?php } ?>

        <?php if (Themes::getParamJson('i_enable_contact')) { ?>
            <?php $this->widget('BasisContactWidget') ?>
        <?php } ?>
    </div>
<?php } ?>


<?php $this->endContent(); ?>