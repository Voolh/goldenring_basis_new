<?php
if (issetModule('comparisonList')) {

    $urlAdd = Yii::app()->createUrl('/comparisonList/main/add');
    $urlDel = Yii::app()->createUrl('/comparisonList/main/del');
    $urlList = Yii::app()->createUrl('comparisonList/main/index');
    $maxCompareString = Yii::t("module_comparisonList", "max_limit", array('{n}' => param('countListingsInComparisonList', 6)));
    $errorString = tc("Error");
    $inComparisonString = tt('In the comparison list', 'comparisonList');
    $addToComparisonString = tt('Add to a comparison list ', 'comparisonList');

    $script = <<< JS
    
    $(document).on("click", "a.compare-label", function() {
        apId = $(this).attr("data-id");
        apId = apId.replace("compare_label", "");

        if ($(this).attr("data-rel-compare") == "false") {
            if (apId) {
                var checkboxCompare = $('[data-id="#compare_check"+apId]');

                if (checkboxCompare.is(":checked"))
                    checkboxCompare.prop("checked", false);
                else {
                    checkboxCompare.prop("checked", true);
                }
                addCompare(apId);
            }
        }
    });

    $(document).on("change", ".compare-check", function() {
        apId = $(this).attr("data-id");
        apId = apId.replace("compare_check", "");

        addCompare(apId);
    });

    function addCompare(apId) {
        apId = apId || 0;

        if (apId) {
            var controlCheckedCompare = $('input[data-id="compare_check'+apId+'"]').prop("checked");

            if (!controlCheckedCompare) {
                deleteCompare(apId);
            }
            else {
                $.ajax({
                    type: "POST",
                    url: '$urlAdd',
                    data: {apId: apId, isJson: 1},
                    dataType: 'json',
                    success: function(data){
                        if (data.status == "ok") {
                            showCompareBar(data);
                            $('[data-id="compare_label'+apId+'"]').html("$inComparisonString");
                            $('[data-id="compare_label'+apId+'"]').prop("href", "$urlList");
                            $('[data-id="compare_label'+apId+'"]').attr("data-rel-compare", "true");
                        }
                        else {
                            $('[data-id="compare_check"+apId]').prop("checked", false);

                            if (data.error == "max_limit") {
                                $('[data-id="compare_label'+apId+'"]').html("$maxCompareString");
                            }
                            else {
                                $('[data-id="compare_label'+apId+'"]').html("$errorString");
                            }
                        }
                    }
                });
            }
        }
    }

    function deleteCompare(apId) {
        $.ajax({
            type: "POST",
            url: "$urlDel",
            data: {apId: apId, isJson: 1},
            dataType: 'json',
            success: function(data){
                if (data.status == "ok") {
                    showCompareBar(data);
                    $('[data-id="compare_label'+apId+'"]').html("$addToComparisonString");
                    $('[data-id="compare_label'+apId+'"]').prop("href", "javascript:void(0);");
                    $('[data-id="compare_label'+apId+'"]').attr("data-rel-compare", "false");
                }
                else {
                    $('[data-id="compare_check'+apId+'"]').prop("checked", true);
                    $('[data-id="compare_label'+apId+'"]').html("$errorString");
                }
            }
        });
    }
    
    function showCompareBar(data) {
       if(data.count > 0){
           $('#comparison_top a').html(data.countString);
           $('#comparison_top').show();
       } else {
           $('#comparison_top').hide();
       }
    }
JS;


    Yii::app()->getClientScript()->registerScript('compare-functions-end', $script, CClientScript::POS_END, array(), true);
}
