<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 footer_bl1">
                <div class="h3 h_line"><?php echo tt('About us', 'theme_basis') ?></div>
                <div class="footer_content">
                    <div class="row">
                        <div class="col-md-6 logo_footer">
                            <div class="logo">
                                <a href="<?= Yii::app()->createAbsoluteUrl('/') ?>">
                                    <div class="logo-img"><img
                                            src="<?php echo $baseThemeUrl ?>/assets/images/logo.png"
                                            alt="<?php echo CHtml::encode(Yii::app()->name); ?>"></div>
                                    <?php if (!param('golden') || (param('golden') && Yii::app()->params->golden['logoText'])) { ?>
                                    <div class="logo-text"><?php echo CHtml::encode(Yii::app()->name); ?></div>
                                    <?php } ?>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 contact_footer">
                            <ul class="list-unstyled">
                                <li><i class="fas fa-envelope"></i> <?php echo $this->protectEmail(tt('index_email', 'theme_basis')) ?></li>
                                <li><i class="fab fa-skype"></i> <?php echo tt('index_skype', 'theme_basis') ?></li>
                                <li><i class="fas fa-phone"></i> <?php echo tt('index_phone', 'theme_basis') ?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="about_product">
                        <p><?php echo tt('index_about_text', 'theme_basis') ?></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-4 footer_bl2">
                <?php $footerBottomMenuItems = Menu::getMenuItems(false, 5);?>
                <?php if (!empty($footerBottomMenuItems)):?>
                    <div class="h3 h_line"><?php echo tt('Helpful information', 'theme_basis') ?></div>
                    <div class="footer_content">
                        <?php
                        $this->widget('BasisMenu', array(
                            'id' => 'main_menu_nav',
                            'items' => $footerBottomMenuItems,
                            'htmlOptions' => array('class' => 'nav navbar-nav'),
                            'encodeLabel' => false,
                            'activateParents' => true,
                        ));

                        ?>
                        <?php
                        // пример своего меню
                        if (0) {

                            ?>
                            <ul class="nav navbar-nav">
                                <li><a href="#" class="slow">Ссылка 1</a></li>
                                <li><a href="#" class="slow">Ссылка 2</a></li>
                                <li><a href="#" class="slow">Ссылка 3</a></li>
                            </ul>
                        <?php } ?>
                    </div>
                <?php endif;?>
            </div>

            <div class="col-md-4 col-sm-4 footer_bl3 text-right">
                <div class="h3 h_line"></div>
                <div class="footer_content">
                    <ul class="list-unstyled">
                        <li><a href="<?php echo Yii::app()->createUrl('/booking/main/mainform') ?>" class="slow btn btn-default fancy"><?php echo tt('Reserve apartment', 'common') ?></a></li>
                        <li class="plus"><a href="<?php echo Yii::app()->createUrl('/guestad/add') ?>" class="slow btn btn-primary"><?php echo tc('List your property') ?></a></li>
                        <li class="lk"><a href="<?php echo Yii::app()->createUrl('/usercpanel/main/index') ?>" class="slow btn btn-default"><?php echo Yii::t('common', 'Control panel') ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer_bottom">
    <div class="container">
        <div class="row">
            <?php echo getGA(); ?>
            <?php echo getJivo(); ?>
            <div class="col-md-6 col-sm-6">&copy; <?php echo CHtml::encode(Yii::app()->name); ?>,    2011—<?php echo date('Y') ?></div>
            <div class="col-md-6 col-sm-6 text-right">
                <?php
                $vkUrl = Themes::getParamJson('i_vk');
                $fbUrl = Themes::getParamJson('i_facebook');
                $twUrl = Themes::getParamJson('i_twitter');
                if($vkUrl || $fbUrl || $twUrl){
                    echo tt('We are in social networks', 'theme_basis').' — ';
                    if ($vkUrl) echo '&nbsp;<a href="'.$vkUrl.'" target="_blank"><i class="fab fa-vk"></i></a>';
                    if ($fbUrl) echo '&nbsp;<a href="'.$fbUrl.'" target="_blank"><i class="fab fa-facebook-f"></i></a>';
                    if ($twUrl) echo '&nbsp;<a href="'.$twUrl.'" target="_blank"><i class="fab fa-twitter"></i></a>';
                }
                ?>
            </div>
        </div>
    </div>
</div>

<div id="loading" style="display:none;"><?php echo Yii::t('common', 'Loading content...'); ?></div>
<div id="loading-blocks" style="display:none;"></div>
<div id="overlay-content" style="display:none;"></div>
<div id="toTop">^ <?php echo tc('Go up'); ?></div>

<?php
$cs->registerScript('main-vars', '
		var BASE_URL = ' . CJavaScript::encode(Yii::app()->baseUrl) . ';
		var CHANGE_SEARCH_URL = ' . CJavaScript::encode(Yii::app()->createUrl('/quicksearch/main/mainsearch/countAjax/1')) . ';
		var INDICATOR = "' . Yii::app()->theme->baseUrl . "/images/pages/indicator.gif" . '";
		var LOADING_NAME = "' . tc('Loading ...') . '";
		var params = {
			change_search_ajax: ' . param("change_search_ajax", 1) . '
		}
	', CClientScript::POS_BEGIN, array(), true);

$this->renderPartial('//layouts/_common');

$this->widget('application.modules.fancybox.EFancyBox', array(
    'target' => 'a.fancy',
    'config' => array(
        'ajax' => array('data' => "isFancy=true"),
        'titlePosition' => 'inside',
        'onClosed' => 'js:function(){
						var capClick = $(".get-new-ver-code");
						if(typeof capClick !== "undefined")	{ 
							capClick.click(); 
						}
					}'
    ),
    )
);

if (Yii::app()->user->checkAccess('apartments_admin')) {
    $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/tooltip/jquery.tipTip.js', CClientScript::POS_END);
    $cs->registerScript('adminMenuToolTip', '
			$(function(){
				$(".adminMainNavItem").tipTip({maxWidth: "auto", edgeOffset: 10, delay: 200});
			});
		', CClientScript::POS_READY);

    ?>

    <div class="admin-menu-small <?php echo demo() ? 'admin-menu-small-demo' : ''; ?> ">
        <a href="<?php echo (Yii::app()->user->checkAccess('stats_admin') === true) ? Yii::app()->baseUrl . '/stats/backend/main/admin' : Yii::app()->baseUrl . '/apartments/backend/main/admin' ?>" title="<?php echo Yii::t('common', 'Administration'); ?>">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/adminmenu/administrator.png" alt="<?php echo Yii::t('common', 'Administration'); ?>" title="<?php echo Yii::t('common', 'Administration'); ?>" class="adminMainNavItem" />
        </a>
    </div>
<?php } ?>

<?php
if (param('useShowInfoUseCookie') && isset(Yii::app()->controller->privatePolicyPage) && !empty(Yii::app()->controller->privatePolicyPage)) {
    $privatePolicyPage = Yii::app()->controller->privatePolicyPage;
    $cs->registerScript('display-info-use-cookie-policy', '
					$.cookieBar({/*acceptOnContinue:false, */ fixed: true, bottom: true, message: "' . CHtml::encode(Yii::app()->name) . ' ' . CHtml::encode(tc('uses cookie')) . ', <a href=\"' . $privatePolicyPage->getUrl() . '\" target=\'_blank\'>' . $privatePolicyPage->getStrByLang('title') . '</a>", acceptText : "X"});
				', CClientScript::POS_READY);
}

?>