<?php $this->beginContent('//layouts/main'); ?>

<?php $this->aData['bodyClass'] = 'inner_page' ?>

<div class="content main_content  container">
    <div class="content_center box">
        <?php if (issetModule('advertising')) : ?>
            <?php $this->renderPartial('//modules/advertising/views/advert-top', array()); ?>
        <?php endif; ?>
        
        <?php if (isset($this->breadcrumbs) && $this->breadcrumbs): ?>
            <?php
            $this->widget('zii.widgets.CBreadcrumbs', array(
                'tagName' => 'ul',
                'homeLink' => '<li class="breadcrumb-item">' . CHtml::link(tc('Home'), Yii::app()->homeUrl, array('class' => 'path')). '</li>',
                'links' => $this->breadcrumbs,
                'htmlOptions' => array('class' => 'breadcrumb'),
                'separator' => false,
                'activeLinkTemplate' => '<li class="breadcrumb-item"><a  href="{url}">{label}</a></li>',
                'inactiveLinkTemplate' => '<li class="breadcrumb-item"><a href="javascript: void(0);" >{label}</a></li>',
            ));

            ?>
        <?php endif; ?>

        <?php
        foreach (Yii::app()->user->getFlashes() as $key => $message) {
            if ($key == 'error' || $key == 'success' || $key == 'notice') {
                echo "<div class='flash-{$key}'>{$message}</div>";
            }
        }

        ?>

        <?php echo $content; ?>

    </div>
</div>
<?php $this->endContent(); ?>