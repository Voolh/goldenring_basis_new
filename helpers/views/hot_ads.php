<div class="box hot_block">
    <div class="h3 h_line"><?php echo tc('Best listings') ?></div>
    <div class="box-content">

        <div class="tab_header">
            <ul class="nav nav-tabs" id="hot_tabs_link">
                <?php
                $i = 0;
                foreach ($tabsData as $id => $data) {
                    if ($i == 0) {
                        echo '<li class="active"><a href="#Tab' . $id . '" class="slow"><span>' . $data['name'] . '</span></a></li>';
                    } else {
                        echo '<li><a href="#Tab' . $id . '" class="slow"><span>' . $data['name'] . '</span></a></li>';
                    }
                    $i++;
                }

                ?>
            </ul>
        </div>

        <div class="hot_block_content tab-content">
            <?php
            $i = 0;

            ?>
            <?php foreach ($tabsData as $key => $data) : ?>
                <div class="tab-pane row fade <?php
                if ($i == 0) {
                    echo 'active in';
                }

                ?>" id="Tab<?php echo $key ?>">

                    <?php
                    $findIds = $countImagesArr = array();
                    foreach ($data['ads'] as $item) {
                        $findIds[] = $item->id;
                    }

                    if (count($findIds) > 0) {
                        $countImagesArr = Images::getApartmentsCountImages($findIds);
                    }

                    $i++;
                    foreach ($data['ads'] as $item) :
                        $adUrl = $item->getUrl();
                        if (!$item) {
                            continue;
                        }
                        $url = $item->getUrl();
                        $title = CHtml::encode($item->getStrByLang('title'));

                        $addClass = '';
                        if (!empty($item->date_up_search) && !is_null($item->date_up_search)) {
                            $addClass = 'up_in_search';
                        }

                        ?>

                        <div class="apartment_item col-md-4 col-sm-6 <?php echo $addClass ?>">

                            <div class="item slow <?php
                            if ($key % 2 == 0) {
                                echo 'even';
                            }

                            ?>">

                                <div class="photo_block">
                                    <div class="item-photo item-photo_large">
                                        <?php
                                        if (isset($item->images) && !empty($item->images)) {
                                            if ($item->images) {

                                                ?>
                                                <a href="<?php echo $url; ?>"
                                                   class="js-item-slider item-slider large-picture">
                                                       <?php
                                                       $this->widget('application.modules.images.components.ImagesWidgetAvito', array(
                                                           'images' => $item->images,
                                                           'objectId' => $item->id,
                                                           'width' => 330,
                                                           'height' => 228
                                                       ));

                                                       ?>
                                                </a>

                                                <?php
                                            }
                                        } else {

                                            ?>

                                            <a href="<?php echo $url; ?>"
                                               class="js-item-slider item-slider large-picture">
                                                <ul class="item-slider-list js-item-slider-list">
                                                    <li class="item-slider-item js-item-slider-item">
                                                        <?php
                                                        $res = Images::getMainThumb(320, 238, $item->images);
                                                        $imgAlt = (isset($res['alt']) && $res['alt']) ? $res['alt'] : CHtml::encode($item->getStrByLang('title'));

                                                        ?>
                                                        <div class="item-slider-image large-picture"
                                                             style="background-image: url(<?php echo $res['thumbUrl']; ?>)"></div>
                                                    </li>
                                                </ul>
                                            </a>

                                        <?php } ?>
                                    </div>

                                    <?php if ($item->is_special_offer) { ?>
                                        <div class="hot_label"><i
                                                class="fab fa-hotjar"></i><?php echo tc('Special offer!') ?></div>
                                        <?php } ?>

                                    <?php if ($item->rating): ?>
                                        <?php $countComments = (isset($item->countComments) && $item->countComments) ? $item->countComments : 0; ?>

                                        <div class="rating item-small-block-rating">
                                            <div class="item-rating-grade">
                                                <?php //echo number_format((float)round($item->rating, 1, PHP_ROUND_HALF_DOWN), 1, '.', ''); ?>
                                                <?php echo $item->rating; ?>
                                            </div>
                                            <?php if ($countComments): ?>
                                                <div class="item-view-all-comments">
                                                    <a href="<?php echo $item->getUrl(); ?>">
                                                        <?php echo Yii::t('common', '{n} review|{n} reviews', $countComments); ?>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>

                                <div class="item_teaser_content">

                                    <div class="title_item"><?php echo '<a href="' . $url . '" class="slow">#' . $item->id . ', ' . truncateText($title, 7) . '</a>';

                                    ?></div>

                                    <ul class="adress_info list-unstyled">
                                        <li>
                                            <i class="fas fa-map-marker"></i> <strong><?php echo HApartment::getLocationString($item); ?></strong>
                                        </li>
                                    </ul>
                                    
                                    <?php if ($item->num_of_rooms || $item->floor || $item->floor_total || $item->square) { ?>
                                        <ul class="list-inline">
                                            <?php if ($item->canShowInView('num_of_rooms')) { ?>
                                                <li> <strong><i class="fas fa-columns"></i><?php echo Yii::t('module_apartments', '{n} bedroom|{n} bedrooms|{n} bedrooms', array($item->num_of_rooms));

                                                ?></strong></li>
                                            <?php } ?>
                                            <?php
                                            if (($item->floor) && ($item->floor_total)) {
                                                echo '<li><i class="fas fa-building"></i> ';
                                                echo ' <strong>' . tt('Floor', 'apartments') . '/' . tt('Total number of floors', 'apartments');
                                                echo ': </strong>' . $item->floor . '/' . $item->floor_total;
                                                echo '</li>';
                                            } elseif ($item->floor) {
                                                echo '<li><i class="fas fa-building"></i> <strong>' . tt('Floor', 'apartments') . ':</strong> ' . $item->floor . '</li>';
                                            } elseif ($item->floor_total) {
                                                echo '<li><i class="fas fa-building"></i> <strong>' . tt('Total number of floors', 'apartments') . ': </strong>' . $item->floor_total . '</li>';
                                            }

                                            ?>

                                            <?php
                                            if ($item->canShowInView('square')) {
                                                echo '<li><i class="fas fa-arrows-alt"></i> <strong>' . utf8_ucfirst(Yii::t('module_apartments', 'total square: {n}', $item->square)) . " " . tc('site_square') . '</strong></li>';
                                            }

                                            ?>
                                        </ul>
                                    <?php } ?>

                                    <?php if ($item->canShowInView('price')) { ?>
                                        <ul class="price list-unstyled">
                                            <?php
                                            if ($item->is_price_poa) {
                                                echo '<li>' . tt('is_price_poa', 'apartments') . '</li>';
                                            } else {
                                                echo '<li>' . $item->getPrettyPrice(false) . '</li>';
                                            }

                                            ?>
                                        </ul>
                                    <?php } ?>

                                    <?php if (0) { ?>
                                        <div class="descr">
                                            <?php
                                            if ($item->canShowInView('description')) {
                                                $description = $item->getStrByLang('description');
                                            }
                                            if (utf8_strlen($description) > 110) {
                                                $description = utf8_substr($description, 0, 110) . '...';
                                            }
                                            //echo $description;
                                            echo truncateText($description, 22);

                                            ?>

                                        </div>
                                    <?php } ?>

                                    <?php //echo '<p><a href="' . $url . '" class="slow read_more">Подробнее</a></p>';

                                    ?>

                                </div>

                                <div class="compare_block">
                                    <div class="col-md-7 col-sm-7 col-xs-7 compare text-center">
                                        <?php if (issetModule('comparisonList')) { ?>
                                            <?php
                                            $inComparisonList = false;
                                            if (in_array($item->id, Yii::app()->controller->apInComparison)) {
                                                $inComparisonList = true;
                                            }

                                            ?>
                                            <div class="row compare-check-control"
                                                 data-id="compare_check_control_<?php echo $item->id; ?>">
                                                <div class="pretty p-default">
                                                    <?php
                                                    $checkedControl = '';

                                                    if ($inComparisonList) {
                                                        $checkedControl = ' checked = checked ';
                                                    }

                                                    ?>
                                                    <input type="checkbox" name="compare<?php echo $item->id; ?>"
                                                           class="compare-check compare-float-left"
                                                           data-id="compare_check<?php echo $item->id; ?>" <?php echo $checkedControl; ?>>
                                                    <div class="state p-warning">
                                                        <label>
                                                            <a href="<?php echo ($inComparisonList) ? Yii::app()->createUrl('comparisonList/main/index') : 'javascript:void(0);'; ?>"
                                                               data-rel-compare="<?php echo ($inComparisonList) ? 'true' : 'false'; ?>"
                                                               data-id="compare_label<?php echo $item->id; ?>"
                                                               class="compare-label">
                                                                   <?php
                                                                   echo ($inComparisonList) ? tt('In the comparison list', 'comparisonList') : tt('Add to a comparison list ', 'comparisonList');

                                                                   ?>
                                                            </a>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5 photo_count text-center">
                                        <?php
                                        $countPhoto = isset($countImagesArr[$item->id]) ? $countImagesArr[$item->id] : 0;
                                        if ($countPhoto > 0) {

                                            ?>
                                            <i class="fas fa-images"></i><?php echo $countPhoto ?> <span><?php echo Yii::t('common', 'photo|photos|photos', $countPhoto) ?></span>
                                        <?php } else { ?>
                                            <i class="fas fa-images"></i> <span><?php echo Yii::t('common', 'Without photo') ?></span>
                                        <?php } ?>
                                    </div>
                                </div>

                            </div>

                        </div>

                    <?php endforeach; ?>

                </div>
            <?php endforeach; ?>
            <div class="clear"></div>
        </div>

    </div>
</div>
