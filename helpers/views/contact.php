<div class="map_block">

    <?php
    $map = new HMap(array(
        'lat' => Themes::getParamJson('i_lat'),
        'lng' => Themes::getParamJson('i_lng'),
        'zoom' => Themes::getParamJson('i_zoom'),
    ));

    $map->createMapWithMarker();
    ?>

    <div class="userForm" id="feedbackForm">
        <?php
        Yii::import('application.modules.contactform.models.ContactForm');
        $model = new ContactForm();

        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'contact-form',
            'enableClientValidation' => false,
            'htmlOptions' => array('class' => 'form-disable-button-after-submit'),
            'action' => Yii::app()->createUrl('/contactform/main/index')
        ));

        ?>

        <div class="h3 h_line"><?php echo tt('Contact Us', 'contactform'); ?></div>


        <div class="form-group">
            <?php
            echo $form->textField($model, 'name', array(
                'size' => 60,
                'maxlength' => 128,
                'class' => 'form-control',
                'placeholder' => $model->getAttributeLabel('name')
            ));

            ?>
        </div>

        <div class="form-group">
            <?php
            echo $form->textField($model, 'email', array(
                'size' => 60,
                'maxlength' => 128,
                'class' => 'form-control',
                'placeholder' => $model->getAttributeLabel('email')
            ));

            ?>
        </div>

        <div class="form-group">
            <?php
            echo $form->textField($model, 'phone', array(
                'size' => 60,
                'maxlength' => 128,
                'class' => 'form-control',
                'placeholder' => $model->getAttributeLabel('phone')
            ));

            ?>
        </div>

        <div class="form-group">
            <?php
            $cssClass = Yii::app()->user->isGuest ? 'contact-textarea-guest' : 'contact-textarea-user';
            echo $form->textArea($model, 'body', array(
                'rows' => 3,
                'cols' => 50,
                'class' => $cssClass . ' form-control',
                'style' => 'resize: none;',
                'placeholder' => $model->getAttributeLabel('body')
            ));

            ?>
        </div>

        <?php
        if (Yii::app()->user->isGuest) {

            ?>
            <div class="form-group">
                <?php 
                    $display = (param('useReCaptcha', 0)) ? 'none;' : 'block;';
                    $cAction = '/contactform/main/captcha';
                ?>
                
                <?php if (param('useReCaptcha', 0)):?>
                    <div class="col-md-12">
                        <?php
                        $this->widget('CustomCaptchaFactory',
                            array(
                                'captchaAction' => $cAction,
                                'buttonOptions' => array('class' => 'get-new-ver-code'),
                                'clickableImage' => true,
                                'imageOptions'=>array('id'=>'contact-index-form_captcha'),
                                'model' => $model,
                                'attribute' => 'verifyCode',
                            )
                        );?>
                        <?php echo $form->error($model, 'verifyCode');?>
                    </div>
                <?php else:?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php //echo $form->labelEx($model, 'verifyCode', array('class' => 'form-control'));?>
                            <?php echo $form->textField($model, 'verifyCode', array('autocomplete' => 'off', 'placeholder' => $model->getAttributeLabel('verifyCode'), 'class' => 'form-control', 'style' => "display: {$display}"));?><br/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <?php
                        $this->widget('CustomCaptchaFactory',
                            array(
                                'captchaAction' => $cAction,
                                'buttonOptions' => array('class' => 'get-new-ver-code'),
                                'clickableImage' => true,
                                'imageOptions'=>array('id'=>'contact-index-form_captcha'),
                                'model' => $model,
                                'attribute' => 'verifyCode',
                            )
                        );?>
                        <?php echo $form->error($model, 'verifyCode');?>
                    </div>
                <?php endif;?>
            </div>

            <?php
        }

        ?>

        <div class="text-center">
            <?php echo CHtml::submitButton(tt('Send message', 'contactform'), array('class' => 'btn btn-default text-uppercase submit-button'));

            ?>
        </div>

        <?php $this->endWidget(); ?>

    </div>
</div>
