<div class="widget box popular-block">
    <div class="box-header h4"><?= tt('Popular Destinations', 'basis_theme') ?></div>
    <div class="box-content">

        <div class="row">
            <?php
            $objTypesList = ApartmentObjType::getListForSearch();

            foreach ($cities as $city) {
                $imgSrc = $city->image ? $city->image->getThumbHref(150, 115) : Yii::app()->baseUrl . '/uploads/150x100_no_photo_img.png';
                $imgAlt = ($city->image && $city->image->seo) ? $city->image->seo->alt : '';
                $cityUrlField = 'url_' . Yii::app()->language;
                $cityUrl = ($city->seo && $city->seo->{$cityUrlField}) ? $city->seo->{$cityUrlField} : 'javascript:;';

                ?>
                <div class="item-popular col-md-12 col-sm-6">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <img src="<?= $imgSrc ?>" alt="<?= $imgAlt ?>">
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <ul class="list-unstyled">
                                <li class="city_name">
                                    <a href="<?= $cityUrl ?>"><?= $city->name ?></a>
                                    <ul class="list-inline">
                                        <?php foreach ($objTypesList as $objTypeId => $objTypeName) { ?>
                                            <?php $url = Yii::app()->createUrl('/search', array('objType' => $objTypeId, 'city[]' => $city->id)) ?>
                                            <li><a href="<?= $url ?>"><?= $objTypeName ?></a></li>
                                        <?php } ?>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

    </div>
</div>