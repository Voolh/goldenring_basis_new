<?php
if (empty($citiesListResult)) {
    return;
}

$paramsType = $paramsObjType = null;
if (isset($params) && !empty($params)) {
	$paramsType = (isset($params['type'])) ? $params['type'] : null;
	$paramsObjType = (isset($params['obj_type_id'])) ? $params['obj_type_id'] : null;
}
$i = 0;

?>
<div class="widget box popular-block">
    <div class="box-header h4"><?= tt('Popular Destinations', 'basis_theme') ?></div>
    <div class="box-content">

        <div class="row">
            <?php foreach ($citiesListResult as $cityId => $cityValue) { ?>
                <?php
                if ($i >= 4) {
                    break;
                }
                if (issetModule('location')) {
                    $city = City::model()->findByPk($cityId);
                } else {
                    $city = ApartmentCity::model()->findByPk($cityId);
                }
                if (!$city) {
                    return false;
                }
                $imgSrc = $city->image ? $city->image->getThumbHref(150, 115) : Yii::app()->baseUrl . '/common/images/150x100_no_photo_city.jpg';
                $imgAlt = ($city->image && $city->image->seo) ? $city->image->seo->alt : '';
                /*$cityUrlField = 'url_' . Yii::app()->language;
                $cityUrl = ($city->seo && $city->seo->{$cityUrlField}) ? $city->seo->{$cityUrlField} : 'javascript:;';*/
                
                $linkParams = array(
                    'cityUrlName' => $cityValue[Yii::app()->language]['url'],
                );

                if (!empty($paramsType)) {
                    $linkParams['apType'] = $paramsType;
                }

                ?>
                <div class="item-popular col-md-12 col-sm-6">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <img src="<?= $imgSrc ?>" alt="<?= $imgAlt ?>">
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <ul class="list-unstyled">
                                <li class="city_name">
                                    <?php
                                    echo CHtml::link(
                                        $cityValue[Yii::app()->language]['name'], 
                                        Yii::app()->controller->createUrl('/seo/main/viewsummaryinfo', $linkParams)
                                    );?>
                                    <ul class="list-inline">
                                        <?php foreach ($objTypesListResult as $objTypeId => $objValue) { ?>
                                            <li>
                                                <?php
                                                $linkName = $objValue[Yii::app()->language]['name'];
                                                $addCount = '';
                                                $class = 'inactive-obj-type-url';
                                                if (!empty($countApartmentsByCategories)) {
                                                    if (isset($countApartmentsByCategories[$cityId]) && isset($countApartmentsByCategories[$cityId][$objTypeId])) {
                                                        $class = 'active-obj-type-url';
                                                        $addCount = '<span class="obj-type-count">(' . $countApartmentsByCategories[$cityId][$objTypeId] . ')</span>';
                                                    }
                                                }

                                                $linkParams = array(
                                                    'cityUrlName' => $cityValue[Yii::app()->language]['url'],
                                                    'objTypeUrlName' => $objValue[Yii::app()->language]['url'],
                                                );

                                                if (!empty($paramsType)) {
                                                    $linkParams['apType'] = $paramsType;
                                                }

                                                echo CHtml::link(
                                                    $linkName . ' ' . $addCount, Yii::app()->controller->createUrl('/seo/main/viewsummaryinfo', $linkParams), array('class' => $class)
                                                );

                                                ?>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php $i++; ?>
            <?php } ?>

        </div>

    </div>
</div>





