<?php $baseThemeUrl = Yii::app()->theme->baseUrl; ?>

<div class="news_block">
    <div class="h3 h_line"><?php echo tt('Last news', 'entries'); ?></div>

    <div class="news_content owl-start owl-carousel owl-theme">

        <?php foreach ($this->entries as $entries) { ?>
            <?php
            $src = $baseThemeUrl . '/assets/images/news_no_photo_272x181.jpg';
            if ($entries->image) {
                $src = Yii::app()->getBaseUrl() . '/uploads/entries/' . $entries->image->getThumb(272, 181, true);
            }
            $title = $entries->getStrByLang('title');
            $url = $entries->getUrl();
            $announce = ($entries->getAnnounce()) ? $entries->getAnnounce() : '&nbsp;';
            $tagLinks = $entries->getTagLinks();

            ?>
            <div class="item_news">
                <div class="img_news">
                    <a href="<?php echo $url ?>"><img src="<?php echo $src ?>" alt="<?php echo $title ?>"></a>
                    <?php if ($tagLinks) { ?>
                        <ul class="list-inline tags">
                            <?php foreach ($tagLinks as $link) { ?>
                                <li><?php echo $link ?></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </div>
                <div class="h5">
                    <a href="<?php echo $url ?>"><?php echo $title ?></a><span class="date_news"><?= $entries->getDateTimeInFormat('date_created') ?></span>
                </div>

                <div class="text_news"><p><?php echo truncateText($announce, 40); ?></p></div>
            </div>
        <?php } ?>

    </div>

    <div class="text-center">
        <a href="<?= Yii::app()->baseUrl . '/news' ?>" class="slow btn btn-default text-uppercase">
            <i class="fas fa-newspaper"></i> <?= tc('All news') ?>
        </a>

        <?php if (Yii::app()->user->hasState('isAdmin')) { ?>
            <a href="<?= Yii::app()->createUrl('/entries/backend/main/create') ?>" class="slow btn btn-primary text-uppercase">
                <i class="fas fa-plus"></i> <?= tc('Add news') ?>
            </a>
        <?php } ?>
    </div>
</div>