<?php
/* * ********************************************************************************************
 * 								Open Real Estate
 * 								----------------
 * 	version				:	V1.28.3
 * 	copyright			:	(c) 2016 Monoray
 * 							http://monoray.net
 * 							http://monoray.ru
 *
 * 	website				:	http://open-real-estate.info/en
 *
 * 	contact us			:	http://open-real-estate.info/en/contact-us
 *
 * 	license:			:	http://open-real-estate.info/en/license
 * 							http://open-real-estate.info/ru/license
 *
 * This file is part of Open Real Estate
 *
 * ********************************************************************************************* */

class BasisHotAdsWidget extends CWidget
{

    // смотрим таблицу apartment_obj_type
    public $objIds = array(1, 2, 5, 6);
    public $limit = 6;

    public function run()
    {
        if (!$this->objIds || !is_array($this->objIds)) {
            return;
        }

        $tabsData = $this->getTabsData();

        if (!$tabsData)
            return;

        $this->render('hot_ads', array(
            'tabsData' => $tabsData
        ));
    }

    public function getTabsData()
    {
        $objTypeList = ApartmentObjType::getList();
        $data = array();

        foreach ($this->objIds as $objId) {
            if (isset($objTypeList[$objId])) {
                $data[$objId]['ads'] = self::getModelsForObjId($objId, $this->limit);
                $data[$objId]['name'] = $objTypeList[$objId];
            }
        }

        return $data;
    }

    private function getModelsForObjId($objId, $limit = 6)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.active = 1 AND t.owner_active = 1 AND t.deleted = 0 ');
        $criteria->order = 't.sorter DESC, t.is_special_offer DESC, t.date_created DESC';
        $with = array('images');
        if (issetModule('seo')) {
            $with = CMap::mergeArray($with, array('seo'));
        }
        $criteria->with = $with;
        $criteria->compare('t.obj_type_id', $objId);


        $criteria->limit = $limit;

        return Apartment::model()->with($with)->findAll($criteria);
    }
}
