<?php
/* * ********************************************************************************************
 * 								Open Real Estate
 * 								----------------
 * 	version				:	V1.28.3
 * 	copyright			:	(c) 2016 Monoray
 * 							http://monoray.net
 * 							http://monoray.ru
 *
 * 	website				:	http://open-real-estate.info/en
 *
 * 	contact us			:	http://open-real-estate.info/en/contact-us
 *
 * 	license:			:	http://open-real-estate.info/en/license
 * 							http://open-real-estate.info/ru/license
 *
 * This file is part of Open Real Estate
 *
 * ********************************************************************************************* */

class BasisAssets
{

    public static function register()
    {
        $isRTL = Lang::isRTLLang(Yii::app()->language);

        $cs = Yii::app()->clientScript;
        $cs->coreScriptPosition = CClientScript::POS_BEGIN;

        $cs->defaultScriptFilePosition = CClientScript::POS_BEGIN;
        $cs->defaultScriptPosition = CClientScript::POS_END;

        $baseThemeUrl = Yii::app()->theme->baseUrl;
        $baseThemePath = Yii::app()->theme->basePath;

        $version = (demo()) ? ORE_VERSION : "1";

        $cs->registerCoreScript('jquery');
        $cs->registerCoreScript('jquery.ui');
        $cs->registerCoreScript('rating');

        $cs->registerScriptFile($cs->getCoreScriptUrl() . '/jui/js/jquery-ui-i18n.min.js', CClientScript::POS_BEGIN); // fix datePicker lang in free
        $cs->registerScriptFile($baseThemeUrl . '/js/sumoselect/jquery.sumoselect.js', CClientScript::POS_BEGIN);
        $cs->registerScriptFile($baseThemeUrl . '/js/jquery.dropdownPlain.js', CClientScript::POS_BEGIN);
        $cs->registerScriptFile($baseThemeUrl . '/js/common.js', CClientScript::POS_BEGIN);
        $cs->registerScriptFile($baseThemeUrl . '/js/habra_alert.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseThemeUrl . '/js/jquery.cookie.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseThemeUrl . '/js/scrollto.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseThemeUrl . '/js/superfish/js/superfish.js', CClientScript::POS_END);

        if (param('useShowInfoUseCookie')) {
            $cs->registerScriptFile(Yii::app()->request->baseUrl . '/common/js/cookiebar/jquery.cookiebar.js', CClientScript::POS_END);
            $cs->registerCssFile(Yii::app()->request->baseUrl . '/common/js/cookiebar/jquery.cookiebar.css');
        }

        if ($isRTL) {
            $cs->registerCssFile($baseThemeUrl . '/css/ui/jquery-ui.multiselect.css');
            $cs->registerCssFile($baseThemeUrl . '/css/redmond/jquery-ui-1.7.1.custom.css');
            $cs->registerCssFile($baseThemeUrl . '/css/ui.slider.extras.css');
            HSite::LTRToRTLCssContent('/js/sumoselect/sumoselect.css');
            HSite::LTRToRTLCssContent('/css/form.css', 'screen');
            HSite::LTRToRTLCssContent('/js/superfish/css/superfish.css', 'screen');
        } else {
            $cs->registerCssFile($baseThemeUrl . '/css/ui/jquery-ui.multiselect.css');
            $cs->registerCssFile($baseThemeUrl . '/css/redmond/jquery-ui-1.7.1.custom.css');
            $cs->registerCssFile($baseThemeUrl . '/css/ui.slider.extras.css');
            $cs->registerCssFile($baseThemeUrl . '/js/sumoselect/sumoselect.css');
            $cs->registerCssFile($baseThemeUrl . '/css/form.css', 'screen');
            $cs->registerCssFile($baseThemeUrl . '/js/superfish/css/superfish.css', 'screen');
        }

        $cs->registerScriptFile($baseThemeUrl . '/assets/js/bootstrap.min.js', CClientScript::POS_BEGIN);
        $cs->registerScriptFile($baseThemeUrl . '/assets/js/jquery.sticky.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseThemeUrl . '/assets/js/jasny-bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseThemeUrl . '/assets/js/owl.carousel.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseThemeUrl . '/assets/js/slick/slick.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseThemeUrl . '/assets/js/common.js', CClientScript::POS_END);
        if ($isRTL) {
            $cs->registerScriptFile($baseThemeUrl . '/assets/js/own-carousel-init-rtl.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseThemeUrl . '/assets/js/slick-slider-init-rtl.js', CClientScript::POS_END);
        } else {
            $cs->registerScriptFile($baseThemeUrl . '/assets/js/own-carousel-init-ltr.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseThemeUrl . '/assets/js/slick-slider-init-ltr.js', CClientScript::POS_END);
        }
        
        
        //theme css
        if ($isRTL) {
            HSite::LTRToRTLCssContent('/assets/css/jquery.accordion.menu.css');
            HSite::LTRToRTLCssContent('/assets/css/bootstrap.min.css');
            HSite::LTRToRTLCssContent('/assets/css/jasny-bootstrap.min.css');
            $cs->registerCssFile($baseThemeUrl . '/assets/css/owl.carousel.min.css');
            $cs->registerCssFile($baseThemeUrl . '/assets/css/owl.theme.default.min.css');
            $cs->registerCssFile($baseThemeUrl . '/assets/css/fontawesome-all.min.css');
            HSite::LTRToRTLCssContent('/assets/css/pretty-checkbox.min.css');
        } else {
            $cs->registerCssFile($baseThemeUrl . '/assets/css/jquery.accordion.menu.css');
            $cs->registerCssFile($baseThemeUrl . '/assets/css/bootstrap.min.css');
            $cs->registerCssFile($baseThemeUrl . '/assets/css/jasny-bootstrap.min.css');
            $cs->registerCssFile($baseThemeUrl . '/assets/css/owl.carousel.min.css');
            $cs->registerCssFile($baseThemeUrl . '/assets/css/owl.theme.default.min.css');
            $cs->registerCssFile($baseThemeUrl . '/assets/css/fontawesome-all.min.css');
            $cs->registerCssFile($baseThemeUrl . '/assets/css/pretty-checkbox.min.css');
        }

        if ($isRTL) {
            HSite::LTRToRTLCssContent('/assets/js/slick/slick.css');
            HSite::LTRToRTLCssContent('/assets/js/slick/slick-theme.css');
            $cs->registerCssFile($baseThemeUrl . '/assets/css/my_slick.css');
            HSite::LTRToRTLCssContent('/assets/css/style.css');
            //HSite::LTRToRTLCssContent('/assets/css/media-queries.css?v=' . $version);
            $cs->registerCssFile($baseThemeUrl . '/assets/css/media-queries.css?v=' . $version);
        } else { 
            $cs->registerCssFile($baseThemeUrl . '/assets/js/slick/slick.css');
            $cs->registerCssFile($baseThemeUrl . '/assets/js/slick/slick-theme.css');
            $cs->registerCssFile($baseThemeUrl . '/assets/css/my_slick.css');
            $cs->registerCssFile($baseThemeUrl . '/assets/css/style.css?v=' . $version);
            $cs->registerCssFile($baseThemeUrl . '/assets/css/media-queries.css?v=' . $version);
        }

        $cs->registerCssFile($baseThemeUrl . '/assets/css/style_img.css?v=' . $version);
    }
}
