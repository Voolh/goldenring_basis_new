<?php
/* * ********************************************************************************************
 * 								Open Real Estate
 * 								----------------
 * 	version				:	V1.28.3
 * 	copyright			:	(c) 2016 Monoray
 * 							http://monoray.net
 * 							http://monoray.ru
 *
 * 	website				:	http://open-real-estate.info/en
 *
 * 	contact us			:	http://open-real-estate.info/en/contact-us
 *
 * 	license:			:	http://open-real-estate.info/en/license
 * 							http://open-real-estate.info/ru/license
 *
 * This file is part of Open Real Estate
 *
 * ********************************************************************************************* */

class BasisMenuHelper
{

    public static function getControlMenu()
    {
        $user = HUser::getModel();

        if ($user && Yii::app()->user->hasState('isAdmin')) {
            $items[] = array(
                'label' => Yii::t('common', 'Administration'),
                'url' => Yii::app()->createUrl('/apartments/backend/main/admin'),
                'itemOptions' => array('class' => 'dropdown-item'),
            );
            $items[] = array(
                'label' => '(' . $user->username . ') ' . tt('Logout', 'common'),
                'url' => array('/site/logout'),
                'itemOptions' => array('class' => 'dropdown-item'),
            );
        } elseif ($user) {
            $items = HUser::getMenu();
            $items[] = array(
                'label' => '(' . $user->username . ') ' . tt('Logout', 'common'),
                'url' => array('/site/logout'),
                'itemOptions' => array('class' => 'dropdown-item'),
            );
        } else {
            $items[] = array(
                'label' => tc('Login'),
                'url' => Yii::app()->createUrl('/site/login'),
                'itemOptions' => array('class' => 'dropdown-item'),
            );
            if (param('useUserRegistration')) {
                $items[] = array(
                    'label' => tc("Join now"),
                    'url' => Yii::app()->createUrl('/site/register'),
                    'itemOptions' => array('class' => 'dropdown-item'),
                );
            }
            $items[] = array(
                'label' => tc('Forgot password?'),
                'url' => Yii::app()->createUrl('/site/recover'),
                'itemOptions' => array('class' => 'dropdown-item'),
                //'active' => Yii::app()->controller->menuIsActive('my_balance'),
            );
        }

        return $items;
    }
}
