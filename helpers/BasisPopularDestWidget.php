<?php
/* * ********************************************************************************************
 * 								Open Real Estate
 * 								----------------
 * 	version				:	V1.28.3
 * 	copyright			:	(c) 2016 Monoray
 * 							http://monoray.net
 * 							http://monoray.ru
 *
 * 	website				:	http://open-real-estate.info/en
 *
 * 	contact us			:	http://open-real-estate.info/en/contact-us
 *
 * 	license:			:	http://open-real-estate.info/en/license
 * 							http://open-real-estate.info/ru/license
 *
 * This file is part of Open Real Estate
 *
 * ********************************************************************************************* */

class BasisPopularDestWidget extends CWidget
{

    public $showWidgetTitle = true;
    public $params = null;
    public $customWidgetTitle;
    public $widgetTitles;

    public function run()
    {
        if (Themes::getParamJson('popular_dest_user_set')) {
            $citiesId = Themes::getParamJson('cities');

            if ($citiesId) {
                $criteria = new CDbCriteria();
                $criteria->compare('t.id', $citiesId);
                $criteria->order = 'field(t.id, ' . implode(',', $citiesId) . ')';

                if (issetModule('location')) {
                    $citiesModels = City::model()->with(array('image'))->findAll($criteria);
                } else {
                    $citiesModels = ApartmentCity::model()->findAll($criteria);
                }

                if ($citiesModels) {
                    $this->render('popular_dest', array('cities' => $citiesModels));
                }
            }
        } else {
            $citiesListResult = $objTypesListResult = $resCounts = array();
            if (issetModule('seo')) {
                $citiesListResult = SeoFriendlyUrl::getActiveCityRoute($this->params);
                $objTypesListResult = SeoFriendlyUrl::getActiveObjTypesRoute($this->params);
                $resCounts = SeoFriendlyUrl::getCountApartmentsForCategories($this->params);
            }

            $countApartmentsByCategories = array();
            if (!empty($resCounts)) {
                foreach ($resCounts as $values) {
                    $countApartmentsByCategories[$values['city']][$values['obj_type_id']] = $values['count'];
                }
            }
            unset($resCounts);

            $subTitleKey = InfoPages::getWidgetSubTitleKey('seosummaryinfo');
            $this->customWidgetTitle = InfoPages::getWidgetSubTitle($subTitleKey, $this->widgetTitles);

            if (empty($this->customWidgetTitle)) {
                $this->showWidgetTitle = false;
            }

            $this->render('popular_dest_seo', array(
                'citiesListResult' => $citiesListResult,
                'objTypesListResult' => $objTypesListResult,
                'countApartmentsByCategories' => $countApartmentsByCategories,
                'params' => $this->params,
                'showWidgetTitle' => $this->showWidgetTitle,
                'customWidgetTitle' => $this->customWidgetTitle
                )
            );
        }
    }
}
