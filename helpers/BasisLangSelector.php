<?php
/* * ********************************************************************************************
 * 								Open Real Estate
 * 								----------------
 * 	version				:	V1.28.3
 * 	copyright			:	(c) 2016 Monoray
 * 							http://monoray.net
 * 							http://monoray.ru
 *
 * 	website				:	http://open-real-estate.info/en
 *
 * 	contact us			:	http://open-real-estate.info/en/contact-us
 *
 * 	license:			:	http://open-real-estate.info/en/license
 * 							http://open-real-estate.info/ru/license
 *
 * This file is part of Open Real Estate
 *
 * ********************************************************************************************* */

class BasisLangSelector extends CWidget
{

    public function run()
    {
        $languages = Lang::getActiveLangs(true);
        $countLang = count($languages);

        if ($countLang == 1) {
            return;
        }

        $langDataCurrent = isset($languages[Yii::app()->language]) ? $languages[Yii::app()->language] : array();
        if (!$langDataCurrent) {
            return;
        }

        echo '<div class="dropdown">';
        echo '<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
        echo $this->getImgForLang($langDataCurrent) . ' ' . $langDataCurrent['name'] . ' <span class="caret"></span>';
        echo '</button>';
        echo '<ul class="dropdown-menu">';
        foreach ($languages as $langData) {
            if ($langData['name_iso'] == Yii::app()->language) {
                continue;
            }
            $imgFlag = $this->getImgForLang($langData);
            echo '<li>';
            $class = ($langData['name_iso'] == Yii::app()->language) ? 'active' : '';
            $label = $imgFlag . ' ' . $langData['name'];

            echo CHtml::link(
                $label, $this->getOwner()->createLangUrl($langData['name_iso']), array('class' => $class . ' dropdown-item')
            );
            echo '</li>';
        }
        echo '</ul>';
        echo '</div>';
    }

    private function getImgForLang($lang)
    {
        return '<img src="' . Yii::app()->getBaseUrl() . Lang::FLAG_DIR . $lang['flag_img'] . '" alt="' . $lang['name'] . '" title="' . $lang['name'] . '" class="flag_img" />';
    }
}
