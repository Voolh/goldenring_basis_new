<?php
/* * ********************************************************************************************
 * 								Open Real Estate
 * 								----------------
 * 	version				:	V1.28.3
 * 	copyright			:	(c) 2016 Monoray
 * 							http://monoray.net
 * 							http://monoray.ru
 *
 * 	website				:	http://open-real-estate.info/en
 *
 * 	contact us			:	http://open-real-estate.info/en/contact-us
 *
 * 	license:			:	http://open-real-estate.info/en/license
 * 							http://open-real-estate.info/ru/license
 *
 * This file is part of Open Real Estate
 *
 * ********************************************************************************************* */

class BasisCurrencySelector extends CWidget
{

    public function run()
    {
        if (!issetModule('currency') || (issetModule('currency') && count(Currency::getActiveCurrency()) == 1)) {
            return;
        }

        $currencyActvie = Currency::getActiveCurrencyArray(2);
        $currentCharCode = Currency::getCurrentCurrencyModel()->char_code;

        echo '<div class="dropdown">';
        echo '<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
        echo $currencyActvie[$currentCharCode] . ' <span class="caret"></span>';
        echo '</button>';
        echo '<ul class="dropdown-menu">';
        foreach ($currencyActvie as $charCode => $currencyName) {
            if ($charCode == $currentCharCode) {
                continue;
            }
            echo '<li><a class="dropdown-item" href="' . $this->getOwner()->createLangUrl(Yii::app()->language, array('currency' => $charCode)) . '" id="currency_' . $charCode . '">' . $currencyName . '</a></li>';
        }
        echo '</ul>';
        echo '</div>';

        /**
         * <div class="dropdown">
         * <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">RUB <span class="caret"></span></button>
         * <ul class="dropdown-menu">
         * <li><a class="dropdown-item" href="#">USD</a></li>
         * <li><a class="dropdown-item" href="#">EUR</a></li>
         * <li><a class="dropdown-item" href="#">SAR</a></li>
         * </ul>
         * </div>
         */
    }
}
